
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(1024) NOT NULL,
  `sex` tinyint(1) NOT NULL,
  `addr` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `info_code`;
CREATE TABLE `info_code` (
  `Code_ID` varchar(30) NOT NULL COMMENT '代码ID',
  `Code_Type` varchar(20) DEFAULT NULL COMMENT '代码类别',
  `Type_Name` varchar(100) DEFAULT NULL COMMENT '类别名称',
  `Val` varchar(20) DEFAULT NULL COMMENT '代码值',
  `Val_Name` varchar(100) DEFAULT NULL COMMENT '代码名称',
  `Begn_Date` date DEFAULT NULL COMMENT '开始日期',
  `Expir_Date` date DEFAULT NULL COMMENT '终止日期',
  `Vali_Flag` varchar(3) DEFAULT NULL COMMENT '有效标志',
  `Seq` int(4) DEFAULT NULL COMMENT '顺序号',
  PRIMARY KEY (`Code_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='代码表';


DROP TABLE IF EXISTS `info_code_version`;
CREATE TABLE `info_code_version` (
  `ID` int(4) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(64) DEFAULT NULL COMMENT 'version code',
  `VERSION` varchar(64) DEFAULT NULL COMMENT '版本号',
  `CREATE_TIME` datetime DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_TIME` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `biz_log`;
CREATE TABLE `biz_log` (
  `BIZ_SN` varchar(32) NOT NULL COMMENT '业务日志编码',
  `PTCP` varchar(20) DEFAULT NULL COMMENT '当事人',
  `PTCP_NAME` varchar(100) DEFAULT NULL COMMENT '当事人名称',
  `PTCP_TYPE` varchar(3) DEFAULT NULL COMMENT '当事人类型',
  `PTCP_POOLAREA` varchar(6) DEFAULT NULL COMMENT '当事人所属统筹区',
  `PTCP_OPTINS` varchar(20) DEFAULT NULL COMMENT '当事人所属机构',
  `OPTER` varchar(20) DEFAULT NULL COMMENT '经办人',
  `OPTER_NAME` varchar(50) DEFAULT NULL COMMENT '经办人姓名',
  `OPT_TIME` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '经办时间',
  `OPTINS` varchar(20) DEFAULT NULL COMMENT '经办机构',
  `POOLAREA` varchar(8) DEFAULT NULL COMMENT '统筹区',
  `SERV_MATT_NODE_NO` varchar(8) DEFAULT NULL COMMENT '服务事项环节编号',
  `REQ_ID` varchar(50) DEFAULT NULL COMMENT '请求编号',
  PRIMARY KEY (`BIZ_SN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;