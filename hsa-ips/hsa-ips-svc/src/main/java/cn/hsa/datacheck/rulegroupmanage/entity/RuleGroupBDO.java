package cn.hsa.datacheck.rulegroupmanage.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import javax.validation.constraints.Size;

/**
 * <p>
 * 规则组  
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("rule_group_b")
@ApiModel(value="RuleGroupB", description="规则组  ")
public class RuleGroupBDO extends Model<RuleGroupBDO> {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "项目编号")
    @TableField(value = "ITEMCODE", exist = false)
    private String itemCode;

    @ApiModelProperty(value = "数据源编号")
    @TableField(value = "DS_NO", exist = false)
    private String dsNo;

    @ApiModelProperty(value = "规则组编号")
    @TableId("RULE_GRP_NO")
    private String ruleGrpNo;

    @ApiModelProperty(value = "组名称")
    @TableField("GRP_NAME")
    private String grpName;

    @ApiModelProperty(value = "创建时间")
    @TableField("CRTE_TIME")
    private Date crteTime;

    @ApiModelProperty(value = "修改时间")
    @TableField("MODI_TIME")
    private Date modiTime;

    @ApiModelProperty(value = "创建用户")
    @TableField("CRTE_USER")
    private String crteUser;

    @ApiModelProperty(value = "修改用户")
    @TableField("MODI_USER")
    private String modiUser;

    @ApiModelProperty(value = "有效标志")
    @TableField("VALI_FLAG")
    private String valiFlag;

    @ApiModelProperty(value = "唯一记录号")
    @TableField("RID")
    private String rid;

    @ApiModelProperty(value = "描述")
    @TableField("DSCR")
    private String dscr;


    @Override
    protected Serializable pkVal() {
        return this.ruleGrpNo;
    }

}
