package cn.hsa.datacheck.databasemanage.bo;

import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.databasemanage.dto.DatabaseInfoBDTO;
import cn.hsa.framework.commons.page.IQuery;
import cn.hsa.framework.commons.bo.IBO;
import cn.hsa.datacheck.databasemanage.dto.DatabaseInfoBQueryDTO;


/**
 * <p>
 * 数据源信息表 服务类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
public interface DatabaseInfoBBO extends IBO {

    /**
     * 分页查询信息
     *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名
     * @param databaseInfoBDto 参数 DTO 对象
     * @return 分页对象
     */
    Map<String,Object> queryByDTOPage(int pageNumber, int pageSize, String sort, String order,  DatabaseInfoBQueryDTO databaseInfoBQueryDto);

    /**
     * 分页查询信息
     *
     * @param databaseInfoBDto 参数 DTO 对象
     * @return 分页对象
     */
    List<DatabaseInfoBDTO> queryByDTO(DatabaseInfoBQueryDTO databaseInfoBDto);

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return DatabaseInfoB
     */
    DatabaseInfoBDTO getById(String id);


    /**
     * 添加
     *
     * @param databaseInfoB 实体
     * @return void
     */
    DatabaseInfoBDTO save(DatabaseInfoBDTO databaseInfoB);

    /**
     * 测试连接
     *
     * @param databaseInfoB 实体
     * @return void
     */
    DatabaseInfoBDTO testConnect(DatabaseInfoBDTO databaseInfoB);

    /**
     * 更新
     *
     * @param databaseInfoB 实体
     * @return void
     */
    DatabaseInfoBDTO updateById(DatabaseInfoBDTO databaseInfoB);

    /**
     * 更新
     *
     * @param databaseInfoBs 实体
     * @return List<DatabaseInfoBDTO>
     */
    List<DatabaseInfoBDTO> updateBatchById(List<DatabaseInfoBDTO> databaseInfoBs);
    /**
     * 保存
     *
     * @param databaseInfoBs 实体
     * @return List<DatabaseInfoBDTO>
     */
    List<DatabaseInfoBDTO> saveBatch(List<DatabaseInfoBDTO> databaseInfoBs);
    /**
     * 删除
     *
     * @param id 主键
     */
    void delete(String id);
}
