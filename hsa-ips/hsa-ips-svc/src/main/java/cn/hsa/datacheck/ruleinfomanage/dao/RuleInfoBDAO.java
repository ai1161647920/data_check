package cn.hsa.datacheck.ruleinfomanage.dao;

import cn.hsa.datacheck.ruleinfomanage.dto.RuleInfoBQueryDTO;
import cn.hsa.datacheck.ruleinfomanage.entity.RuleInfoBDO;
import cn.hsa.framework.commons.dao.BaseDAO;
import cn.hsa.framework.commons.page.IQuery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 规则表   Mapper 接口
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Mapper
@Repository("NeuRuleInfoBDAO")
public interface RuleInfoBDAO extends BaseDAO<RuleInfoBDO> {

    /**
     * 分页查询（根据 dto 条件）
     * @param page         分页查询条件（可以为 RowBounds.DEFAULT）
     * @param dto          表字段 RuleInfoBQueryDTO 对象
     */
    IQuery<RuleInfoBDO> baseQueryByDTO(IQuery<RuleInfoBDO> page,@Param("dto") RuleInfoBQueryDTO dto);

    /**
     * 不分页查询（根据 dto 条件）
     * @param dto          表字段 RuleInfoBQueryDTO 对象
     */
    List<RuleInfoBDO> baseQueryByDTO(@Param("dto") RuleInfoBQueryDTO dto);
}
