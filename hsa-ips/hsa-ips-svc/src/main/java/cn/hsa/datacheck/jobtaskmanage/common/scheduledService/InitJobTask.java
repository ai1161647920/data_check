package cn.hsa.datacheck.jobtaskmanage.common.scheduledService;

import cn.hsa.datacheck.jobtaskmanage.common.constants.Constant;
import cn.hsa.datacheck.jobtaskmanage.common.util.SpringContextHolder;
import cn.hsa.datacheck.jobtaskmanage.controller.ScheduledController;
import cn.hsa.datacheck.jobtaskmanage.dto.JobTaskDTO;
import cn.hsa.datacheck.jobtaskmanage.dto.JobTaskQueryDTO;
import cn.hsa.datacheck.jobtaskmanage.dto.RuleExecInfoDto;
import cn.hsa.datacheck.jobtaskmanage.service.JobTaskService;
import cn.hsa.datacheck.jobtaskmanage.task.JobTask;
import cn.hsa.datacheck.jobtaskmanage.util.ScheduledFutureHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;

@Slf4j
@Component
public class InitJobTask implements ApplicationRunner {
    /**
     * 服务重启后，后台检查job_task哪些任务的状态是启动的，并重新定时任务
     * @param args
     * @throws Exception
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("=====================定时任务初始化开始====================");
        JobTaskQueryDTO jobTaskQueryDTO = new JobTaskQueryDTO();
        jobTaskQueryDTO.setValiFlag(Constant.VALI_FLAG_1);
        jobTaskQueryDTO.setTimrStas(Constant.TIMR_STAS_START);
        JobTaskService jobTaskService = SpringContextHolder.getBean(JobTaskService.class);
        List<JobTaskDTO> jobTaskDTOS = jobTaskService.queryAll(jobTaskQueryDTO);
        for(JobTaskDTO jobTaskDto : jobTaskDTOS){
            // 执行周期
            String corn = jobTaskDto.getJobCorn();
            ConcurrentHashMap<String, RuleExecInfoDto> ruleExecParamMap = jobTaskService.constructRuleExecParam(jobTaskDto);
            //将任务交给任务调度器执行
            ThreadPoolTaskScheduler threadPoolTaskScheduler = SpringContextHolder.getBean("jobTaskThreadPoolTaskScheduler");
            ScheduledFuture<?> schedule = threadPoolTaskScheduler.schedule(new JobTask(ruleExecParamMap, jobTaskDto, Constant.JOB_TASK_EXEC_START), new CronTrigger(corn));
            //将任务包装成ScheduledFutureHolder
            ScheduledFutureHolder scheduledFutureHolder = new ScheduledFutureHolder();
            scheduledFutureHolder.setScheduledFuture(schedule);
            //scheduledFutureHolder.setRunnableClass(helloTask.getClass());
            scheduledFutureHolder.setCorn(corn);
            scheduledFutureHolder.setTaskNo(jobTaskDto.getTaskNo());
            //将任务信息放入scheduleMap
            Map<String, ScheduledFutureHolder> scheduleMap = ScheduledController.getScheduleMap();
            scheduleMap.put(scheduledFutureHolder.getTaskNo(),scheduledFutureHolder);
        }
        log.info("=====================定时任务初始化结束====================");

    }
}
