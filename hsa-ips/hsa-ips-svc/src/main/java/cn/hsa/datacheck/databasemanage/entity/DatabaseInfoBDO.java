package cn.hsa.datacheck.databasemanage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import javax.validation.constraints.Size;

/**
 * <p>
 * 数据源信息表
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("database_info_b")
@ApiModel(value="DatabaseInfoB", description="数据源信息表")
public class DatabaseInfoBDO extends Model<DatabaseInfoBDO> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "数据源编号")
    @TableId("DS_NO")
    private String dsNo;

    @ApiModelProperty(value = "数据源")
    @TableField("DS")
    private String ds;

    @ApiModelProperty(value = "数据源名称")
    @TableField("DS_NAME")
    private String dsName;

    @ApiModelProperty(value = "所属项目")
    @TableField("ITEM")
    private String item;

    @ApiModelProperty(value = "用户名  ")
    @TableField("USER_NAME")
    private String userName;

    @ApiModelProperty(value = "密码")
    @TableField("PWD")
    private String pwd;

    @ApiModelProperty(value = "连接串")
    @TableField("CONNECTSTRING")
    private String connectstring;

    @ApiModelProperty(value = "驱动")
    @TableField("DRIVER")
    private String driver;

    @ApiModelProperty(value = "创建时间")
    @TableField("CRTE_TIME")
    private Date crteTime;

    @ApiModelProperty(value = "修改时间")
    @TableField("MODI_TIME")
    private Date modiTime;

    @ApiModelProperty(value = "创建用户")
    @TableField("CRTE_USER")
    private String crteUser;

    @ApiModelProperty(value = "修改用户")
    @TableField("MODI_USER")
    private String modiUser;

    @ApiModelProperty(value = "有效标志")
    @TableField("VALI_FLAG")
    private String valiFlag;

    @ApiModelProperty(value = "唯一记录号")
    @TableField("RID")
    private String rid;

    @ApiModelProperty(value = "描述")
    @TableField("DSCR")
    private String dscr;


    @Override
    protected Serializable pkVal() {
        return this.dsNo;
    }

}
