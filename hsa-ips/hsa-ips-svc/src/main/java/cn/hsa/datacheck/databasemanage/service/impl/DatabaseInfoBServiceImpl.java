package cn.hsa.datacheck.databasemanage.service.impl;

import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.databasemanage.dto.DatabaseInfoBQueryDTO;
import cn.hsa.datacheck.databasemanage.dto.DatabaseInfoBDTO;
import cn.hsa.datacheck.databasemanage.bo.DatabaseInfoBBO;
import cn.hsa.datacheck.databasemanage.service.DatabaseInfoBService;
import cn.hsa.framework.commons.page.IQuery;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import cn.hsa.framework.commons.service.BaseService;
/**
 * <p>
 * 数据源信息表 服务实现类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Service("NeuDatabaseInfoBServiceImpl")
public class DatabaseInfoBServiceImpl extends BaseService implements DatabaseInfoBService {


    private final DatabaseInfoBBO databaseInfoBBO;

    @Autowired
    public DatabaseInfoBServiceImpl(DatabaseInfoBBO databaseInfoBBO){
        this.databaseInfoBBO = databaseInfoBBO;
    }
    
    /**
     * 数据源信息表简单分页查询
     *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名  
     * @param databaseInfoB 数据源信息表
     * @return
     */
    @Override
	public Map<String, Object> queryPage(int pageNumber, int pageSize, String sort, String order, DatabaseInfoBQueryDTO databaseInfoB) {
    	return databaseInfoBBO.queryByDTOPage(pageNumber, pageSize, sort, order, databaseInfoB);
	}

    /**
     * 查询所有信息
     *
     * @param databaseInfoB 参数 DatabaseInfoBQueryDTO 对象
     * @return 对象List
     */
    @Override
    public List<DatabaseInfoBDTO> queryAll(DatabaseInfoBQueryDTO databaseInfoB) {
        return databaseInfoBBO.queryByDTO(databaseInfoB);
    }

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return DatabaseInfoB
     */
    @Override
    public DatabaseInfoBDTO queryById(String id) {
        return databaseInfoBBO.getById(id);
    }


    /**
     * 添加
     *
     * @param databaseInfoB DTO
     * @return DatabaseInfoBDTO
     */
    @Override
    public DatabaseInfoBDTO save(DatabaseInfoBDTO databaseInfoB) {
        return databaseInfoBBO.save(databaseInfoB);
    }

    /**
     * 测试连接
     *
     * @param databaseInfoB DTO
     * @return DatabaseInfoBDTO
     */
    @Override
    public DatabaseInfoBDTO testConnect(DatabaseInfoBDTO databaseInfoB) {
        return databaseInfoBBO.testConnect(databaseInfoB);
    }

    /**
     * 更新
     *
     * @param databaseInfoB DTO
     * @return DatabaseInfoBDTO
     */
    @Override
    public DatabaseInfoBDTO update(DatabaseInfoBDTO databaseInfoB) {
        return databaseInfoBBO.updateById(databaseInfoB);
    }

    /**
     * 删除
     *
     * @param id 主键
     */
    @Override
    public void delete(String id) {
        databaseInfoBBO.delete(id);
    }

    /**
     * 批量更新
     *
     * @param databaseInfoBs 实体
     * @return List<DatabaseInfoBDTO>
     */
    @Override
    public List<DatabaseInfoBDTO> updateBatch(List<DatabaseInfoBDTO> databaseInfoBs) {
        return databaseInfoBBO.updateBatchById(databaseInfoBs);
    }

    /**
     * 批量新增
     *
     * @param databaseInfoBs 实体
     * @return List<DatabaseInfoBDTO>
     */
    @Override
    public List<DatabaseInfoBDTO> saveBatch(List<DatabaseInfoBDTO> databaseInfoBs) {
        return databaseInfoBBO.saveBatch(databaseInfoBs);
    }

}
