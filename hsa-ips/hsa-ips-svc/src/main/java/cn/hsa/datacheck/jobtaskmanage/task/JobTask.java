package cn.hsa.datacheck.jobtaskmanage.task;

import cn.hsa.datacheck.jobtaskmanage.common.constants.Constant;
import cn.hsa.datacheck.jobtaskmanage.common.util.CommonUtil;
import cn.hsa.datacheck.jobtaskmanage.common.util.SpringContextHolder;
import cn.hsa.datacheck.jobtaskmanage.dto.DbInfoDTO;
import cn.hsa.datacheck.jobtaskmanage.dto.JobTaskDTO;
import cn.hsa.datacheck.jobtaskmanage.dto.RuleExecInfoDto;
import cn.hsa.datacheck.jobtaskmanage.service.CommonService;
import cn.hsa.datacheck.jobtaskmanage.service.JobTaskService;
import cn.hsa.datacheck.rulecheckresult.dto.RuleCheckResultDTO;
import cn.hsa.datacheck.rulecheckresult.service.RuleCheckResultService;
import cn.hsa.datacheck.ruleinfomanage.dto.RuleInfoBDTO;

import cn.hsa.framework.commons.util.OrgHelper;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class JobTask implements Runnable{

    private ConcurrentHashMap<String, RuleExecInfoDto> ruleExecParamMap;

    private JobTaskDTO jobTaskDto;

    //操作，执行或者启动
    private String operation;



    @Override
    public void run() {
        log.info("JobTask 开始执行====================,RuleExecParamMap is {}", ruleExecParamMap);
        String execResult = Constant.EXEC_RESULT_SUCCESS;
        //设置任务批次号final Long chkBtch =
        Long chkBtchLong = new Date().getTime();
        final String chkBtch = chkBtchLong.toString();
        for(String ruleGrpNo : ruleExecParamMap.keySet()){
            RuleExecInfoDto ruleExecInfoDto = ruleExecParamMap.get(ruleGrpNo);
            DbInfoDTO dbInfoDTO = ruleExecInfoDto.getDbInfoDTO();

            if(dbInfoDTO == null || dbInfoDTO.getClass()== null || dbInfoDTO.getUrl()== null){
                log.info("dbInfoDTO参数非法");
                JobTaskExecResultToDB(this.jobTaskDto, Constant.EXEC_RESULT_FAIL);
                return;
            }
            if(dbInfoDTO.getUsername() == null || dbInfoDTO.getPassword() == null){
                log.info("dbInfoDTO参数用户信息非法");
                JobTaskExecResultToDB(this.jobTaskDto, Constant.EXEC_RESULT_FAIL);
                return;
            }
            // 同一个规则组下的规则列表
            List<RuleInfoBDTO> ruleInfoBDTOList = ruleExecInfoDto.getRuleInfoBDTOList();
            Connection connection = null;
            Statement stmt = null;
            ResultSet rs = null;
            try {
                Class.forName(dbInfoDTO.getDriveClassName());
                connection = DriverManager.getConnection(dbInfoDTO.getUrl(), dbInfoDTO.getUsername(), dbInfoDTO.getPassword());
                stmt = connection.createStatement();
                for(RuleInfoBDTO ruleInfor : ruleInfoBDTOList){
                    String tableName =  ruleInfor.getTabName();
                    String sqlCond = ruleInfor.getChkSqlCond();
                    if(tableName == null || sqlCond == null){
                        log.info("sql信息异常，" + ruleInfor);
                        continue;
                    }
                    StringBuffer CountByConditionSql = new StringBuffer(Constant.SELECT_COUNT);
                    CountByConditionSql.append(sqlCond);
                    StringBuffer  CountSql = new StringBuffer(Constant.SELECT_COUNT).append(tableName);
                    ResultSet AllCountResultSet = stmt.executeQuery(CountSql.toString());
                    int AllRowCount = 0;
                    if(AllCountResultSet.next()){
                        AllRowCount = AllCountResultSet.getInt(1);
                    }

                    ResultSet conditionCountResultSet = stmt.executeQuery(CountByConditionSql.toString());
                    int conditionRowCount = 0;
                    if(conditionCountResultSet.next()){
                        conditionRowCount = conditionCountResultSet.getInt(1);
                    }
                    RuleCheckResultService ruleCheckResultService = SpringContextHolder.getBean(RuleCheckResultService.class);
                    RuleCheckResultDTO ruleCheckResultDTO = new RuleCheckResultDTO();
                    // 设置批次号
                    ruleCheckResultDTO.setChkBtch(chkBtch);
                    //设置ruleCheckResultDTO
                    ruleCheckResultDTO.setRuleNo(ruleInfor.getRuleNo());//规则编号
                    ruleCheckResultDTO.setItemcode(ruleExecInfoDto.getItemcode());//项目编号
                    ruleCheckResultDTO.setTotlDataAmt(new BigDecimal(AllRowCount));//总数据量
                    ruleCheckResultDTO.setAbnDataAmt(new BigDecimal(conditionRowCount));//异常数据量
                    //ruleCheckResultDTO.setCrteUser("system");
                    //用户信息改为获取当前用户信息(共修改22个地方，7个文件) zhangliang 2021-11-10
                    ruleCheckResultDTO.setCrteUser(OrgHelper.getUserAcct());
                    ruleCheckResultDTO.setCrtOpnn("同意");
                    Date date = new Date();
                    ruleCheckResultDTO.setCrteTime(date);//创建时间
                    ruleCheckResultDTO.setModiTime(date);//修改时间
                    ruleCheckResultDTO.setValiFlag(Constant.VALI_FLAG_1);

                    RuleCheckResultDTO save = ruleCheckResultService.save(ruleCheckResultDTO);
                    if(save == null){
                        log.info("================RuleCheckResultDTO增加失败");
                    }
                    log.info("====================RuleCheckResultDTO增加成功");

                    StringBuffer selectAllSql = new StringBuffer(Constant.SELECT_All_FIELD);
                    selectAllSql.append(sqlCond);
                    ResultSet resultSet = stmt.executeQuery(selectAllSql.toString());
                    //

                    JSONArray queryResults = CommonUtil.resultSetToJson(resultSet);
                    System.out.println("=========" + queryResults);
                    List<String> insertSqlList = new ArrayList<>();
                    //遍历拼接sql，并插入数据
                    for(int i= 0; i < queryResults.size(); i++){
                        JSONObject singleJson = queryResults.getJSONObject(i);
                        // 新增批次号和规则编号
                        singleJson.put("CHK_BTCH", chkBtch);
                        singleJson.put("RULE_NO",ruleInfor.getRuleNo());
                        Set<String> keySet = singleJson.keySet();
                        List<String> keys = new ArrayList<>(keySet);
                        //StringBuffer insertSql = new StringBuffer(Constant.INSERT_INTO).append(tableName).append(Constant.BLANK_SPACE);
                        StringBuffer insertSql = new StringBuffer();
                        StringBuffer fileds = new StringBuffer();
                        StringBuffer values = new StringBuffer();
                        //
                        for(int index = 0; index < keys.size(); index++){
                            String field = keys.get(index);
                            String value = singleJson.getString(field);
                            if(index == 0){
                                fileds.append(Constant.LEFT_CIR);
                                values.append(Constant.LEFT_CIR);
                            }
                            value = appendStr(value);
                            fileds = index != keys.size() -1 ? fileds.append(field).append(",") : fileds.append(field);
                            values = index != keys.size() -1 ? values.append(value).append(",") : values.append(value);
                            if(index == keys.size() -1){
                                fileds.append(Constant.RIGHT_CIR).append(Constant.BLANK_SPACE);
                                values.append(Constant.RIGHT_CIR);
                            }
                        }
                        insertSql.append(fileds).append(Constant.VALUES).append(values);
                        log.info("insert:" + insertSql.toString());
                        insertSqlList.add(insertSql.toString());
                        CommonService commonService = SpringContextHolder.getBean(CommonService.class);
                        commonService.insertByDynamicSql(tableName, insertSql.toString());
//                        try {
//                            stmt.executeUpdate(insertSql.toString());
//                        } catch (SQLException throwables) {
//                            log.error("数据添加失败"+ insertSql.toString()+ ":"+ throwables);
//                        }
                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
                execResult = Constant.EXEC_RESULT_FAIL;
            } finally {
                try {
                    if(rs != null){
                        rs.close();
                    }
                    if(stmt != null){
                        stmt.close();
                    }
                    if(connection != null){
                        connection.close();
                    }
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
            //如果是执行一次
            if(Constant.JOB_TASK_EXEC_OPERATION.equals(this.operation)){
                //任务执行完，则状态为停止
                jobTaskDto.setTimrStas(Constant.TIMR_STAS_STOP);
            }
            // 刷新jobtask执行结果
            JobTaskExecResultToDB(jobTaskDto, execResult);

            //String dataSourceKey = ruleExecInfoDto.getRuleGrpNo();
//            DataSourceUtil.addDataSourceToDynamic(dataSourceKey,dbInfoDTO);
//            // 切换数据源
//            DynamicDataSourceContextHolder.setContextKey(dataSourceKey);
//            CommonDao commonDao = SpringContextHolder.getBean(CommonDao.class);
//            commonDao.delTable("test_user");
//            DynamicDataSourceContextHolder.removeContextKey();
//            DataSourceUtil.delDataSourceToDynamic(dataSourceKey);
//            log.info("datasource关闭成功====================");
        }

    }

    private String appendStr(String field){
        return new StringBuffer("'").append(field).append("'").toString();
    }

    public JobTask(ConcurrentHashMap<String, RuleExecInfoDto> ruleExecParamMap, JobTaskDTO jobTaskDto, String operation) {
        this.ruleExecParamMap = ruleExecParamMap;
        this.jobTaskDto = jobTaskDto;
        this.operation = operation;
    }

    /**
     * 刷新jobtask执行结果
     * @param jobTaskDto
     * @param execResult
     */
    private void JobTaskExecResultToDB (JobTaskDTO jobTaskDto, String execResult){
        JobTaskService jobTaskService = SpringContextHolder.getBean(JobTaskService.class);
        jobTaskDto.setLastExeRslt(execResult);
        jobTaskDto.setModiTime(new Date());
        jobTaskService.update(jobTaskDto);

    }
}

