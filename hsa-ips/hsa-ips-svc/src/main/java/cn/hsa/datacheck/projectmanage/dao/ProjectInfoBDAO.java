package cn.hsa.datacheck.projectmanage.dao;

import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.projectmanage.entity.ProjectInfoBDO;
import cn.hsa.datacheck.projectmanage.dto.ProjectInfoBQueryDTO;
import cn.hsa.framework.commons.dao.BaseDAO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.hsa.framework.commons.page.IQuery;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 项目信息表 Mapper 接口
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Mapper
@Repository("NeuProjectInfoBDAO")
public interface ProjectInfoBDAO extends BaseDAO<ProjectInfoBDO> {

    /**
     * 分页查询（根据 dto 条件）
     * @param page         分页查询条件（可以为 RowBounds.DEFAULT）
     * @param dto          表字段 ProjectInfoBQueryDTO 对象
     */
    IQuery<ProjectInfoBDO> baseQueryByDTO(IQuery<ProjectInfoBDO> page,@Param("dto") ProjectInfoBQueryDTO dto);

    /**
     * 不分页查询（根据 dto 条件）
     * @param dto          表字段 ProjectInfoBQueryDTO 对象
     */
    List<ProjectInfoBDO> baseQueryByDTO(@Param("dto") ProjectInfoBQueryDTO dto);
}
