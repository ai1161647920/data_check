package cn.hsa.datacheck.jobtaskmanage.bo.impl;



import cn.hsa.datacheck.jobtaskmanage.dao.JobTaskDAO;
import cn.hsa.datacheck.jobtaskmanage.entity.JobTaskDO;
import cn.hsa.framework.commons.bo.CommonBO;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 定时任务表   服务实现类
 * </p>
 *
 * @author pengli
 * @since 2021-07-26
 */
@Service
public class JobTaskCommonBOImpl extends CommonBO<JobTaskDAO, JobTaskDO>  {


}
