package cn.hsa.datacheck.projectrulemanage.controller;

import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.hsa.hsaf.core.framework.web.WrapperResponse;

import cn.hsa.datacheck.projectrulemanage.dto.ProjectRuleDTO;
import cn.hsa.datacheck.projectrulemanage.dto.ProjectRuleQueryDTO;
import cn.hsa.datacheck.projectrulemanage.service.ProjectRuleService;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Map;
import org.springframework.web.bind.annotation.RestController;
import cn.hsa.framework.commons.controller.BaseController;
import lombok.extern.slf4j.Slf4j;
import javax.validation.Valid;

/**
 * <p>
 * 项目规则表 前端控制器
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@RestController
@Slf4j
@Api(value = "/web/projectrulemanage/projectRule", tags = {"操作项目规则表"})
@RequestMapping("/web/projectrulemanage/projectRule")
public class ProjectRuleController extends BaseController  {


    private final ProjectRuleService projectRuleService;

    @Autowired
    public ProjectRuleController(ProjectRuleService projectRuleService){
        this.projectRuleService = projectRuleService;
    }
    /**
     * 通过ID查询
     *
     * @param id ID
     * @return ProjectRule
     */
    @ApiOperation(value = "通过ID查询", tags = {"通过ID查询"}, notes = "ID不能为空")
    @GetMapping("/{id}")
    public WrapperResponse<ProjectRuleDTO> get(@PathVariable String id) {
        return WrapperResponse.success(projectRuleService.queryById(id));
    }

    /**
     * 通过ruleGrpNo查询
     *
     * @param ruleGrpNo ruleGrpNo
     * @return ProjectRule
     */
   @ApiOperation(value = "通过ruleGrpNo查询", tags = {"通过ruleGrpNo查询"}, notes = "ruleGrpNo不能为空")
    @GetMapping("/ruleGrp/{ruleGrpNo}")
    public WrapperResponse<ProjectRuleDTO> getByRuleGrpNo(@PathVariable String ruleGrpNo) {
        return WrapperResponse.success(projectRuleService.getByRuleGrpNo(ruleGrpNo));
    }



    /**
     * 分页查询信息
     *
     * @param projectRuleQuery 分页对象
     * @return 分页对象
     */
    @ApiOperation(value = "分页查询")
    @GetMapping("/page")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "pageNumber", value = "页码", dataType = "string", paramType = "query", example = "1"),
        @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "long", paramType = "query", example = "10"),
        @ApiImplicitParam(name = "sort", value = "正序/倒序", dataType = "string", paramType = "query", example = "asc/desc"),
        @ApiImplicitParam(name = "order", value = "排序字段-属性名", dataType = "string", paramType = "query", example = "rid"),
        @ApiImplicitParam(name = "projectRule", value = "项目规则表查询条件", dataType = "ProjectRuleQueryDTO", paramType = "query")
    })
    public WrapperResponse<Map<String, Object>> page(
            Integer pageNumber,
            Integer pageSize,
            String sort,
            String order,
            ProjectRuleQueryDTO projectRuleQuery) {
        Map<String, Object> map = projectRuleService.queryPage(pageNumber, pageSize, sort, order, projectRuleQuery);
    	// 如不想动BO，此处可以再加工为前台需要的自定义对象，适配界面用，例如 (**DTO)map.get("result")
    	return WrapperResponse.success(map);
    }

    /**
     * 添加
     *
     * @param projectRuleDto 实体
     * @return success/false
     */
    @PostMapping
    @ApiOperation(value = "添加项目规则表")
    @ResponseStatus(HttpStatus.CREATED)
    public WrapperResponse<?> add(@Valid @RequestBody @ApiParam(name="项目规则表",value="传入json格式",required=true) ProjectRuleDTO projectRuleDto) {
        ProjectRuleDTO ProjectRuleDto = projectRuleService.save(projectRuleDto);
        return WrapperResponse.success(ProjectRuleDto);
    }

    /**
     * 删除
     *
     * @param id ID
     * @return success/false
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除项目规则表")
    public WrapperResponse<?>  delete(@PathVariable String id) {
        projectRuleService.delete(id);
        return WrapperResponse.success("");
    }

    /**
     * 编辑
     *
     * @param projectRuleDto 实体
     * @return success/false
     */
    @PutMapping
    @ApiOperation(value = "编辑项目规则表")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public WrapperResponse<?> edit(@Valid @RequestBody @ApiParam(name="项目规则表",value="传入json格式",required=true) ProjectRuleDTO projectRuleDto) {
        ProjectRuleDTO ProjectRuleDto = projectRuleService.update(projectRuleDto);
        return WrapperResponse.success(ProjectRuleDto);
    }

    /**
     * 保存List
     *
     * @param projectRuleList 实体
     * @return success/false
     */
    @PostMapping("/saveList")
    @ApiOperation(value = "编辑项目规则表")
    public WrapperResponse<?> saveList(@Valid @RequestBody @ApiParam(name="项目规则表",value="传入json格式",required=true) List<ProjectRuleDTO> projectRuleList) {
        projectRuleService.saveBatch(projectRuleList);
        return WrapperResponse.success("");
    }
}
