package cn.hsa.datacheck.rulegroupmanage.service.impl;

import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.rulegroupmanage.dto.RuleGroupBQueryDTO;
import cn.hsa.datacheck.rulegroupmanage.dto.RuleGroupBDTO;
import cn.hsa.datacheck.rulegroupmanage.bo.RuleGroupBBO;
import cn.hsa.datacheck.rulegroupmanage.service.RuleGroupBService;
import cn.hsa.framework.commons.page.IQuery;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import cn.hsa.framework.commons.service.BaseService;
/**
 * <p>
 * 规则组   服务实现类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Service("NeuRuleGroupBServiceImpl")
public class RuleGroupBServiceImpl extends BaseService implements RuleGroupBService {


    private final RuleGroupBBO ruleGroupBBO;

    @Autowired
    public RuleGroupBServiceImpl(RuleGroupBBO ruleGroupBBO){
        this.ruleGroupBBO = ruleGroupBBO;
    }
    
    /**
     * 规则组  简单分页查询
     *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名  
     * @param ruleGroupB 规则组  
     * @return
     */
    @Override
	public Map<String, Object> queryPage(int pageNumber, int pageSize, String sort, String order, RuleGroupBQueryDTO ruleGroupB) {
    	return ruleGroupBBO.queryByDTOPage(pageNumber, pageSize, sort, order, ruleGroupB);
	}

    /**
     * 查询所有信息
     *
     * @param ruleGroupB 参数 RuleGroupBQueryDTO 对象
     * @return 对象List
     */
    @Override
    public List<RuleGroupBDTO> queryAll(RuleGroupBQueryDTO ruleGroupB) {
        return ruleGroupBBO.queryByDTO(ruleGroupB);
    }

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return RuleGroupB
     */
    @Override
    public RuleGroupBDTO queryById(String id) {
        return ruleGroupBBO.getById(id);
    }


    /**
     * 添加
     *
     * @param ruleGroupB DTO
     * @return RuleGroupBDTO
     */
    @Override
    public RuleGroupBDTO save(RuleGroupBDTO ruleGroupB) {
        return ruleGroupBBO.save(ruleGroupB);
    }

    /**
     * 更新
     *
     * @param ruleGroupB DTO
     * @return RuleGroupBDTO
     */
    @Override
    public RuleGroupBDTO update(RuleGroupBDTO ruleGroupB) {
        return ruleGroupBBO.updateById(ruleGroupB);
    }

    /**
     * 删除
     *
     * @param ruleGroupBDto 主键
     */
    @Override
    public void delete(RuleGroupBDTO ruleGroupBDto) {
        ruleGroupBBO.delete(ruleGroupBDto);
    }

    /**
     * 批量更新
     *
     * @param ruleGroupBs 实体
     * @return List<RuleGroupBDTO>
     */
    @Override
    public List<RuleGroupBDTO> updateBatch(List<RuleGroupBDTO> ruleGroupBs) {
        return ruleGroupBBO.updateBatchById(ruleGroupBs);
    }

    /**
     * 批量新增
     *
     * @param ruleGroupBs 实体
     * @return List<RuleGroupBDTO>
     */
    @Override
    public List<RuleGroupBDTO> saveBatch(List<RuleGroupBDTO> ruleGroupBs) {
        return ruleGroupBBO.saveBatch(ruleGroupBs);
    }

    @Override
    public List<RuleGroupBDTO> baseQueryByDTO(RuleGroupBQueryDTO dto) {

        return ruleGroupBBO.baseQueryByDTO(dto);
    }

}
