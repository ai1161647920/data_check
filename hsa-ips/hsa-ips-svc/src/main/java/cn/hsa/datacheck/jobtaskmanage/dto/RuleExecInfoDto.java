package cn.hsa.datacheck.jobtaskmanage.dto;


import cn.hsa.datacheck.ruleinfomanage.dto.RuleInfoBDTO;
import lombok.Data;

import java.util.List;

@Data
public class RuleExecInfoDto {

    /**
     *规则组编号
     */
    private String ruleGrpNo;

    /**
     * 项目编号
     */
    private String itemcode;

    /**
     * 规则列表
     */
    List<RuleInfoBDTO> RuleInfoBDTOList;

    /**
     * 规则组对应的数据源信息
     */
    private DbInfoDTO dbInfoDTO;

    public RuleExecInfoDto(String ruleGrpNo, String itemcode, List<RuleInfoBDTO> ruleInfoBDTOList, DbInfoDTO dbInfoDTO) {
        this.ruleGrpNo = ruleGrpNo;
        this.itemcode = itemcode;
        this.RuleInfoBDTOList = ruleInfoBDTOList;
        this.dbInfoDTO = dbInfoDTO;
    }
}
