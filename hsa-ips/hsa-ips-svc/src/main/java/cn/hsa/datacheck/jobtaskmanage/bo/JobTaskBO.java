package cn.hsa.datacheck.jobtaskmanage.bo;

import java.util.List;
import java.util.Map;


import cn.hsa.datacheck.jobtaskmanage.dto.JobTaskDTO;
import cn.hsa.datacheck.jobtaskmanage.dto.JobTaskQueryDTO;
import cn.hsa.framework.commons.page.IQuery;
import cn.hsa.framework.commons.bo.IBO;



/**
 * <p>
 * 定时任务表   服务类
 * </p>
 *
 * @author pengli
 * @since 2021-07-26
 */
public interface JobTaskBO extends IBO {

    /**
     * 分页查询信息
     *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名
     * @param jobTaskDto 参数 DTO 对象
     * @return 分页对象
     */
    Map<String,Object> queryByDTOPage(int pageNumber, int pageSize, String sort, String order,  JobTaskQueryDTO jobTaskQueryDto);

    /**
     * 分页查询信息
     *
     * @param jobTaskDto 参数 DTO 对象
     * @return 分页对象
     */
    List<JobTaskDTO> queryByDTO(JobTaskQueryDTO jobTaskDto);

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return JobTask
     */
    JobTaskDTO getById(String id);


    /**
     * 添加
     *
     * @param jobTask 实体
     * @return void
     */
    JobTaskDTO save(JobTaskDTO jobTask);


    /**
     * 更新
     *
     * @param jobTask 实体
     * @return void
     */
    JobTaskDTO updateById(JobTaskDTO jobTask);

    /**
     * 更新
     *
     * @param jobTasks 实体
     * @return List<JobTaskDTO>
     */
    List<JobTaskDTO> updateBatchById(List<JobTaskDTO> jobTasks);
    /**
     * 保存
     *
     * @param jobTasks 实体
     * @return List<JobTaskDTO>
     */
    List<JobTaskDTO> saveBatch(List<JobTaskDTO> jobTasks);
    /**
     * 删除
     *
     * @param id 主键
     */
    void delete(String id);

    /**
     * 根据taskNo查询
     * @param taskNo
     * @return
     */
    JobTaskDTO queryByTaskNo(String taskNo);

    /**
     * 根据任务编号、任务名称判断是否已经存在
     * @param jobTaskDto
     */
    List<JobTaskDTO> preSave(JobTaskDTO jobTaskDto);
}
