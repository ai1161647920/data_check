package cn.hsa.datacheck.databasemanage.controller;


import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.hsa.hsaf.core.framework.web.WrapperResponse;

import cn.hsa.datacheck.databasemanage.dto.DatabaseInfoBDTO;
import cn.hsa.datacheck.databasemanage.dto.DatabaseInfoBQueryDTO;
import cn.hsa.datacheck.databasemanage.service.DatabaseInfoBService;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Map;
import org.springframework.web.bind.annotation.RestController;
import cn.hsa.framework.commons.controller.BaseController;
import lombok.extern.slf4j.Slf4j;
import javax.validation.Valid;

/**
 * <p>
 * 数据源信息表 前端控制器
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@RestController
@Slf4j
@Api(value = "/web/databasemanage/databaseInfoB", tags = {"操作数据源信息表"})
@RequestMapping("/web/databasemanage/databaseInfoB")
public class DatabaseInfoBController extends BaseController  {


    private final DatabaseInfoBService databaseInfoBService;

    @Autowired
    public DatabaseInfoBController(DatabaseInfoBService databaseInfoBService){
        this.databaseInfoBService = databaseInfoBService;
    }
    /**
     * 通过ID查询
     *
     * @param id ID
     * @return DatabaseInfoB
     */
    @ApiOperation(value = "通过ID查询", tags = {"通过ID查询"}, notes = "ID不能为空")
    @GetMapping("/{id}")
    public WrapperResponse<DatabaseInfoBDTO> get(@PathVariable String id) {
        return WrapperResponse.success(databaseInfoBService.queryById(id));
    }

    /**
     * 查询数据源信息
     */
    @ApiOperation(value = "查询数据源信息", tags = {"查询数据源信息"})
    @GetMapping("/getDataBases")
    public WrapperResponse<List<DatabaseInfoBDTO>> getDataBases() {
        return WrapperResponse.success(databaseInfoBService.queryAll(new DatabaseInfoBQueryDTO()));
    }

    /**
     * 分页查询信息
     *
     * @param databaseInfoBQuery 分页对象
     * @return 分页对象
     */
    @ApiOperation(value = "分页查询")
    @GetMapping("/page")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "pageNumber", value = "页码", dataType = "string", paramType = "query", example = "1"),
        @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "long", paramType = "query", example = "10"),
        @ApiImplicitParam(name = "sort", value = "正序/倒序", dataType = "string", paramType = "query", example = "asc/desc"),
        @ApiImplicitParam(name = "order", value = "排序字段-属性名", dataType = "string", paramType = "query", example = "rid"),
        @ApiImplicitParam(name = "databaseInfoB", value = "数据源信息表查询条件", dataType = "DatabaseInfoBQueryDTO", paramType = "query")
    })
    public WrapperResponse<Map<String, Object>> page(
            Integer pageNumber,
            Integer pageSize,
            String sort,
            String order,
            DatabaseInfoBQueryDTO databaseInfoBQuery) {
        Map<String, Object> map = databaseInfoBService.queryPage(pageNumber, pageSize, sort, order, databaseInfoBQuery);
    	// 如不想动BO，此处可以再加工为前台需要的自定义对象，适配界面用，例如 (**DTO)map.get("result")
    	return WrapperResponse.success(map);
    }

    /**
     * 添加
     *
     * @param databaseInfoBDto 实体
     * @return success/false
     */
    @PostMapping
    @ApiOperation(value = "添加数据源信息表")
    @ResponseStatus(HttpStatus.CREATED)
    public WrapperResponse<?> add(@Valid @RequestBody @ApiParam(name="数据源信息表",value="传入json格式",required=true) DatabaseInfoBDTO databaseInfoBDto) {
        DatabaseInfoBDTO DatabaseInfoBDto = databaseInfoBService.save(databaseInfoBDto);
        return WrapperResponse.success(DatabaseInfoBDto);
    }

    /**
     * 测试连接
     *
     * @param databaseInfoBDto 实体
     * @return success/false
     */
    @PostMapping("/testConnect")
    @ApiOperation(value = "测试数据源")
    public WrapperResponse<?> testConnect(@Valid @RequestBody @ApiParam(name="数据源信息表",value="传入json格式",required=true) DatabaseInfoBDTO databaseInfoBDto) {
        DatabaseInfoBDTO DatabaseInfoBDto = databaseInfoBService.testConnect(databaseInfoBDto);
        if(DatabaseInfoBDto == null){
            return WrapperResponse.fail("连接失败！");
        }else{
            return WrapperResponse.success(DatabaseInfoBDto);
        }

    }

    /**
     * 删除
     *
     * @param id ID
     * @return success/false
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除数据源信息表")
    public WrapperResponse<?>  delete(@PathVariable String id) {
        databaseInfoBService.delete(id);
        return WrapperResponse.success("");
    }

    /**
     * 编辑
     *
     * @param databaseInfoBDto 实体
     * @return success/false
     */
    @PutMapping
    @ApiOperation(value = "编辑数据源信息表")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public WrapperResponse<?> edit(@Valid @RequestBody @ApiParam(name="数据源信息表",value="传入json格式",required=true) DatabaseInfoBDTO databaseInfoBDto) {
        DatabaseInfoBDTO DatabaseInfoBDto = databaseInfoBService.update(databaseInfoBDto);
        return WrapperResponse.success(DatabaseInfoBDto);
    }

    /**
     * 保存List
     *
     * @param databaseInfoBList 实体
     * @return success/false
     */
    @PostMapping("/saveList")
    @ApiOperation(value = "编辑数据源信息表")
    public WrapperResponse<?> saveList(@Valid @RequestBody @ApiParam(name="数据源信息表",value="传入json格式",required=true) List<DatabaseInfoBDTO> databaseInfoBList) {
        databaseInfoBService.saveBatch(databaseInfoBList);
        return WrapperResponse.success("");
    }
}
