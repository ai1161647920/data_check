package cn.hsa.datacheck.projectrulemanage.service.impl;

import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.projectrulemanage.dto.ProjectRuleQueryDTO;
import cn.hsa.datacheck.projectrulemanage.dto.ProjectRuleDTO;
import cn.hsa.datacheck.projectrulemanage.bo.ProjectRuleBO;
import cn.hsa.datacheck.projectrulemanage.service.ProjectRuleService;
import cn.hsa.framework.commons.page.IQuery;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import cn.hsa.framework.commons.service.BaseService;
/**
 * <p>
 * 项目规则表 服务实现类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Service("NeuProjectRuleServiceImpl")
public class ProjectRuleServiceImpl extends BaseService implements ProjectRuleService {


    private final ProjectRuleBO projectRuleBO;

    @Autowired
    public ProjectRuleServiceImpl(ProjectRuleBO projectRuleBO){
        this.projectRuleBO = projectRuleBO;
    }
    
    /**
     * 项目规则表简单分页查询
     *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名  
     * @param projectRule 项目规则表
     * @return
     */
    @Override
	public Map<String, Object> queryPage(int pageNumber, int pageSize, String sort, String order, ProjectRuleQueryDTO projectRule) {
    	return projectRuleBO.queryByDTOPage(pageNumber, pageSize, sort, order, projectRule);
	}

    /**
     * 查询所有信息
     *
     * @param projectRule 参数 ProjectRuleQueryDTO 对象
     * @return 对象List
     */
    @Override
    public List<ProjectRuleDTO> queryAll(ProjectRuleQueryDTO projectRule) {
        return projectRuleBO.queryByDTO(projectRule);
    }

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return ProjectRule
     */
    @Override
    public ProjectRuleDTO queryById(String id) {
        return projectRuleBO.getById(id);
    }


    /**
     * 添加
     *
     * @param projectRule DTO
     * @return ProjectRuleDTO
     */
    @Override
    public ProjectRuleDTO save(ProjectRuleDTO projectRule) {
        return projectRuleBO.save(projectRule);
    }

    /**
     * 更新
     *
     * @param projectRule DTO
     * @return ProjectRuleDTO
     */
    @Override
    public ProjectRuleDTO update(ProjectRuleDTO projectRule) {
        return projectRuleBO.updateById(projectRule);
    }

    /**
     * 删除
     *
     * @param id 主键
     */
    @Override
    public void delete(String id) {
        projectRuleBO.delete(id);
    }

    /**
     * 批量更新
     *
     * @param projectRules 实体
     * @return List<ProjectRuleDTO>
     */
    @Override
    public List<ProjectRuleDTO> updateBatch(List<ProjectRuleDTO> projectRules) {
        return projectRuleBO.updateBatchById(projectRules);
    }

    /**
     * 批量新增
     *
     * @param projectRules 实体
     * @return List<ProjectRuleDTO>
     */
    @Override
    public List<ProjectRuleDTO> saveBatch(List<ProjectRuleDTO> projectRules) {
        return projectRuleBO.saveBatch(projectRules);
    }

    @Override
    public ProjectRuleDTO getByRuleGrpNo(String ruleGrpNo) {
        return projectRuleBO.getByRuleGrpNo(ruleGrpNo);
    }

}
