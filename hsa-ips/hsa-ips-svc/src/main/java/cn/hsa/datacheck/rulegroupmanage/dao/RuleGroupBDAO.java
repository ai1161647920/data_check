package cn.hsa.datacheck.rulegroupmanage.dao;

import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.rulegroupmanage.entity.RuleGroupBDO;
import cn.hsa.datacheck.rulegroupmanage.dto.RuleGroupBQueryDTO;
import cn.hsa.framework.commons.dao.BaseDAO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.hsa.framework.commons.page.IQuery;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 规则组   Mapper 接口
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Mapper
@Repository("NeuRuleGroupBDAO")
public interface RuleGroupBDAO extends BaseDAO<RuleGroupBDO> {

    /**
     * 分页查询（根据 dto 条件）
     * @param page         分页查询条件（可以为 RowBounds.DEFAULT）
     * @param dto          表字段 RuleGroupBQueryDTO 对象
     */
    IQuery<RuleGroupBDO> baseQueryByDTO(IQuery<RuleGroupBDO> page,@Param("dto") RuleGroupBQueryDTO dto);

    /**
     * 不分页查询（根据 dto 条件）
     * @param dto          表字段 RuleGroupBQueryDTO 对象
     */
    List<RuleGroupBDO> baseQueryByDTO(@Param("dto") RuleGroupBQueryDTO dto);



}
