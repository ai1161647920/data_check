package cn.hsa.datacheck.rulegroupmanage.service;


import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.rulegroupmanage.dto.RuleGroupBDTO;
import cn.hsa.datacheck.rulegroupmanage.dto.RuleGroupBQueryDTO;
import cn.hsa.framework.commons.page.IQuery;
import cn.hsa.framework.commons.service.IBaseService;

/**
 * <p>
 * 规则组   服务类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
public interface RuleGroupBService extends IBaseService {
	/**
	 * 规则组  简单分页查询
	 *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名     	 
	 * @param ruleGroupB 规则组  
	 * @return
	 */
	Map<String,Object> queryPage(int pageNumber, int pageSize, String sort, String order, RuleGroupBQueryDTO ruleGroupB);

    /**
     * 查询所有信息
     *
     * @param ruleGroupB 参数 RuleGroupBQueryDTO 对象
     * @return 分页对象
     */
    List<RuleGroupBDTO> queryAll(RuleGroupBQueryDTO ruleGroupB);

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return RuleGroupB
     */
    RuleGroupBDTO queryById(String id);


    /**
     * 添加
     *
     * @param ruleGroupB DTO
     * @return RuleGroupBDTO
     */
    RuleGroupBDTO save(RuleGroupBDTO ruleGroupB);

    /**
     * 更新
     *
     * @param ruleGroupB DTO
     * @return RuleGroupBDTO
     */
    RuleGroupBDTO update(RuleGroupBDTO ruleGroupB);

    /**
     * 删除
     *
     * @param ruleGroupBDto 主键
     * @return success/false
     */
    void delete(RuleGroupBDTO ruleGroupBDto);


    /**
     * 更新
     *
     * @param ruleGroupBs DTO
     * @return List<RuleGroupBDTO>
     */
    List<RuleGroupBDTO> updateBatch(List<RuleGroupBDTO> ruleGroupBs);
    /**
     * 保存
     *
     * @param ruleGroupBs DTO
     * @return List<RuleGroupBDTO>
     */
    List<RuleGroupBDTO> saveBatch(List<RuleGroupBDTO> ruleGroupBs);


    /**
     *
     * @param dto
     * @return
     */
    List<RuleGroupBDTO> baseQueryByDTO(RuleGroupBQueryDTO dto);



}

