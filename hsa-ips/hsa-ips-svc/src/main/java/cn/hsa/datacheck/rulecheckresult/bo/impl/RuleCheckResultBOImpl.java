package cn.hsa.datacheck.rulecheckresult.bo.impl;

import cn.hsa.datacheck.databasemanage.dao.DatabaseInfoBDAO;
import cn.hsa.datacheck.databasemanage.entity.DatabaseInfoBDO;
import cn.hsa.datacheck.jobtaskmanage.common.constants.Constant;
import cn.hsa.datacheck.jobtaskmanage.common.util.CommonUtil;
import cn.hsa.datacheck.rulecheckresult.bo.RuleCheckResultBO;
import cn.hsa.datacheck.rulecheckresult.common.Constants;
import cn.hsa.datacheck.rulecheckresult.dao.RuleCheckResultDAO;
import cn.hsa.datacheck.rulecheckresult.dto.RuleCheckResultDTO;
import cn.hsa.datacheck.rulecheckresult.dto.RuleCheckResultDtoAssembler;
import cn.hsa.datacheck.rulecheckresult.dto.RuleCheckResultQueryDTO;
import cn.hsa.datacheck.rulecheckresult.entity.RuleCheckResultDO;
import cn.hsa.datacheck.ruleinfomanage.dao.RuleInfoBDAO;
import cn.hsa.datacheck.ruleinfomanage.entity.RuleInfoBDO;
import cn.hsa.db.config.DBSlaveConfig;
import cn.hsa.framework.commons.bo.BaseBO;
import cn.hsa.framework.commons.page.IQuery;
import cn.hsa.framework.commons.page.QueryResult;
import cn.hsa.framework.commons.util.OrgHelper;
import cn.hsa.framework.commons.util.SequenceUtil;
import com.alibaba.fastjson.JSONArray;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 项目规则校验结果汇总表 服务实现类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Service("NeuRuleCheckResultBOImpl")
@Slf4j
public class RuleCheckResultBOImpl extends BaseBO implements RuleCheckResultBO {

    @Autowired
    private DBSlaveConfig dbSlaveConfig;

    @Autowired
    protected SequenceUtil sequenceUtil;

    private final RuleCheckResultDAO ruleCheckResultDAO;

    private final DatabaseInfoBDAO databaseInfoBDAO;

    private final RuleInfoBDAO ruleInfoBDAO;

    private final RuleCheckResultCommonBOImpl ruleCheckResultCommonBO;

    @Autowired
    public RuleCheckResultBOImpl(RuleInfoBDAO ruleInfoBDAO,RuleCheckResultDAO ruleCheckResultDAO,DatabaseInfoBDAO databaseInfoBDAO,  RuleCheckResultCommonBOImpl ruleCheckResultCommonBO,DBSlaveConfig dbSlaveConfig) {
        this.ruleCheckResultDAO = ruleCheckResultDAO;
        this.ruleInfoBDAO = ruleInfoBDAO;
        this.databaseInfoBDAO = databaseInfoBDAO;
        this.ruleCheckResultCommonBO = ruleCheckResultCommonBO;
        this.dbSlaveConfig = dbSlaveConfig;
    }


    /**
     * 分页查询信息
     *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名
     * @return 分页对象
     */
    @Override
    public Map<String, Object> queryByDTOPage(int pageNumber, int pageSize, String sort, String order, RuleCheckResultQueryDTO ruleCheckResultQueryDto) {
       
        //调用中台或DB
    	IQuery<RuleCheckResultDO> page = QueryResult.of(pageNumber, pageSize, sort, order);
    	IQuery<RuleCheckResultDO> da = ruleCheckResultDAO.baseQueryByDTO(page, ruleCheckResultQueryDto);
    	List<RuleCheckResultDTO> ruleCheckResultList = RuleCheckResultDtoAssembler.toRuleCheckResultDtoList(da.getResult());

    	Map<String,Object> rs = new HashMap<String, Object>();
    	rs.put("pageNumber", da.getPageNumber());
    	rs.put("pageSize", da.getPageSize());
    	rs.put("total", da.getRecordCount());
    	rs.put("result", ruleCheckResultList);
    	return rs;
    	    	
	}

    @Override
    public Map<String, Object> queryByDTOPage(int pageNumber, int pageSize, String sort, String order, String ruleNo,String chkBtch,String tableName) {
        Connection connection = null;
        Statement stmt = null;
        ResultSet rs = null;
        int total = 0;
        //DatabaseInfoBDO dbInfoDTO = databaseInfoBDAO.selectById(Constants.DATA_ID);
        RuleInfoBDO ruleInfoB = ruleInfoBDAO.selectById(ruleNo);
        tableName = ruleInfoB.getTabName();
        try {
            dbSlaveConfig.setPlatform("mysql");
            Class.forName(dbSlaveConfig.getDriverClassName());
            connection = DriverManager.getConnection(dbSlaveConfig.getUrl(), dbSlaveConfig.getUsername(), dbSlaveConfig.getPassword());
            stmt = connection.createStatement();
            StringBuffer  CountSql = new StringBuffer(Constant.SELECT_COUNT).append(tableName).append(" where RULE_NO = \""+ruleNo+"\"  and CHK_BTCH = \""+chkBtch +"\"");
            ResultSet AllCountResultSet = stmt.executeQuery(CountSql.toString());

                if(AllCountResultSet.next()){
                    total = AllCountResultSet.getInt(1);
                }
                StringBuffer selectAllSql = new StringBuffer(Constant.SELECT_All_FIELD);
                StringBuffer headSql = new StringBuffer();
                switch (dbSlaveConfig.getPlatform().toUpperCase()){
                    case Constants.ORACLE:
                        String whereC = "(select rownum r,e. * from "+ tableName +" e where RULE_NO = \""+ruleNo+"\"  and CHK_BTCH = \""+chkBtch+"\" and rownum<" + pageNumber*pageSize +") t where r>="+((pageNumber-1)*pageSize);
                        selectAllSql.append(whereC);
                        headSql.append("select column_name columnName,comments columnComment from user_col_comments t WHERE upper(TABLE_NAME) = upper('");
                        headSql.append(tableName);
                        headSql.append("')");

                        break;
                    default :
                        selectAllSql.append(tableName);
                        selectAllSql.append(" where RULE_NO = \""+ruleNo+"\"  and CHK_BTCH = \""+chkBtch+"\" limit  "+ ((pageNumber-1)*pageSize) + "," + pageSize);

                        String schema = dbSlaveConfig.getUrl().split("/")[3].split("\\?")[0];
                        headSql.append("SELECT COLUMN_NAME columnName,column_comment columnComment FROM INFORMATION_SCHEMA.Columns WHERE table_name='");
                        headSql.append(tableName);
                        headSql.append("' AND table_schema='");
                        headSql.append(schema);
                        headSql.append("'");
                        break;
                }


                ResultSet resultSet = stmt.executeQuery(selectAllSql.toString());
                JSONArray queryResults = CommonUtil.resultSetToJson(resultSet);
            ResultSet headResultSet = stmt.executeQuery(headSql.toString());
            JSONArray headResults = CommonUtil.resultSetToJson(headResultSet);
            Map<String,Object> rst = new HashMap<String, Object>();
            rst.put("pageNumber", pageNumber);
            rst.put("pageSize", pageSize);
            rst.put("total", total);
            rst.put("result", queryResults);
            rst.put("head", headResults);
            return rst;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if(rs != null){
                    rs.close();
                }
                if(stmt != null){
                    stmt.close();
                }
                if(connection != null){
                    connection.close();
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        Map<String,Object> rst = new HashMap<String, Object>();
        rst.put("pageNumber", pageNumber);
        rst.put("pageSize", pageSize);
        rst.put("total", total);
        rst.put("result", "");
        return rst;


    }

    /**
     * 分页查询信息
     *
     * @param ruleCheckResultDto 参数 DTO 对象
     * @return 分页对象
     */
    @Override
    public List<RuleCheckResultDTO> queryByDTO(RuleCheckResultQueryDTO ruleCheckResultDto) {
        List<RuleCheckResultDO> ruleCheckResultList = ruleCheckResultDAO.baseQueryByDTO(ruleCheckResultDto);
        return RuleCheckResultDtoAssembler.toRuleCheckResultDtoList(ruleCheckResultList);
    }

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return RuleCheckResult
     */
    @Override
    public RuleCheckResultDTO getById(String id) {
        RuleCheckResultDO ruleCheckResult = ruleCheckResultDAO.selectById(id);
        return RuleCheckResultDtoAssembler.toRuleCheckResultDto(ruleCheckResult);
    }

    /**
     * 添加
     *
     * @param ruleCheckResultDto RuleCheckResultDTO
     * @return RuleCheckResultDTO
     */
    @Override
    public RuleCheckResultDTO save(RuleCheckResultDTO ruleCheckResultDto) {
        RuleCheckResultDO ruleCheckResult = RuleCheckResultDtoAssembler.toRuleCheckResultDo(ruleCheckResultDto);
        Date date = new Date();
        ruleCheckResult.setValiFlag("1");
        ruleCheckResult.setCrteTime(date);
        ruleCheckResult.setModiTime(date);
        ruleCheckResult.setModiUser(OrgHelper.getUserAcct());
        ruleCheckResult.setCrteUser(OrgHelper.getUserAcct());
        ruleCheckResult.setRid(sequenceUtil.getRID());
        ruleCheckResultCommonBO.saveEntity(ruleCheckResult);
        return RuleCheckResultDtoAssembler.toRuleCheckResultDto(ruleCheckResult);
    }


    /**
     * 更新
     *
     * @param ruleCheckResultDto RuleCheckResultDTO
     * @return RuleCheckResultDTO
     */
    @Override
    public RuleCheckResultDTO updateById(RuleCheckResultDTO ruleCheckResultDto) {
        RuleCheckResultDO ruleCheckResult = RuleCheckResultDtoAssembler.toRuleCheckResultDo(ruleCheckResultDto);
        ruleCheckResult.setModiTime(new Date());
        ruleCheckResult.setModiUser(OrgHelper.getUserAcct());
        ruleCheckResultCommonBO.updateEntityById(ruleCheckResult);
        return RuleCheckResultDtoAssembler.toRuleCheckResultDto(ruleCheckResult);
    }

    /**
     * 更新
     *
     * @param ruleCheckResultDtos List<RuleCheckResultDTO>
     * @return List<RuleCheckResultDTO>
     */
    @Override
    public List<RuleCheckResultDTO> updateBatchById(List<RuleCheckResultDTO> ruleCheckResultDtos) {
        List<RuleCheckResultDO> updateList = RuleCheckResultDtoAssembler.toRuleCheckResultDoList(ruleCheckResultDtos);
        ruleCheckResultCommonBO.updateEntityBatchById(updateList);
        return RuleCheckResultDtoAssembler.toRuleCheckResultDtoList(updateList);
    }

    /**
     * 保存
     *
     * @param ruleCheckResultDtos List<RuleCheckResultDTO>
     * @return List<RuleCheckResultDTO>
     */
    @Override
    public List<RuleCheckResultDTO> saveBatch(List<RuleCheckResultDTO> ruleCheckResultDtos) {
        List<RuleCheckResultDO> saveList = RuleCheckResultDtoAssembler.toRuleCheckResultDoList(ruleCheckResultDtos);
        ruleCheckResultCommonBO.saveEntityBatch(saveList);
        return RuleCheckResultDtoAssembler.toRuleCheckResultDtoList(saveList);
    }

     /**
     * 逻辑删除
     *
     * @param id 主键
     */
     @Override
     public void delete(String id) {
        RuleCheckResultDO ruleCheckResult = new RuleCheckResultDO();
        ruleCheckResult.setChkWater(id);
        ruleCheckResult.setValiFlag("0");
        ruleCheckResultCommonBO.updateEntityById(ruleCheckResult);
     }

}
