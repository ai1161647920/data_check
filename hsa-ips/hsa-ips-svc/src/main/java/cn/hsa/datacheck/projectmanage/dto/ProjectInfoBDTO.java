package cn.hsa.datacheck.projectmanage.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import javax.validation.constraints.Size;

/**
 * <p>
 * 项目信息表
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@ApiModel(value="ProjectInfoBDTO", description="项目信息表")
public class ProjectInfoBDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "项目编号")
    @Size(max=40,message="不能超过最大长度：40")
    private String itemcode;

    @ApiModelProperty(value = "项目名称")
    @Size(max=100,message="不能超过最大长度：100")
    private String itemname;

    @ApiModelProperty(value = "创建时间")
    private Date crteTime;

    @ApiModelProperty(value = "修改时间")
    private Date modiTime;

    @ApiModelProperty(value = "创建用户")
    @Size(max=20,message="不能超过最大长度：20")
    private String crteUser;

    @ApiModelProperty(value = "修改用户")
    @Size(max=20,message="不能超过最大长度：20")
    private String modiUser;

    @ApiModelProperty(value = "有效标志")
    @Size(max=1,message="不能超过最大长度：1")
    private String valiFlag;

    @ApiModelProperty(value = "唯一记录号")
    @Size(max=40,message="不能超过最大长度：40")
    private String rid;

    @ApiModelProperty(value = "描述")
    @Size(max=65535,message="不能超过最大长度：65535")
    private String dscr;


    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public Date getCrteTime() {
        return crteTime;
    }

    public void setCrteTime(Date crteTime) {
        this.crteTime = crteTime;
    }

    public Date getModiTime() {
        return modiTime;
    }

    public void setModiTime(Date modiTime) {
        this.modiTime = modiTime;
    }

    public String getCrteUser() {
        return crteUser;
    }

    public void setCrteUser(String crteUser) {
        this.crteUser = crteUser;
    }

    public String getModiUser() {
        return modiUser;
    }

    public void setModiUser(String modiUser) {
        this.modiUser = modiUser;
    }

    public String getValiFlag() {
        return valiFlag;
    }

    public void setValiFlag(String valiFlag) {
        this.valiFlag = valiFlag;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getDscr() {
        return dscr;
    }

    public void setDscr(String dscr) {
        this.dscr = dscr;
    }


    @Override
    public String toString() {
        return "ProjectInfoB{" +
        "itemcode=" + itemcode +
        ", itemname=" + itemname +
        ", crteTime=" + crteTime +
        ", modiTime=" + modiTime +
        ", crteUser=" + crteUser +
        ", modiUser=" + modiUser +
        ", valiFlag=" + valiFlag +
        ", rid=" + rid +
        ", dscr=" + dscr +
        "}";
    }
}
