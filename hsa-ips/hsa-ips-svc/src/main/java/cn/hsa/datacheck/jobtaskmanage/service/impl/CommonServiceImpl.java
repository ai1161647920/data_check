package cn.hsa.datacheck.jobtaskmanage.service.impl;


import cn.hsa.datacheck.jobtaskmanage.bo.CommonBO;
import cn.hsa.datacheck.jobtaskmanage.service.CommonService;
import com.baomidou.dynamic.datasource.annotation.DS;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@DS("slave")
@Service
public class CommonServiceImpl implements CommonService {

  private final CommonBO commonBo;
  @Autowired
  public CommonServiceImpl(CommonBO commonBo) {
    this.commonBo = commonBo;
  }


  /**
     * 创建表
     * @param tableName
     */
  @DS("slave")
    @Override
    public void addTable(String tableName) {
        commonBo.addTable(tableName);
    }

    /**
     * 删除表
     * @param tableName
     */
    @DS("slave")
    @Override
    public void delTable(String tableName) {
        commonBo.delTable(tableName);
    }
  @DS("slave")
  @Override
  public void insertByDynamicSql(String tableName, String dynamicSql) {
    commonBo.insertByDynamicSql(tableName, dynamicSql);
  }
}
