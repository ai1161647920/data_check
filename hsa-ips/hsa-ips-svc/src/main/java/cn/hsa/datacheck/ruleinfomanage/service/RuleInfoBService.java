package cn.hsa.datacheck.ruleinfomanage.service;


import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.ruleinfomanage.dto.RuleInfoBDTO;
import cn.hsa.datacheck.ruleinfomanage.dto.RuleInfoBQueryDTO;
import cn.hsa.framework.commons.page.IQuery;
import cn.hsa.framework.commons.service.IBaseService;

/**
 * <p>
 * 规则表   服务类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
public interface RuleInfoBService extends IBaseService {
	/**
	 * 规则表  简单分页查询
	 *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名     	 
	 * @param ruleInfoB 规则表  
	 * @return
	 */
	Map<String,Object> queryPage(int pageNumber, int pageSize, String sort, String order, RuleInfoBQueryDTO ruleInfoB);

    /**
     * 查询所有信息
     *
     * @param ruleInfoB 参数 RuleInfoBQueryDTO 对象
     * @return 分页对象
     */
    List<RuleInfoBDTO> queryAll(RuleInfoBQueryDTO ruleInfoB);

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return RuleInfoB
     */
    RuleInfoBDTO queryById(String id);



    /**
     * 通过ruleGrpNo查询
     *
     * @param ruleGrpNo ruleGrpNo
     * @return RuleInfoB
     */
    List<RuleInfoBDTO> queryByRuleGrpNo(String ruleGrpNo);

    /**
     * 添加
     *
     * @param ruleInfoB DTO
     * @return RuleInfoBDTO
     */
    RuleInfoBDTO save(RuleInfoBDTO ruleInfoB);
    RuleInfoBDTO testConnect(RuleInfoBDTO ruleInfoB);
    Map<String, Object> getTables(RuleInfoBDTO ruleInfoBDto);



    /**
     * 更新
     *
     * @param ruleInfoB DTO
     * @return RuleInfoBDTO
     */
    RuleInfoBDTO update(RuleInfoBDTO ruleInfoB);

    /**
     * 删除
     *
     * @param id 主键
     * @return success/false
     */
    void delete(String id);


    /**
     * 更新
     *
     * @param ruleInfoBs DTO
     * @return List<RuleInfoBDTO>
     */
    List<RuleInfoBDTO> updateBatch(List<RuleInfoBDTO> ruleInfoBs);
    /**
     * 保存
     *
     * @param ruleInfoBs DTO
     * @return List<RuleInfoBDTO>
     */
    List<RuleInfoBDTO> saveBatch(List<RuleInfoBDTO> ruleInfoBs);



}

