package cn.hsa.datacheck.jobtaskmanage.dao;

import java.util.List;
import java.util.Map;


import cn.hsa.datacheck.jobtaskmanage.dto.JobTaskDTO;
import cn.hsa.datacheck.jobtaskmanage.dto.JobTaskQueryDTO;
import cn.hsa.datacheck.jobtaskmanage.entity.JobTaskDO;
import cn.hsa.framework.commons.dao.BaseDAO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.hsa.framework.commons.page.IQuery;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 定时任务表   Mapper 接口
 * </p>
 *
 * @author pengli
 * @since 2021-07-26
 */
@Mapper
@Repository
public interface JobTaskDAO extends BaseDAO<JobTaskDO> {

    /**
     * 分页查询（根据 dto 条件）
     * @param page         分页查询条件（可以为 RowBounds.DEFAULT）
     * @param dto          表字段 JobTaskQueryDTO 对象
     */
    IQuery<JobTaskDO> baseQueryByDTO(IQuery<JobTaskDO> page,@Param("dto") JobTaskQueryDTO dto);

    /**
     * 不分页查询（根据 dto 条件）
     * @param dto          表字段 JobTaskQueryDTO 对象
     */
    List<JobTaskDO> baseQueryByDTO(@Param("dto") JobTaskQueryDTO dto);

    /**
     * 根据taskNo查询
     * @param taskNo
     * @return
     */
    JobTaskDO queryByTaskNo(@Param("taskNo") String taskNo);

    /**
     * 根据任务名称和任务编号查询任务记录是否存在
     * @param jobTaskDto
     * @return
     */
    List<JobTaskDO> preSave(@Param("dto") JobTaskDTO jobTaskDto);
}
