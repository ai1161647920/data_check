package cn.hsa.datacheck.databasemanage.dto;

import cn.hsa.datacheck.databasemanage.entity.DatabaseInfoBDO;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 转换
 */
public class DatabaseInfoBDtoAssembler {

    private DatabaseInfoBDtoAssembler() {
        // hide for utils
    }

    /**
     * 转换为DTO对象
     *
     * @param value
     * @return
     */
    public static DatabaseInfoBDTO toDatabaseInfoBDto(DatabaseInfoBDO value) {
        if (value == null) {
            return null;
        }
        DatabaseInfoBDTO dto = new DatabaseInfoBDTO();
        BeanUtils.copyProperties(value, dto);
        return dto;
    }

    /**
     * 转换为实体对象
     *
     * @param dto
     * @return
     */
    public static DatabaseInfoBDO toDatabaseInfoBDo(DatabaseInfoBDTO dto) {
        if (dto == null) {
            return null;
        }
        DatabaseInfoBDO value = new DatabaseInfoBDO();
        BeanUtils.copyProperties(dto, value);
        return value;
    }

    /**
     * 转换为DTO对象list
     *
     * @param databaseInfoBs
     * @return
     */
    public static List<DatabaseInfoBDTO> toDatabaseInfoBDtoList(List<DatabaseInfoBDO> databaseInfoBs) {
        if (databaseInfoBs == null) {
            return null;
        }
        return databaseInfoBs.stream().map(DatabaseInfoBDtoAssembler::toDatabaseInfoBDto).collect(Collectors.toList());
    }

    /**
     * 转换为实体对象list
     *
     * @param dtos
     * @return
     */
    public static List<DatabaseInfoBDO> toDatabaseInfoBDoList(List<DatabaseInfoBDTO> dtos) {
        if (dtos == null) {
            return null;
        }
        return dtos.stream().map(DatabaseInfoBDtoAssembler::toDatabaseInfoBDo).collect(Collectors.toList());
    }

}
