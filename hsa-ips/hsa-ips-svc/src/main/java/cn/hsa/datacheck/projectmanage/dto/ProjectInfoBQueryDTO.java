package cn.hsa.datacheck.projectmanage.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * <p>
 * 项目信息表
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@ApiModel(value="ProjectInfoBQueryDTO", description="项目信息表")
public class ProjectInfoBQueryDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "项目编号")
    private String itemcode;

    @ApiModelProperty(value = "项目名称")
    private String itemname;

    @ApiModelProperty(value = "创建时间")
    private Date crteTime;

    @ApiModelProperty(value = "修改时间")
    private Date modiTime;

    @ApiModelProperty(value = "创建用户")
    private String crteUser;

    @ApiModelProperty(value = "修改用户")
    private String modiUser;

    @ApiModelProperty(value = "有效标志")
    private String valiFlag;

    @ApiModelProperty(value = "唯一记录号")
    private String rid;

    @ApiModelProperty(value = "描述")
    private String dscr;


    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public Date getCrteTime() {
        return crteTime;
    }

    public void setCrteTime(Long crteTime) {
        this.crteTime = crteTime != null ? new Date(crteTime) : null;
    }

    public Date getModiTime() {
        return modiTime;
    }

    public void setModiTime(Long modiTime) {
        this.modiTime = modiTime != null ? new Date(modiTime) : null;
    }

    public String getCrteUser() {
        return crteUser;
    }

    public void setCrteUser(String crteUser) {
        this.crteUser = crteUser;
    }

    public String getModiUser() {
        return modiUser;
    }

    public void setModiUser(String modiUser) {
        this.modiUser = modiUser;
    }

    public String getValiFlag() {
        return valiFlag;
    }

    public void setValiFlag(String valiFlag) {
        this.valiFlag = valiFlag;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getDscr() {
        return dscr;
    }

    public void setDscr(String dscr) {
        this.dscr = dscr;
    }


    @Override
    public String toString() {
        return "ProjectInfoB{" +
        "itemcode=" + itemcode +
        ", itemname=" + itemname +
        ", crteTime=" + crteTime +
        ", modiTime=" + modiTime +
        ", crteUser=" + crteUser +
        ", modiUser=" + modiUser +
        ", valiFlag=" + valiFlag +
        ", rid=" + rid +
        ", dscr=" + dscr +
        "}";
    }
}
