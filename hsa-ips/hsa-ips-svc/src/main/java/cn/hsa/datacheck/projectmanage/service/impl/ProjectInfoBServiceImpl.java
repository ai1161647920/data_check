package cn.hsa.datacheck.projectmanage.service.impl;

import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.projectmanage.dto.ProjectInfoBQueryDTO;
import cn.hsa.datacheck.projectmanage.dto.ProjectInfoBDTO;
import cn.hsa.datacheck.projectmanage.bo.ProjectInfoBBO;
import cn.hsa.datacheck.projectmanage.service.ProjectInfoBService;
import cn.hsa.framework.commons.page.IQuery;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import cn.hsa.framework.commons.service.BaseService;
/**
 * <p>
 * 项目信息表 服务实现类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Service("NeuProjectInfoBServiceImpl")
public class ProjectInfoBServiceImpl extends BaseService implements ProjectInfoBService {


    private final ProjectInfoBBO projectInfoBBO;

    @Autowired
    public ProjectInfoBServiceImpl(ProjectInfoBBO projectInfoBBO){
        this.projectInfoBBO = projectInfoBBO;
    }
    
    /**
     * 项目信息表简单分页查询
     *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名  
     * @param projectInfoB 项目信息表
     * @return
     */
    @Override
	public Map<String, Object> queryPage(int pageNumber, int pageSize, String sort, String order, ProjectInfoBQueryDTO projectInfoB) {
    	return projectInfoBBO.queryByDTOPage(pageNumber, pageSize, sort, order, projectInfoB);
	}

    /**
     * 查询所有信息
     *
     * @param projectInfoB 参数 ProjectInfoBQueryDTO 对象
     * @return 对象List
     */
    @Override
    public List<ProjectInfoBDTO> queryAll(ProjectInfoBQueryDTO projectInfoB) {
        return projectInfoBBO.queryByDTO(projectInfoB);
    }

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return ProjectInfoB
     */
    @Override
    public ProjectInfoBDTO queryById(String id) {
        return projectInfoBBO.getById(id);
    }


    /**
     * 添加
     *
     * @param projectInfoB DTO
     * @return ProjectInfoBDTO
     */
    @Override
    public ProjectInfoBDTO save(ProjectInfoBDTO projectInfoB) {
        return projectInfoBBO.save(projectInfoB);
    }

    /**
     * 更新
     *
     * @param projectInfoB DTO
     * @return ProjectInfoBDTO
     */
    @Override
    public ProjectInfoBDTO update(ProjectInfoBDTO projectInfoB) {
        return projectInfoBBO.updateById(projectInfoB);
    }

    /**
     * 删除
     *
     * @param id 主键
     */
    @Override
    public void delete(String id) {
        projectInfoBBO.delete(id);
    }

    /**
     * 批量更新
     *
     * @param projectInfoBs 实体
     * @return List<ProjectInfoBDTO>
     */
    @Override
    public List<ProjectInfoBDTO> updateBatch(List<ProjectInfoBDTO> projectInfoBs) {
        return projectInfoBBO.updateBatchById(projectInfoBs);
    }

    /**
     * 批量新增
     *
     * @param projectInfoBs 实体
     * @return List<ProjectInfoBDTO>
     */
    @Override
    public List<ProjectInfoBDTO> saveBatch(List<ProjectInfoBDTO> projectInfoBs) {
        return projectInfoBBO.saveBatch(projectInfoBs);
    }

}
