package cn.hsa.datacheck.rulecheckresult.dto;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import javax.validation.constraints.Size;

/**
 * <p>
 * 项目规则校验结果汇总表
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@ApiModel(value="RuleCheckResultDTO", description="项目规则校验结果汇总表")
public class RuleCheckResultDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "校验流水")
    @Size(max=40,message="不能超过最大长度：40")
    private String chkWater;

    @ApiModelProperty(value = "规则编号")
    @Size(max=40,message="不能超过最大长度：40")
    private String ruleNo;

    @ApiModelProperty(value = "校验SQL条件")
    private String chkSqlCond;

    @ApiModelProperty(value = "项目编号")
    @Size(max=40,message="不能超过最大长度：40")
    private String itemcode;

    @ApiModelProperty(value = "总数据量")
    private BigDecimal totlDataAmt;

    @ApiModelProperty(value = "异常数据量")
    private BigDecimal abnDataAmt;

    @ApiModelProperty(value = "当前处理人")
    @Size(max=20,message="不能超过最大长度：20")
    private String crtProcer;

    @ApiModelProperty(value = "当前处理意见")
    @Size(max=65535,message="不能超过最大长度：65535")
    private String crtOpnn;

    @ApiModelProperty(value = "校验批次")
    @Size(max=15,message="不能超过最大的长度：13")
    private String chkBtch;

    @ApiModelProperty(value = "创建时间")
    private Date crteTime;

    @ApiModelProperty(value = "修改时间")
    private Date modiTime;

    @ApiModelProperty(value = "创建用户")
    @Size(max=20,message="不能超过最大长度：20")
    private String crteUser;

    @ApiModelProperty(value = "修改用户")
    @Size(max=20,message="不能超过最大长度：20")
    private String modiUser;

    @ApiModelProperty(value = "有效标志")
    @Size(max=5,message="不能超过最大长度：5")
    private String valiFlag;

    @ApiModelProperty(value = "唯一记录号")
    @Size(max=40,message="不能超过最大长度：40")
    private String rid;

    @ApiModelProperty(value = "备注")
    @Size(max=65535,message="不能超过最大长度：65535")
    private String dscr;


    public String getChkWater() {
        return chkWater;
    }

    public void setChkWater(String chkWater) {
        this.chkWater = chkWater;
    }

    public String getRuleNo() {
        return ruleNo;
    }

    public void setRuleNo(String ruleNo) {
        this.ruleNo = ruleNo;
    }

    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    public BigDecimal getTotlDataAmt() {
        return totlDataAmt;
    }

    public void setTotlDataAmt(BigDecimal totlDataAmt) {
        this.totlDataAmt = totlDataAmt;
    }

    public BigDecimal getAbnDataAmt() {
        return abnDataAmt;
    }

    public void setAbnDataAmt(BigDecimal abnDataAmt) {
        this.abnDataAmt = abnDataAmt;
    }

    public String getCrtProcer() {
        return crtProcer;
    }

    public void setCrtProcer(String crtProcer) {
        this.crtProcer = crtProcer;
    }

    public String getCrtOpnn() {
        return crtOpnn;
    }

    public void setCrtOpnn(String crtOpnn) {
        this.crtOpnn = crtOpnn;
    }

    public String getChkBtch() {
        return chkBtch;
    }

    public void setChkBtch(String chkBtch) {
        this.chkBtch = chkBtch;
    }

    public Date getCrteTime() {
        return crteTime;
    }

    public void setCrteTime(Date crteTime) {
        this.crteTime = crteTime;
    }

    public Date getModiTime() {
        return modiTime;
    }

    public void setModiTime(Date modiTime) {
        this.modiTime = modiTime;
    }

    public String getCrteUser() {
        return crteUser;
    }

    public void setCrteUser(String crteUser) {
        this.crteUser = crteUser;
    }

    public String getModiUser() {
        return modiUser;
    }

    public void setModiUser(String modiUser) {
        this.modiUser = modiUser;
    }

    public String getValiFlag() {
        return valiFlag;
    }

    public void setValiFlag(String valiFlag) {
        this.valiFlag = valiFlag;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getDscr() {
        return dscr;
    }

    public void setDscr(String dscr) {
        this.dscr = dscr;
    }


    @Override
    public String toString() {
        return "RuleCheckResult{" +
        "chkWater=" + chkWater +
        ", ruleNo=" + ruleNo +
        ", itemcode=" + itemcode +
        ", totlDataAmt=" + totlDataAmt +
        ", abnDataAmt=" + abnDataAmt +
        ", crtProcer=" + crtProcer +
        ", crtOpnn=" + crtOpnn +
        ", chkBtch=" + chkBtch +
        ", crteTime=" + crteTime +
        ", modiTime=" + modiTime +
        ", crteUser=" + crteUser +
        ", modiUser=" + modiUser +
        ", valiFlag=" + valiFlag +
        ", rid=" + rid +
        ", dscr=" + dscr +
        "}";
    }
}
