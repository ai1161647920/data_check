package cn.hsa.datacheck.ruleinfomanage.controller;


import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.hsa.hsaf.core.framework.web.WrapperResponse;

import cn.hsa.datacheck.ruleinfomanage.dto.RuleInfoBDTO;
import cn.hsa.datacheck.ruleinfomanage.dto.RuleInfoBQueryDTO;
import cn.hsa.datacheck.ruleinfomanage.service.RuleInfoBService;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Map;
import org.springframework.web.bind.annotation.RestController;
import cn.hsa.framework.commons.controller.BaseController;
import lombok.extern.slf4j.Slf4j;
import javax.validation.Valid;

/**
 * <p>
 * 规则表   前端控制器
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@RestController
@Slf4j
@Api(value = "/web/ruleinfomanage/ruleInfoB", tags = {"操作规则表  "})
@RequestMapping("/web/ruleinfomanage/ruleInfoB")
public class RuleInfoBController extends BaseController  {


    private final RuleInfoBService ruleInfoBService;

    @Autowired
    public RuleInfoBController(RuleInfoBService ruleInfoBService){
        this.ruleInfoBService = ruleInfoBService;
    }
    /**
     * 通过ID查询
     *
     * @param id ID
     * @return RuleInfoB
     */
    @ApiOperation(value = "通过ID查询", tags = {"通过ID查询"}, notes = "ID不能为空")
    @GetMapping("/{id}")
    public WrapperResponse<RuleInfoBDTO> get(@PathVariable String id) {
        return WrapperResponse.success(ruleInfoBService.queryById(id));
    }

    /**
     * 分页查询信息
     *
     * @param ruleInfoBQuery 分页对象
     * @return 分页对象
     */
    @ApiOperation(value = "分页查询")
    @GetMapping("/page")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "pageNumber", value = "页码", dataType = "string", paramType = "query", example = "1"),
        @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "long", paramType = "query", example = "10"),
        @ApiImplicitParam(name = "sort", value = "正序/倒序", dataType = "string", paramType = "query", example = "asc/desc"),
        @ApiImplicitParam(name = "order", value = "排序字段-属性名", dataType = "string", paramType = "query", example = "rid"),
        @ApiImplicitParam(name = "ruleInfoB", value = "规则表  查询条件", dataType = "RuleInfoBQueryDTO", paramType = "query")
    })
    public WrapperResponse<Map<String, Object>> page(
            Integer pageNumber,
            Integer pageSize,
            String sort,
            String order,
            RuleInfoBQueryDTO ruleInfoBQuery) {
        Map<String, Object> map = ruleInfoBService.queryPage(pageNumber, pageSize, sort, order, ruleInfoBQuery);
    	// 如不想动BO，此处可以再加工为前台需要的自定义对象，适配界面用，例如 (**DTO)map.get("result")
    	return WrapperResponse.success(map);
    }

    /**
     * 添加
     *
     * @param ruleInfoBDto 实体
     * @return success/false
     */
    @PostMapping
    @ApiOperation(value = "添加规则表  ")
    @ResponseStatus(HttpStatus.CREATED)
    public WrapperResponse<?> add(@Valid @RequestBody @ApiParam(name="规则表  ",value="传入json格式",required=true) RuleInfoBDTO ruleInfoBDto) {
        RuleInfoBDTO RuleInfoBDto = ruleInfoBService.save(ruleInfoBDto);
        return WrapperResponse.success(RuleInfoBDto);
    }

    /**
     * 测试连接
     *
     * @param ruleInfoBDto 实体
     * @return success/false
     */
    @PostMapping("/testConnect")
    @ApiOperation(value = "测试sql")
    public WrapperResponse<?> testConnect(@Valid @RequestBody @ApiParam(name="规则表",value="传入json格式",required=true) RuleInfoBDTO ruleInfoBDto) {
        RuleInfoBDTO RuleInfoBDto = ruleInfoBService.testConnect(ruleInfoBDto);
        if(ruleInfoBService == null){
            return WrapperResponse.fail("连接失败！");
        }else{
            return WrapperResponse.success(RuleInfoBDto);
        }

    }

    /**
     * 通过表名模糊查询,仅查询十条数据
     *
     * @param ruleInfoBDto RuleInfoBDTO
     * @return FixmedinsB
     */

    @PostMapping("/getTables")
    @ApiOperation(value = "通过模糊表名查询")
    public WrapperResponse<Map<String, Object>> getTables(@Valid @RequestBody @ApiParam(name="规则表",value="传入json格式",required=true) RuleInfoBDTO ruleInfoBDto) {
        Map<String, Object> map = ruleInfoBService.getTables(ruleInfoBDto);

        return WrapperResponse.success(map);
    }

    /**
     * 删除
     *
     * @param id ID
     * @return success/false
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除规则表  ")
    public WrapperResponse<?>  delete(@PathVariable String id) {
        ruleInfoBService.delete(id);
        return WrapperResponse.success("");
    }

    /**
     * 编辑
     *
     * @param ruleInfoBDto 实体
     * @return success/false
     */
    @PutMapping
    @ApiOperation(value = "编辑规则表  ")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public WrapperResponse<?> edit(@Valid @RequestBody @ApiParam(name="规则表  ",value="传入json格式",required=true) RuleInfoBDTO ruleInfoBDto) {
        RuleInfoBDTO RuleInfoBDto = ruleInfoBService.update(ruleInfoBDto);
        return WrapperResponse.success(RuleInfoBDto);
    }

    /**
     * 保存List
     *
     * @param ruleInfoBList 实体
     * @return success/false
     */
    @PostMapping("/saveList")
    @ApiOperation(value = "编辑规则表  ")
    public WrapperResponse<?> saveList(@Valid @RequestBody @ApiParam(name="规则表  ",value="传入json格式",required=true) List<RuleInfoBDTO> ruleInfoBList) {
        ruleInfoBService.saveBatch(ruleInfoBList);
        return WrapperResponse.success("");
    }
}
