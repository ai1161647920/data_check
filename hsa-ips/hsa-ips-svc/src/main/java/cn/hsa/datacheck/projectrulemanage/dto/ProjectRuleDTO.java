package cn.hsa.datacheck.projectrulemanage.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import javax.validation.constraints.Size;

/**
 * <p>
 * 项目规则表
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@ApiModel(value="ProjectRuleDTO", description="项目规则表")
public class ProjectRuleDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "规则组编号")
    @Size(max=40,message="不能超过最大长度：40")
    private String ruleGrpNo;

    @ApiModelProperty(value = "项目编号")
    @Size(max=40,message="不能超过最大长度：40")
    private String itemCode;

    @ApiModelProperty(value = "数据源编号")
    @Size(max=40,message="不能超过最大长度：40")
    private String dsNo;

    @ApiModelProperty(value = "创建时间")
    private Date crteTime;

    @ApiModelProperty(value = "修改时间")
    private Date modiTime;

    @ApiModelProperty(value = "创建用户")
    @Size(max=20,message="不能超过最大长度：20")
    private String crteUser;

    @ApiModelProperty(value = "修改用户")
    @Size(max=20,message="不能超过最大长度：20")
    private String modiUser;

    @ApiModelProperty(value = "有效标志")
    @Size(max=1,message="不能超过最大长度：1")
    private String valiFlag;

    @ApiModelProperty(value = "唯一记录号")
    @Size(max=40,message="不能超过最大长度：40")
    private String rid;


    @ApiModelProperty(value = "备注")
    @Size(max=65535,message="不能超过最大长度：65535")
    private String dscr;


    @Override
    public String toString() {
        return "ProjectRuleDTO{" +
                "ruleGrpNo='" + ruleGrpNo + '\'' +
                ", itemcode='" + itemCode + '\'' +
                ", dsNo='" + dsNo + '\'' +
                ", crteTime=" + crteTime +
                ", modiTime=" + modiTime +
                ", crteUser='" + crteUser + '\'' +
                ", modiUser='" + modiUser + '\'' +
                ", valiFlag='" + valiFlag + '\'' +
                ", rid='" + rid + '\'' +
                ", dscr='" + dscr + '\'' +
                '}';
    }
}
