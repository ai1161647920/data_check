package cn.hsa.datacheck.projectmanage.service;


import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.projectmanage.dto.ProjectInfoBDTO;
import cn.hsa.datacheck.projectmanage.dto.ProjectInfoBQueryDTO;
import cn.hsa.framework.commons.page.IQuery;
import cn.hsa.framework.commons.service.IBaseService;

/**
 * <p>
 * 项目信息表 服务类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
public interface ProjectInfoBService extends IBaseService {
	/**
	 * 项目信息表简单分页查询
	 *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名     	 
	 * @param projectInfoB 项目信息表
	 * @return
	 */
	Map<String,Object> queryPage(int pageNumber, int pageSize, String sort, String order, ProjectInfoBQueryDTO projectInfoB);

    /**
     * 查询所有信息
     *
     * @param projectInfoB 参数 ProjectInfoBQueryDTO 对象
     * @return 分页对象
     */
    List<ProjectInfoBDTO> queryAll(ProjectInfoBQueryDTO projectInfoB);

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return ProjectInfoB
     */
    ProjectInfoBDTO queryById(String id);

    /**
     * 添加
     *
     * @param projectInfoB DTO
     * @return ProjectInfoBDTO
     */
    ProjectInfoBDTO save(ProjectInfoBDTO projectInfoB);

    /**
     * 更新
     *
     * @param projectInfoB DTO
     * @return ProjectInfoBDTO
     */
    ProjectInfoBDTO update(ProjectInfoBDTO projectInfoB);

    /**
     * 删除
     *
     * @param id 主键
     * @return success/false
     */
    void delete(String id);


    /**
     * 更新
     *
     * @param projectInfoBs DTO
     * @return List<ProjectInfoBDTO>
     */
    List<ProjectInfoBDTO> updateBatch(List<ProjectInfoBDTO> projectInfoBs);
    /**
     * 保存
     *
     * @param projectInfoBs DTO
     * @return List<ProjectInfoBDTO>
     */
    List<ProjectInfoBDTO> saveBatch(List<ProjectInfoBDTO> projectInfoBs);



}

