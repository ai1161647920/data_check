package cn.hsa.datacheck.rulecheckresult.dto;

import cn.hsa.datacheck.rulecheckresult.entity.RuleCheckResultDO;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 转换
 */
public class RuleCheckResultDtoAssembler {

    private RuleCheckResultDtoAssembler() {
        // hide for utils
    }

    /**
     * 转换为DTO对象
     *
     * @param value
     * @return
     */
    public static RuleCheckResultDTO toRuleCheckResultDto(RuleCheckResultDO value) {
        if (value == null) {
            return null;
        }
        RuleCheckResultDTO dto = new RuleCheckResultDTO();
        BeanUtils.copyProperties(value, dto);
        return dto;
    }

    /**
     * 转换为实体对象
     *
     * @param dto
     * @return
     */
    public static RuleCheckResultDO toRuleCheckResultDo(RuleCheckResultDTO dto) {
        if (dto == null) {
            return null;
        }
        RuleCheckResultDO value = new RuleCheckResultDO();
        BeanUtils.copyProperties(dto, value);
        return value;
    }

    /**
     * 转换为DTO对象list
     *
     * @param ruleCheckResults
     * @return
     */
    public static List<RuleCheckResultDTO> toRuleCheckResultDtoList(List<RuleCheckResultDO> ruleCheckResults) {
        if (ruleCheckResults == null) {
            return null;
        }
        return ruleCheckResults.stream().map(RuleCheckResultDtoAssembler::toRuleCheckResultDto).collect(Collectors.toList());
    }

    /**
     * 转换为实体对象list
     *
     * @param dtos
     * @return
     */
    public static List<RuleCheckResultDO> toRuleCheckResultDoList(List<RuleCheckResultDTO> dtos) {
        if (dtos == null) {
            return null;
        }
        return dtos.stream().map(RuleCheckResultDtoAssembler::toRuleCheckResultDo).collect(Collectors.toList());
    }

}
