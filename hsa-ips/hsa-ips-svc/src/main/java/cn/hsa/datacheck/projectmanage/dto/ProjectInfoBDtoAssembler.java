package cn.hsa.datacheck.projectmanage.dto;

import cn.hsa.datacheck.projectmanage.entity.ProjectInfoBDO;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 转换
 */
public class ProjectInfoBDtoAssembler {

    private ProjectInfoBDtoAssembler() {
        // hide for utils
    }

    /**
     * 转换为DTO对象
     *
     * @param value
     * @return
     */
    public static ProjectInfoBDTO toProjectInfoBDto(ProjectInfoBDO value) {
        if (value == null) {
            return null;
        }
        ProjectInfoBDTO dto = new ProjectInfoBDTO();
        BeanUtils.copyProperties(value, dto);
        return dto;
    }

    /**
     * 转换为实体对象
     *
     * @param dto
     * @return
     */
    public static ProjectInfoBDO toProjectInfoBDo(ProjectInfoBDTO dto) {
        if (dto == null) {
            return null;
        }
        ProjectInfoBDO value = new ProjectInfoBDO();
        BeanUtils.copyProperties(dto, value);
        return value;
    }

    /**
     * 转换为DTO对象list
     *
     * @param projectInfoBs
     * @return
     */
    public static List<ProjectInfoBDTO> toProjectInfoBDtoList(List<ProjectInfoBDO> projectInfoBs) {
        if (projectInfoBs == null) {
            return null;
        }
        return projectInfoBs.stream().map(ProjectInfoBDtoAssembler::toProjectInfoBDto).collect(Collectors.toList());
    }

    /**
     * 转换为实体对象list
     *
     * @param dtos
     * @return
     */
    public static List<ProjectInfoBDO> toProjectInfoBDoList(List<ProjectInfoBDTO> dtos) {
        if (dtos == null) {
            return null;
        }
        return dtos.stream().map(ProjectInfoBDtoAssembler::toProjectInfoBDo).collect(Collectors.toList());
    }

}
