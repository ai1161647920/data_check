package cn.hsa.datacheck.rulecheckresult.common;

public class Constants {
    public static final String DATA_ID = "DCR_00001";
    public static final String MYSQL = "MYSQL";
    public static final String ORACLE = "ORACLE";
}
