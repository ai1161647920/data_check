package cn.hsa.datacheck.jobtaskmanage.service;


import cn.hsa.datacheck.jobtaskmanage.dto.JobTaskDTO;
import cn.hsa.datacheck.jobtaskmanage.dto.RuleExecInfoDto;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public interface CommonService {
    void addTable(String tableName);

    /**
     * 删除表
     * @param tableName
     */
    void delTable( String tableName);



    void insertByDynamicSql(String tableName, String dynamicSql);
}
