package cn.hsa.datacheck.jobtaskmanage.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * <p>
 * 定时任务表  
 * </p>
 *
 * @author pengli
 * @since 2021-07-26
 */
@ApiModel(value="JobTaskQueryDTO", description="定时任务表  ")
public class JobTaskQueryDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "任务编号")
    private String taskNo;

    @ApiModelProperty(value = "任务名称")
    private String taskName;

    @ApiModelProperty(value = "项目名称")
    private String itemname;

    @ApiModelProperty(value = "执行规则范围")
    private String exeRuleScp;

    @ApiModelProperty(value = "Cron表达式")
    private String jobCorn;

    @ApiModelProperty(value = "定时器状态")
    private String timrStas;

    @ApiModelProperty(value = "上次执行结果")
    private String lastExeRslt;

    @ApiModelProperty(value = "创建时间")
    private Date crteTime;

    @ApiModelProperty(value = "修改时间")
    private Date modiTime;

    @ApiModelProperty(value = "创建用户")
    private String crteUser;

    @ApiModelProperty(value = "修改用户")
    private String modiUser;

    @ApiModelProperty(value = "有效标志")
    private String valiFlag;

    @ApiModelProperty(value = "唯一记录号")
    private String rid;

    @ApiModelProperty(value = "备注")
    private String dscr;


    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public String getExeRuleScp() {
        return exeRuleScp;
    }

    public void setExeRuleScp(String exeRuleScp) {
        this.exeRuleScp = exeRuleScp;
    }

    public String getJobCorn() {
        return jobCorn;
    }

    public void setJobCorn(String jobCorn) {
        this.jobCorn = jobCorn;
    }

    public String getTimrStas() {
        return timrStas;
    }

    public void setTimrStas(String timrStas) {
        this.timrStas = timrStas;
    }

    public String getLastExeRslt() {
        return lastExeRslt;
    }

    public void setLastExeRslt(String lastExeRslt) {
        this.lastExeRslt = lastExeRslt;
    }

    public Date getCrteTime() {
        return crteTime;
    }

    public void setCrteTime(Long crteTime) {
        this.crteTime = crteTime != null ? new Date(crteTime) : null;
    }

    public Date getModiTime() {
        return modiTime;
    }

    public void setModiTime(Long modiTime) {
        this.modiTime = modiTime != null ? new Date(modiTime) : null;
    }

    public String getCrteUser() {
        return crteUser;
    }

    public void setCrteUser(String crteUser) {
        this.crteUser = crteUser;
    }

    public String getModiUser() {
        return modiUser;
    }

    public void setModiUser(String modiUser) {
        this.modiUser = modiUser;
    }

    public String getValiFlag() {
        return valiFlag;
    }

    public void setValiFlag(String valiFlag) {
        this.valiFlag = valiFlag;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getDscr() {
        return dscr;
    }

    public void setDscr(String dscr) {
        this.dscr = dscr;
    }


    @Override
    public String toString() {
        return "JobTask{" +
        "taskNo=" + taskNo +
        ", taskName=" + taskName +
        ", itemname=" + itemname +
        ", exeRuleScp=" + exeRuleScp +
        ", jobCorn=" + jobCorn +
        ", timrStas=" + timrStas +
        ", lastExeRslt=" + lastExeRslt +
        ", crteTime=" + crteTime +
        ", modiTime=" + modiTime +
        ", crteUser=" + crteUser +
        ", modiUser=" + modiUser +
        ", valiFlag=" + valiFlag +
        ", rid=" + rid +
        ", dscr=" + dscr +
        "}";
    }
}
