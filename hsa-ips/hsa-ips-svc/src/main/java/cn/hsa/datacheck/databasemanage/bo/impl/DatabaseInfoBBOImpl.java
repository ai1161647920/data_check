package cn.hsa.datacheck.databasemanage.bo.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import cn.hsa.datacheck.databasemanage.entity.DatabaseInfoBDO;
import cn.hsa.datacheck.databasemanage.dto.DatabaseInfoBDtoAssembler;
import cn.hsa.datacheck.databasemanage.dao.DatabaseInfoBDAO;
import cn.hsa.datacheck.databasemanage.bo.DatabaseInfoBBO;
import cn.hsa.framework.commons.page.IQuery;
import cn.hsa.framework.commons.page.QueryResult;
import cn.hsa.framework.commons.bo.BaseBO;

import cn.hsa.datacheck.databasemanage.dto.DatabaseInfoBQueryDTO;
import cn.hsa.datacheck.databasemanage.dto.DatabaseInfoBDTO;
import cn.hsa.framework.commons.util.OrgHelper;
import cn.hsa.framework.commons.util.SequenceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;


/**
 * <p>
 * 数据源信息表 服务实现类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Service("NeuDatabaseInfoBBOImpl")
@Slf4j
public class DatabaseInfoBBOImpl extends BaseBO implements DatabaseInfoBBO {

    @Autowired
    protected SequenceUtil sequenceUtil;

    private final DatabaseInfoBDAO databaseInfoBDAO;

    private final DatabaseInfoBCommonBOImpl databaseInfoBCommonBO;

    @Autowired
    public DatabaseInfoBBOImpl(DatabaseInfoBDAO databaseInfoBDAO, DatabaseInfoBCommonBOImpl databaseInfoBCommonBO) {
        this.databaseInfoBDAO = databaseInfoBDAO;
        this.databaseInfoBCommonBO = databaseInfoBCommonBO;
    }


    /**
     * 分页查询信息
     *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名
     * @param databaseInfoBQueryDto 参数 DTO 对象
     * @return 分页对象
     */
    @Override
    public Map<String, Object> queryByDTOPage(int pageNumber, int pageSize, String sort, String order, DatabaseInfoBQueryDTO databaseInfoBQueryDto) {
       
        //调用中台或DB
    	IQuery<DatabaseInfoBDO> page = QueryResult.of(pageNumber, pageSize, sort, order);
    	IQuery<DatabaseInfoBDO> da = databaseInfoBDAO.baseQueryByDTO(page, databaseInfoBQueryDto);
    	List<DatabaseInfoBDTO> databaseInfoBList = DatabaseInfoBDtoAssembler.toDatabaseInfoBDtoList(da.getResult());

    	Map<String,Object> rs = new HashMap<String, Object>();
    	rs.put("pageNumber", da.getPageNumber());
    	rs.put("pageSize", da.getPageSize());
    	rs.put("total", da.getRecordCount());
    	rs.put("result", databaseInfoBList);
    	return rs;
    	    	
	}

    /**
     * 分页查询信息
     *
     * @param databaseInfoBDto 参数 DTO 对象
     * @return 分页对象
     */
    @Override
    public List<DatabaseInfoBDTO> queryByDTO(DatabaseInfoBQueryDTO databaseInfoBDto) {
        List<DatabaseInfoBDO> databaseInfoBList = databaseInfoBDAO.baseQueryByDTO(databaseInfoBDto);
        return DatabaseInfoBDtoAssembler.toDatabaseInfoBDtoList(databaseInfoBList);
    }

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return DatabaseInfoB
     */
    @Override
    public DatabaseInfoBDTO getById(String id) {
        DatabaseInfoBDO databaseInfoB = databaseInfoBDAO.selectById(id);
        return DatabaseInfoBDtoAssembler.toDatabaseInfoBDto(databaseInfoB);
    }

    /**
     * 添加
     *
     * @param databaseInfoBDto DatabaseInfoBDTO
     * @return DatabaseInfoBDTO
     */
    @Override
    public DatabaseInfoBDTO save(DatabaseInfoBDTO databaseInfoBDto) {
        DatabaseInfoBDO databaseInfoB = DatabaseInfoBDtoAssembler.toDatabaseInfoBDo(databaseInfoBDto);
        Date date = new Date();
        databaseInfoB.setValiFlag("1");
        databaseInfoB.setCrteTime(date);
        databaseInfoB.setModiTime(date);
        databaseInfoB.setModiUser(OrgHelper.getUserAcct());
        databaseInfoB.setCrteUser(OrgHelper.getUserAcct());
        databaseInfoB.setRid(sequenceUtil.getRID());

        databaseInfoBCommonBO.saveEntity(databaseInfoB);
        return DatabaseInfoBDtoAssembler.toDatabaseInfoBDto(databaseInfoB);
    }

    /**
     * 测试连接
     *
     * @param databaseInfoBDto DatabaseInfoBDTO
     * @return DatabaseInfoBDTO
     */
    @Override
    public DatabaseInfoBDTO testConnect(DatabaseInfoBDTO databaseInfoBDto) {
        DatabaseInfoBDO databaseInfoB = DatabaseInfoBDtoAssembler.toDatabaseInfoBDo(databaseInfoBDto);
        Connection connection = null;
        try {
            Class.forName(databaseInfoB.getDriver());
            connection = DriverManager.getConnection(databaseInfoB.getConnectstring(), databaseInfoB.getUserName(), databaseInfoB.getPwd());
        } catch (Exception e) {
            return null;
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return DatabaseInfoBDtoAssembler.toDatabaseInfoBDto(databaseInfoB);
    }


    /**
     * 更新
     *
     * @param databaseInfoBDto DatabaseInfoBDTO
     * @return DatabaseInfoBDTO
     */
    @Override
    public DatabaseInfoBDTO updateById(DatabaseInfoBDTO databaseInfoBDto) {
        DatabaseInfoBDO databaseInfoB = DatabaseInfoBDtoAssembler.toDatabaseInfoBDo(databaseInfoBDto);

        databaseInfoB.setModiTime(new Date());
        databaseInfoB.setModiUser(OrgHelper.getUserAcct());
        databaseInfoBCommonBO.updateEntityById(databaseInfoB);
        return DatabaseInfoBDtoAssembler.toDatabaseInfoBDto(databaseInfoB);
    }

    /**
     * 更新
     *
     * @param databaseInfoBDtos List<DatabaseInfoBDTO>
     * @return List<DatabaseInfoBDTO>
     */
    @Override
    public List<DatabaseInfoBDTO> updateBatchById(List<DatabaseInfoBDTO> databaseInfoBDtos) {
        List<DatabaseInfoBDO> updateList = DatabaseInfoBDtoAssembler.toDatabaseInfoBDoList(databaseInfoBDtos);
        databaseInfoBCommonBO.updateEntityBatchById(updateList);
        return DatabaseInfoBDtoAssembler.toDatabaseInfoBDtoList(updateList);
    }

    /**
     * 保存
     *
     * @param databaseInfoBDtos List<DatabaseInfoBDTO>
     * @return List<DatabaseInfoBDTO>
     */
    @Override
    public List<DatabaseInfoBDTO> saveBatch(List<DatabaseInfoBDTO> databaseInfoBDtos) {
        List<DatabaseInfoBDO> saveList = DatabaseInfoBDtoAssembler.toDatabaseInfoBDoList(databaseInfoBDtos);
        databaseInfoBCommonBO.saveEntityBatch(saveList);
        return DatabaseInfoBDtoAssembler.toDatabaseInfoBDtoList(saveList);
    }

     /**
     * 逻辑删除
     *
     * @param id 主键
     */
     @Override
     public void delete(String id) {
        DatabaseInfoBDO databaseInfoB = new DatabaseInfoBDO();
        databaseInfoB.setDsNo(id);
        databaseInfoB.setValiFlag("0");
        databaseInfoBCommonBO.updateEntityById(databaseInfoB);
     }

}
