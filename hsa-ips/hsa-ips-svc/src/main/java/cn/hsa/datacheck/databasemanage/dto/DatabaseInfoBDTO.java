package cn.hsa.datacheck.databasemanage.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import javax.validation.constraints.Size;

/**
 * <p>
 * 数据源信息表
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@ApiModel(value="DatabaseInfoBDTO", description="数据源信息表")
public class DatabaseInfoBDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "数据源编号")
    @Size(max=40,message="不能超过最大长度：40")
    private String dsNo;

    @ApiModelProperty(value = "数据源")
    @Size(max=40,message="不能超过最大长度：40")
    private String ds;

    @ApiModelProperty(value = "数据源名称")
    @Size(max=100,message="不能超过最大长度：100")
    private String dsName;

    @ApiModelProperty(value = "所属项目")
    @Size(max=40,message="不能超过最大长度：40")
    private String item;

    @ApiModelProperty(value = "用户名  ")
    @Size(max=20,message="不能超过最大长度：20")
    private String userName;

    @ApiModelProperty(value = "密码")
    @Size(max=40,message="不能超过最大长度：40")
    private String pwd;

    @ApiModelProperty(value = "连接串")
    @Size(max=200,message="不能超过最大长度：200")
    private String connectstring;

    @ApiModelProperty(value = "驱动")
    @Size(max=200,message="不能超过最大长度：200")
    private String driver;

    @ApiModelProperty(value = "创建时间")
    private Date crteTime;

    @ApiModelProperty(value = "修改时间")
    private Date modiTime;

    @ApiModelProperty(value = "创建用户")
    @Size(max=20,message="不能超过最大长度：20")
    private String crteUser;

    @ApiModelProperty(value = "修改用户")
    @Size(max=20,message="不能超过最大长度：20")
    private String modiUser;

    @ApiModelProperty(value = "有效标志")
    @Size(max=1,message="不能超过最大长度：1")
    private String valiFlag;

    @ApiModelProperty(value = "唯一记录号")
    @Size(max=40,message="不能超过最大长度：40")
    private String rid;

    @ApiModelProperty(value = "描述")
    @Size(max=65535,message="不能超过最大长度：65535")
    private String dscr;


    public String getDsNo() {
        return dsNo;
    }

    public void setDsNo(String dsNo) {
        this.dsNo = dsNo;
    }

    public String getDs() {
        return ds;
    }

    public void setDs(String ds) {
        this.ds = ds;
    }

    public String getDsName() {
        return dsName;
    }

    public void setDsName(String dsName) {
        this.dsName = dsName;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getConnectstring() {
        return connectstring;
    }

    public void setConnectstring(String connectstring) {
        this.connectstring = connectstring;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public Date getCrteTime() {
        return crteTime;
    }

    public void setCrteTime(Date crteTime) {
        this.crteTime = crteTime;
    }

    public Date getModiTime() {
        return modiTime;
    }

    public void setModiTime(Date modiTime) {
        this.modiTime = modiTime;
    }

    public String getCrteUser() {
        return crteUser;
    }

    public void setCrteUser(String crteUser) {
        this.crteUser = crteUser;
    }

    public String getModiUser() {
        return modiUser;
    }

    public void setModiUser(String modiUser) {
        this.modiUser = modiUser;
    }

    public String getValiFlag() {
        return valiFlag;
    }

    public void setValiFlag(String valiFlag) {
        this.valiFlag = valiFlag;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getDscr() {
        return dscr;
    }

    public void setDscr(String dscr) {
        this.dscr = dscr;
    }


    @Override
    public String toString() {
        return "DatabaseInfoB{" +
        "dsNo=" + dsNo +
        ", ds=" + ds +
        ", dsName=" + dsName +
        ", item=" + item +
        ", userName=" + userName +
        ", pwd=" + pwd +
        ", connectstring=" + connectstring +
        ", driver=" + driver +
        ", crteTime=" + crteTime +
        ", modiTime=" + modiTime +
        ", crteUser=" + crteUser +
        ", modiUser=" + modiUser +
        ", valiFlag=" + valiFlag +
        ", rid=" + rid +
        ", dscr=" + dscr +
        "}";
    }
}
