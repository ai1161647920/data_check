package cn.hsa.datacheck.rulegroupmanage.bo.impl;

import cn.hsa.datacheck.rulegroupmanage.entity.RuleGroupBDO;
import cn.hsa.datacheck.rulegroupmanage.dao.RuleGroupBDAO;

import cn.hsa.framework.commons.bo.CommonBO;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 规则组   服务实现类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Service
public class RuleGroupBCommonBOImpl extends CommonBO<RuleGroupBDAO, RuleGroupBDO>  {


}
