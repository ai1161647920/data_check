package cn.hsa.datacheck.rulegroupmanage.controller;

import cn.hsa.datacheck.projectmanage.dto.ProjectInfoBDTO;
import cn.hsa.datacheck.projectmanage.dto.ProjectInfoBQueryDTO;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.hsa.hsaf.core.framework.web.WrapperResponse;
import cn.hsa.framework.commons.page.IQuery;
import cn.hsa.framework.commons.page.QueryResult;

import cn.hsa.datacheck.rulegroupmanage.dto.RuleGroupBDTO;
import cn.hsa.datacheck.rulegroupmanage.dto.RuleGroupBQueryDTO;
import cn.hsa.datacheck.rulegroupmanage.service.RuleGroupBService;
import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.web.bind.annotation.RestController;
import cn.hsa.framework.commons.controller.BaseController;
import lombok.extern.slf4j.Slf4j;
import javax.validation.Valid;

/**
 * <p>
 * 规则组   前端控制器
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@RestController
@Slf4j
@Api(value = "/web/rulegroupmanage/ruleGroupB", tags = {"操作规则组  "})
@RequestMapping("/web/rulegroupmanage/ruleGroupB")
public class RuleGroupBController extends BaseController  {


    private final RuleGroupBService ruleGroupBService;

    @Autowired
    public RuleGroupBController(RuleGroupBService ruleGroupBService){
        this.ruleGroupBService = ruleGroupBService;
    }
    /**
     * 通过ID查询
     *
     * @param id ID
     * @return RuleGroupB
     *//*
    @ApiOperation(value = "通过ID查询", tags = {"通过ID查询"}, notes = "ID不能为空")
    @GetMapping("/{id}")
    public WrapperResponse<RuleGroupBDTO> get(@PathVariable String id) {
        return WrapperResponse.success(ruleGroupBService.queryById(id));
    }*/



    /**
     * 通过ruleGrpNo查询
     *
     * @param ruleGrpNo ruleGrpNo
     * @return RuleGroupB
     */
   /* @ApiOperation(value = "通过ruleGrpNo查询", tags = {"通过ruleGrpNo查询"}, notes = "ruleGrpNo不能为空")
    @GetMapping("/ruleGrpNo/{ruleGrpNo}")
    public WrapperResponse<List<RuleGroupBDTO>> getByRuleGrpNo(@PathVariable String ruleGrpNo) {
        RuleGroupBQueryDTO ruleGroupBQueryDTO = new RuleGroupBQueryDTO();
        ruleGroupBQueryDTO.setRuleGrpNo(ruleGrpNo);
        return WrapperResponse.success(ruleGroupBService.baseQueryByDTO(ruleGroupBQueryDTO));
    }*/

    /**
     * 分页查询信息
     *
     * @param ruleGroupBQuery 分页对象
     * @return 分页对象
     */
    @ApiOperation(value = "分页查询")
    @GetMapping("/page")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "pageNumber", value = "页码", dataType = "string", paramType = "query", example = "1"),
        @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "long", paramType = "query", example = "10"),
        @ApiImplicitParam(name = "sort", value = "正序/倒序", dataType = "string", paramType = "query", example = "asc/desc"),
        @ApiImplicitParam(name = "order", value = "排序字段-属性名", dataType = "string", paramType = "query", example = "rid"),
        @ApiImplicitParam(name = "ruleGroupB", value = "规则组  查询条件", dataType = "RuleGroupBQueryDTO", paramType = "query")
    })


    public WrapperResponse<Map<String, Object>> page(
            Integer pageNumber,
            Integer pageSize,
            String sort,
            String order,
            RuleGroupBQueryDTO ruleGroupBQuery) {
        Map<String, Object> map = ruleGroupBService.queryPage(pageNumber, pageSize, sort, order, ruleGroupBQuery);
    	// 如不想动BO，此处可以再加工为前台需要的自定义对象，适配界面用，例如 (**DTO)map.get("result")
    	return WrapperResponse.success(map);
    }

    /**
     * 查询规则组信息
     */
    @ApiOperation(value = "查询规则组信息", tags = {"查询规则组信息"})
    @GetMapping("/getRules")
    public WrapperResponse<List<RuleGroupBDTO>> getRules() {
        return WrapperResponse.success(ruleGroupBService.queryAll(new RuleGroupBQueryDTO()));
    }

    /**
     *
     * @param ruleGroupBDto
     * @return
     */
    @PostMapping
    @ApiOperation(value = "添加规则组  ")
    @ResponseStatus(HttpStatus.CREATED)
    public WrapperResponse<?> add(@Valid @RequestBody @ApiParam(name="规则组  ",value="传入json格式",required=true) RuleGroupBDTO ruleGroupBDto) {
        RuleGroupBDTO RuleGroupBDto = ruleGroupBService.save(ruleGroupBDto);
        return WrapperResponse.success(RuleGroupBDto);
    }

    /**
     * 删除
     *
     * @param ruleGroupBDto ID
     * @return success/false
     */
    @DeleteMapping("/delete")
    @ApiOperation(value = "删除规则组  ")
    public WrapperResponse<?>  delete(@Valid @RequestBody @ApiParam(name="规则组  ",value="传入json格式",required=true) RuleGroupBDTO ruleGroupBDto) {
        ruleGroupBService.delete(ruleGroupBDto);
        return WrapperResponse.success("");
    }

    /**
     *
     * @param ruleGroupBDto
     * @return
     */
    @PutMapping
    @ApiOperation(value = "编辑规则组  ")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public WrapperResponse<?> edit(@Valid @RequestBody @ApiParam(name="规则组  ",value="传入json格式",required=true) RuleGroupBDTO ruleGroupBDto) {
        RuleGroupBDTO RuleGroupBDto = ruleGroupBService.update(ruleGroupBDto);
        return WrapperResponse.success(RuleGroupBDto);
    }

    /**
     * 保存List
     *
     * @param ruleGroupBList 实体
     * @return success/false
     */
    @PostMapping("/saveList")
    @ApiOperation(value = "编辑规则组  ")
    public WrapperResponse<?> saveList(@Valid @RequestBody @ApiParam(name="规则组  ",value="传入json格式",required=true) List<RuleGroupBDTO> ruleGroupBList) {
        ruleGroupBService.saveBatch(ruleGroupBList);
        return WrapperResponse.success("");
    }
}
