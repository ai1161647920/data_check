package cn.hsa.datacheck.projectrulemanage.bo.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.projectrulemanage.entity.ProjectRuleDO;
import cn.hsa.datacheck.projectrulemanage.dto.ProjectRuleDtoAssembler;
import cn.hsa.datacheck.projectrulemanage.dao.ProjectRuleDAO;
import cn.hsa.datacheck.projectrulemanage.bo.ProjectRuleBO;
import cn.hsa.framework.commons.page.IQuery;
import cn.hsa.framework.commons.page.QueryResult;
import cn.hsa.framework.commons.bo.BaseBO;

import cn.hsa.datacheck.projectrulemanage.dto.ProjectRuleQueryDTO;
import cn.hsa.datacheck.projectrulemanage.dto.ProjectRuleDTO;
import cn.hsa.framework.commons.util.OrgHelper;
import cn.hsa.framework.commons.util.SequenceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;


/**
 * <p>
 * 项目规则表 服务实现类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Service("NeuProjectRuleBOImpl")
@Slf4j
public class ProjectRuleBOImpl extends BaseBO implements ProjectRuleBO {
    @Autowired
    protected SequenceUtil sequenceUtil;

    private final ProjectRuleDAO projectRuleDAO;

    private final ProjectRuleCommonBOImpl projectRuleCommonBO;

    @Autowired
    public ProjectRuleBOImpl(ProjectRuleDAO projectRuleDAO, ProjectRuleCommonBOImpl projectRuleCommonBO) {
        this.projectRuleDAO = projectRuleDAO;
        this.projectRuleCommonBO = projectRuleCommonBO;
    }


    /**
     * 分页查询信息
     *
     * @param pageNumber     页码
     * @param pageSize       每页数量
     * @param sort           正序/倒序
     * @param order          排序字段-属性名
     * @param projectRuleQueryDto 参数 DTO 对象
     * @return 分页对象
     */
    @Override
    public Map<String, Object> queryByDTOPage(int pageNumber, int pageSize, String sort, String order, ProjectRuleQueryDTO projectRuleQueryDto) {

        //调用中台或DB
        IQuery<ProjectRuleDO> page = QueryResult.of(pageNumber, pageSize, sort, order);
        IQuery<ProjectRuleDO> da = projectRuleDAO.baseQueryByDTO(page, projectRuleQueryDto);
        List<ProjectRuleDTO> projectRuleList = ProjectRuleDtoAssembler.toProjectRuleDtoList(da.getResult());

        Map<String, Object> rs = new HashMap<String, Object>();
        rs.put("pageNumber", da.getPageNumber());
        rs.put("pageSize", da.getPageSize());
        rs.put("total", da.getRecordCount());
        rs.put("result", projectRuleList);
        return rs;

    }

    /**
     * 分页查询信息
     *
     * @param projectRuleDto 参数 DTO 对象
     * @return 分页对象
     */
    @Override
    public List<ProjectRuleDTO> queryByDTO(ProjectRuleQueryDTO projectRuleDto) {
        List<ProjectRuleDO> projectRuleList = projectRuleDAO.baseQueryByDTO(projectRuleDto);
        return ProjectRuleDtoAssembler.toProjectRuleDtoList(projectRuleList);
    }

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return ProjectRule
     */
    @Override
    public ProjectRuleDTO getById(String id) {
        ProjectRuleDO projectRule = projectRuleDAO.selectById(id);
        return ProjectRuleDtoAssembler.toProjectRuleDto(projectRule);
    }

    /**
     * 添加
     *
     * @param projectRuleDto ProjectRuleDTO
     * @return ProjectRuleDTO
     */
    @Override
    public ProjectRuleDTO save(ProjectRuleDTO projectRuleDto) {
        ProjectRuleDO projectRule = ProjectRuleDtoAssembler.toProjectRuleDo(projectRuleDto);

        Date date = new Date();
        projectRule.setValiFlag("1");
        projectRule.setCrteTime(date);
        projectRule.setModiTime(date);
        projectRule.setModiUser(OrgHelper.getUserAcct());
        projectRule.setCrteUser(OrgHelper.getUserAcct());
        projectRule.setRid(sequenceUtil.getRID());
        projectRuleCommonBO.saveEntity(projectRule);
        return ProjectRuleDtoAssembler.toProjectRuleDto(projectRule);
    }


    /**
     * 更新
     *
     * @param projectRuleDto ProjectRuleDTO
     * @return ProjectRuleDTO
     */
    @Override
    public ProjectRuleDTO updateById(ProjectRuleDTO projectRuleDto) {
        ProjectRuleDO projectRule = ProjectRuleDtoAssembler.toProjectRuleDo(projectRuleDto);
        projectRule.setModiTime(new Date());
        projectRule.setModiUser(OrgHelper.getUserAcct());
        projectRuleCommonBO.updateEntityById(projectRule);
        return ProjectRuleDtoAssembler.toProjectRuleDto(projectRule);
    }

    /**
     * 更新
     *
     * @param projectRuleDtos List<ProjectRuleDTO>
     * @return List<ProjectRuleDTO>
     */
    @Override
    public List<ProjectRuleDTO> updateBatchById(List<ProjectRuleDTO> projectRuleDtos) {
        List<ProjectRuleDO> updateList = ProjectRuleDtoAssembler.toProjectRuleDoList(projectRuleDtos);
        projectRuleCommonBO.updateEntityBatchById(updateList);
        return ProjectRuleDtoAssembler.toProjectRuleDtoList(updateList);
    }

    /**
     * 保存
     *
     * @param projectRuleDtos List<ProjectRuleDTO>
     * @return List<ProjectRuleDTO>
     */
    @Override
    public List<ProjectRuleDTO> saveBatch(List<ProjectRuleDTO> projectRuleDtos) {
        List<ProjectRuleDO> saveList = ProjectRuleDtoAssembler.toProjectRuleDoList(projectRuleDtos);
        projectRuleCommonBO.saveEntityBatch(saveList);
        return ProjectRuleDtoAssembler.toProjectRuleDtoList(saveList);
    }

    /**
     * 逻辑删除
     *
     * @param id 主键
     */
    @Override
    public void delete(String id) {
        ProjectRuleDO projectRule = new ProjectRuleDO();
        projectRule.setRuleGrpNo(id);
        projectRule.setValiFlag("0");
        projectRuleCommonBO.updateEntityById(projectRule);
    }

    @Override
    public ProjectRuleDTO getByRuleGrpNo(String ruleGrpNo) {
        ProjectRuleQueryDTO projectRuleQueryDTO = new ProjectRuleQueryDTO();
        projectRuleQueryDTO.setRuleGrpNo(ruleGrpNo);
        List<ProjectRuleDO> projectRuleDOS = projectRuleDAO.baseQueryByDTO(projectRuleQueryDTO);
        if (projectRuleDOS == null || projectRuleDOS.size() == 0) {
            return null;
        }
        return ProjectRuleDtoAssembler.toProjectRuleDto(projectRuleDOS.get(0));

    }


}
