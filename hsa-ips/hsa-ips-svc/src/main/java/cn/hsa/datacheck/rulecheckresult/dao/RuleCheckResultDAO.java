package cn.hsa.datacheck.rulecheckresult.dao;

import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.rulecheckresult.entity.RuleCheckResultDO;
import cn.hsa.datacheck.rulecheckresult.dto.RuleCheckResultQueryDTO;
import cn.hsa.framework.commons.dao.BaseDAO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.hsa.framework.commons.page.IQuery;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 项目规则校验结果汇总表 Mapper 接口
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Mapper
@Repository("NeuRuleCheckResultDAO")
public interface RuleCheckResultDAO extends BaseDAO<RuleCheckResultDO> {

    /**
     * 分页查询（根据 dto 条件）
     * @param page         分页查询条件（可以为 RowBounds.DEFAULT）
     * @param dto          表字段 RuleCheckResultQueryDTO 对象
     */
    IQuery<RuleCheckResultDO> baseQueryByDTO(IQuery<RuleCheckResultDO> page,@Param("dto") RuleCheckResultQueryDTO dto);

    /**
     * 不分页查询（根据 dto 条件）
     * @param dto          表字段 RuleCheckResultQueryDTO 对象
     */
    List<RuleCheckResultDO> baseQueryByDTO(@Param("dto") RuleCheckResultQueryDTO dto);
}
