package cn.hsa.datacheck.jobtaskmanage.common.constants;

public class Constant {

    /**
     * 有效标识，1为有效，0为无效
     */
    public static final String VALI_FLAG_1 = "1";
    /**
     * 有效标识，1为有效，0为无效
     */
    public static final String VALI_FLAG_0 = "0";

    //执行结果成功
    public static final String EXEC_RESULT_SUCCESS = "1";
    //执行结果失败
    public static final String EXEC_RESULT_FAIL = "0";
   //执行结果默认未知 默认-1
    public static final String EXEC_RESULT_DEF = "-1";


   //停止
    public static final String TIMR_STATUS_0 = "0";
    //启动
    public static final String TIMR_STATUS_1 = "1";


    //","分隔符
    public static final String SPLIT_RULESCP = ",";


    public static final String SELECT_COUNT = "SELECT count(1) FROM  ";

    public static final String SELECT_All_FIELD = "SELECT * FROM  ";

    public static final String AND = "AND";

    public static final String BLANK_SPACE = " ";

    public static final String T_WHERE = "  T WHERE ";


    public static final String INSERT_INTO = "INSERT INTO ";

    public static final String VALUES = "VALUES ";

    public static final String LEFT_CIR = "(";

    public static final String RIGHT_CIR = ")";

    /**
     * 定时器的状态：1（启动）
     */
    public static final String TIMR_STAS_START = "1";


    /**
     * 定时器的默认状态：0（停止）
     */
    public static final String TIMR_STAS_STOP = "0";

    /**
     * 定时器的状态：2（执行）
     */
    public static final String TIMR_STAS_EXEC = "2";




    /**
     * 执行操作
     */
    public static  final String JOB_TASK_EXEC_OPERATION = "EXECUTE";


    /**
     * 启动操作
     */
    public static  final String JOB_TASK_EXEC_START = "START";

}
