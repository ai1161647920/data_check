package cn.hsa.datacheck.ruleinfomanage.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * <p>
 * 规则表  
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@ApiModel(value="RuleInfoBQueryDTO", description="规则表  ")
public class RuleInfoBQueryDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "规则编号")
    private String ruleNo;

    @ApiModelProperty(value = "所属组")
    private String ruleGrpNo;

    @ApiModelProperty(value = "表用户")
    private String tabUser;

    @ApiModelProperty(value = "表名")
    private String tabName;

    @ApiModelProperty(value = "表注释")
    private String tabAtn;

    @ApiModelProperty(value = "校验SQL条件")
    private String chkSqlCond;

    @ApiModelProperty(value = "规则类型")
    private String ruleType;

    @ApiModelProperty(value = "审核标志")
    private String chkFlag;

    @ApiModelProperty(value = "创建时间")
    private Date crteTime;

    @ApiModelProperty(value = "修改时间")
    private Date modiTime;

    @ApiModelProperty(value = "创建用户")
    private String crteUser;

    @ApiModelProperty(value = "修改用户")
    private String modiUser;

    @ApiModelProperty(value = "有效标志")
    private String valiFlag;

    @ApiModelProperty(value = "唯一记录号")
    private String rid;

    @ApiModelProperty(value = "备注")
    private String dscr;

    @ApiModelProperty(value = "规则注释")
    private String ruleNote;

    @ApiModelProperty(value = "规则描述")
    private String ruleDec;


    public String getRuleNo() {
        return ruleNo;
    }

    public void setRuleNo(String ruleNo) {
        this.ruleNo = ruleNo;
    }

    public String getRuleGrpNo() {
        return ruleGrpNo;
    }

    public void setRuleGrpNo(String ruleGrpNo) {
        this.ruleGrpNo = ruleGrpNo;
    }

    public String getTabUser() {
        return tabUser;
    }

    public void setTabUser(String tabUser) {
        this.tabUser = tabUser;
    }

    public String getTabName() {
        return tabName;
    }

    public void setTabName(String tabName) {
        this.tabName = tabName;
    }

    public String getTabAtn() {
        return tabAtn;
    }

    public void setTabAtn(String tabAtn) {
        this.tabAtn = tabAtn;
    }

    public String getChkSqlCond() {
        return chkSqlCond;
    }

    public void setChkSqlCond(String chkSqlCond) {
        this.chkSqlCond = chkSqlCond;
    }

    public String getRuleType() {
        return ruleType;
    }

    public void setRuleType(String ruleType) {
        this.ruleType = ruleType;
    }

    public String getChkFlag() {
        return chkFlag;
    }

    public void setChkFlag(String chkFlag) {
        this.chkFlag = chkFlag;
    }

    public Date getCrteTime() {
        return crteTime;
    }

    public void setCrteTime(Long crteTime) {
        this.crteTime = crteTime != null ? new Date(crteTime) : null;
    }

    public Date getModiTime() {
        return modiTime;
    }

    public void setModiTime(Long modiTime) {
        this.modiTime = modiTime != null ? new Date(modiTime) : null;
    }

    public String getCrteUser() {
        return crteUser;
    }

    public void setCrteUser(String crteUser) {
        this.crteUser = crteUser;
    }

    public String getModiUser() {
        return modiUser;
    }

    public void setModiUser(String modiUser) {
        this.modiUser = modiUser;
    }

    public String getValiFlag() {
        return valiFlag;
    }

    public void setValiFlag(String valiFlag) {
        this.valiFlag = valiFlag;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getDscr() {
        return dscr;
    }

    public void setDscr(String dscr) {
        this.dscr = dscr;
    }

    public String getRuleNote() {
        return ruleNote;
    }

    public void setRuleNote(String ruleNote) {
        this.ruleNote = ruleNote;
    }

    public String getRuleDec() {
        return ruleDec;
    }

    public void setRuleDec(String ruleDec) {
        this.ruleDec = ruleDec;
    }


    @Override
    public String toString() {
        return "RuleInfoB{" +
        "ruleNo=" + ruleNo +
        ", ruleGrpNo=" + ruleGrpNo +
        ", tabUser=" + tabUser +
        ", tabName=" + tabName +
        ", tabAtn=" + tabAtn +
        ", chkSqlCond=" + chkSqlCond +
        ", ruleType=" + ruleType +
        ", chkFlag=" + chkFlag +
        ", crteTime=" + crteTime +
        ", modiTime=" + modiTime +
        ", crteUser=" + crteUser +
        ", modiUser=" + modiUser +
        ", valiFlag=" + valiFlag +
        ", rid=" + rid +
        ", dscr=" + dscr +
        ", ruleNote=" + ruleNote +
        ", ruleDec=" + ruleDec +
        "}";
    }
}
