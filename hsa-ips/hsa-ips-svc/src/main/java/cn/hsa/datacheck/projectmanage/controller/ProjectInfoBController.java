package cn.hsa.datacheck.projectmanage.controller;


import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.hsa.hsaf.core.framework.web.WrapperResponse;

import cn.hsa.datacheck.projectmanage.dto.ProjectInfoBDTO;
import cn.hsa.datacheck.projectmanage.dto.ProjectInfoBQueryDTO;
import cn.hsa.datacheck.projectmanage.service.ProjectInfoBService;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Map;
import org.springframework.web.bind.annotation.RestController;
import cn.hsa.framework.commons.controller.BaseController;
import lombok.extern.slf4j.Slf4j;
import javax.validation.Valid;

/**
 * <p>
 * 项目信息表 前端控制器
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@RestController
@Slf4j
@Api(value = "/web/projectmanage/projectInfoB", tags = {"操作项目信息表"})
@RequestMapping("/web/projectmanage/projectInfoB")
public class ProjectInfoBController extends BaseController  {


    private final ProjectInfoBService projectInfoBService;

    @Autowired
    public ProjectInfoBController(ProjectInfoBService projectInfoBService){
        this.projectInfoBService = projectInfoBService;
    }
    /**
     * 通过ID查询
     *
     * @param id ID
     * @return ProjectInfoB
     */
    @ApiOperation(value = "通过ID查询", tags = {"通过ID查询"}, notes = "ID不能为空")
    @GetMapping("/{id}")
    public WrapperResponse<ProjectInfoBDTO> get(@PathVariable String id) {
        return WrapperResponse.success(projectInfoBService.queryById(id));
    }

    /**
     * 分页查询信息
     *
     * @param projectInfoBQuery 分页对象
     * @return 分页对象
     */
    @ApiOperation(value = "分页查询")
    @GetMapping("/page")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "pageNumber", value = "页码", dataType = "string", paramType = "query", example = "1"),
        @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "long", paramType = "query", example = "10"),
        @ApiImplicitParam(name = "sort", value = "正序/倒序", dataType = "string", paramType = "query", example = "asc/desc"),
        @ApiImplicitParam(name = "order", value = "排序字段-属性名", dataType = "string", paramType = "query", example = "rid"),
        @ApiImplicitParam(name = "projectInfoB", value = "项目信息表查询条件", dataType = "ProjectInfoBQueryDTO", paramType = "query")
    })
    public WrapperResponse<Map<String, Object>> page(
            Integer pageNumber,
            Integer pageSize,
            String sort,
            String order,
            ProjectInfoBQueryDTO projectInfoBQuery) {
        Map<String, Object> map = projectInfoBService.queryPage(pageNumber, pageSize, sort, order, projectInfoBQuery);
    	// 如不想动BO，此处可以再加工为前台需要的自定义对象，适配界面用，例如 (**DTO)map.get("result")
    	return WrapperResponse.success(map);
    }

    /**
     * 添加
     *
     * @param projectInfoBDto 实体
     * @return success/false
     */
    @PostMapping
    @ApiOperation(value = "添加项目信息表")
    @ResponseStatus(HttpStatus.CREATED)
    public WrapperResponse<?> add(@Valid @RequestBody @ApiParam(name="项目信息表",value="传入json格式",required=true) ProjectInfoBDTO projectInfoBDto) {
        ProjectInfoBDTO ProjectInfoBDto = projectInfoBService.save(projectInfoBDto);
        return WrapperResponse.success(ProjectInfoBDto);
    }

    /**
     * 删除
     *
     * @param id ID
     * @return success/false
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除项目信息表")
    public WrapperResponse<?>  delete(@PathVariable String id) {
        projectInfoBService.delete(id);
        return WrapperResponse.success("");
    }

    /**
     * 编辑
     *
     * @param projectInfoBDto 实体
     * @return success/false
     */
    @PutMapping
    @ApiOperation(value = "编辑项目信息表")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public WrapperResponse<?> edit(@Valid @RequestBody @ApiParam(name="项目信息表",value="传入json格式",required=true) ProjectInfoBDTO projectInfoBDto) {
        ProjectInfoBDTO ProjectInfoBDto = projectInfoBService.update(projectInfoBDto);
        return WrapperResponse.success(ProjectInfoBDto);
    }

    /**
     * 查询项目信息
     *
     */
    @ApiOperation(value = "查询项目信息", tags = {"查询项目信息"})
    @GetMapping("/getItems")
    public WrapperResponse<List<ProjectInfoBDTO>> getItems() {
        return WrapperResponse.success(projectInfoBService.queryAll(new ProjectInfoBQueryDTO()));
    }

    /**
     * 保存List
     *
     * @param projectInfoBList 实体
     * @return success/false
     */
    @PostMapping("/saveList")
    @ApiOperation(value = "编辑项目信息表")
    public WrapperResponse<?> saveList(@Valid @RequestBody @ApiParam(name="项目信息表",value="传入json格式",required=true) List<ProjectInfoBDTO> projectInfoBList) {
        projectInfoBService.saveBatch(projectInfoBList);
        return WrapperResponse.success("");
    }
}
