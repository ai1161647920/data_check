package cn.hsa.datacheck.jobtaskmanage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import javax.validation.constraints.Size;

/**
 * <p>
 * 定时任务表  
 * </p>
 *
 * @author pengli
 * @since 2021-07-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("job_task")
@ApiModel(value="JobTask", description="定时任务表  ")
public class JobTaskDO extends Model<JobTaskDO> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "任务编号")
    @TableId("TASK_NO")
    private String taskNo;

    @ApiModelProperty(value = "任务名称")
    @TableField("TASK_NAME")
    private String taskName;

    @ApiModelProperty(value = "项目名称")
    @TableField("ITEMNAME")
    private String itemname;

    @ApiModelProperty(value = "执行规则范围")
    @TableField("EXE_RULE_SCP")
    private String exeRuleScp;

    @ApiModelProperty(value = "Cron表达式")
    @TableField("JOB_CORN")
    private String jobCorn;

    @ApiModelProperty(value = "定时器状态")
    @TableField("TIMR_STAS")
    private String timrStas;

    @ApiModelProperty(value = "上次执行结果")
    @TableField("LAST_EXE_RSLT")
    private String lastExeRslt;

    @ApiModelProperty(value = "创建时间")
    @TableField("CRTE_TIME")
    private Date crteTime;

    @ApiModelProperty(value = "修改时间")
    @TableField("MODI_TIME")
    private Date modiTime;

    @ApiModelProperty(value = "创建用户")
    @TableField("CRTE_USER")
    private String crteUser;

    @ApiModelProperty(value = "修改用户")
    @TableField("MODI_USER")
    private String modiUser;

    @ApiModelProperty(value = "有效标志")
    @TableField("VALI_FLAG")
    private String valiFlag;

    @ApiModelProperty(value = "唯一记录号")
    @TableField("RID")
    private String rid;

    @ApiModelProperty(value = "备注")
    @TableField("DSCR")
    private String dscr;


    @Override
    protected Serializable pkVal() {
        return this.taskNo;
    }

}
