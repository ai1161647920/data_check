package cn.hsa.datacheck.jobtaskmanage.bo.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.jobtaskmanage.bo.JobTaskBO;
import cn.hsa.datacheck.jobtaskmanage.common.constants.Constant;
import cn.hsa.datacheck.jobtaskmanage.dao.JobTaskDAO;
import cn.hsa.datacheck.jobtaskmanage.dto.JobTaskDTO;
import cn.hsa.datacheck.jobtaskmanage.dto.JobTaskDtoAssembler;
import cn.hsa.datacheck.jobtaskmanage.dto.JobTaskQueryDTO;
import cn.hsa.datacheck.jobtaskmanage.entity.JobTaskDO;
import cn.hsa.framework.commons.page.IQuery;
import cn.hsa.framework.commons.page.QueryResult;
import cn.hsa.framework.commons.bo.BaseBO;

import cn.hsa.framework.commons.util.SequenceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;


/**
 * <p>
 * 定时任务表   服务实现类
 * </p>
 *
 * @author pengli
 * @since 2021-07-26
 */
@Service("NeuJobTaskBOImpl")
@Slf4j
public class JobTaskBOImpl extends BaseBO implements JobTaskBO {

    @Autowired
    protected SequenceUtil sequenceUtil;

    private final JobTaskDAO jobTaskDAO;

    private final JobTaskCommonBOImpl jobTaskCommonBO;

    @Autowired
    public JobTaskBOImpl(JobTaskDAO jobTaskDAO, JobTaskCommonBOImpl jobTaskCommonBO) {
        this.jobTaskDAO = jobTaskDAO;
        this.jobTaskCommonBO = jobTaskCommonBO;
    }


    /**
     * 分页查询信息
     *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名
     * @param  jobTaskQueryDto DTO 对象
     * @return 分页对象
     */
    @Override
    public Map<String, Object> queryByDTOPage(int pageNumber, int pageSize, String sort, String order, JobTaskQueryDTO jobTaskQueryDto) {
       
        //调用中台或DB
    	IQuery<JobTaskDO> page = QueryResult.of(pageNumber, pageSize, sort, order);
        jobTaskQueryDto.setValiFlag(Constant.VALI_FLAG_1);
    	IQuery<JobTaskDO> da = jobTaskDAO.baseQueryByDTO(page, jobTaskQueryDto);
    	List<JobTaskDTO> jobTaskList = JobTaskDtoAssembler.toJobTaskDtoList(da.getResult());
    	Map<String,Object> rs = new HashMap<String, Object>();
    	rs.put("pageNumber", da.getPageNumber());
    	rs.put("pageSize", da.getPageSize());
    	rs.put("total", da.getRecordCount());
    	rs.put("result", jobTaskList);
    	return rs;
    	    	
	}

    /**
     * 分页查询信息
     *
     * @param jobTaskDto 参数 DTO 对象
     * @return 分页对象
     */
    @Override
    public List<JobTaskDTO> queryByDTO(JobTaskQueryDTO jobTaskDto) {
        List<JobTaskDO> jobTaskList = jobTaskDAO.baseQueryByDTO(jobTaskDto);
        return JobTaskDtoAssembler.toJobTaskDtoList(jobTaskList);
    }

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return JobTask
     */
    @Override
    public JobTaskDTO getById(String id) {
        JobTaskDO jobTask = jobTaskDAO.selectById(id);
        return JobTaskDtoAssembler.toJobTaskDto(jobTask);
    }

    /**
     * 添加
     *
     * @param jobTaskDto JobTaskDTO
     * @return JobTaskDTO
     */
    @Override
    public JobTaskDTO save(JobTaskDTO jobTaskDto) {
        JobTaskDO jobTask = JobTaskDtoAssembler.toJobTaskDo(jobTaskDto);
        jobTask.setRid(sequenceUtil.getRID());
        // 有效标识设置为有效
        jobTask.setValiFlag(Constant.VALI_FLAG_1);
        // 定时器状态默认为停止
        jobTask.setTimrStas(Constant.TIMR_STATUS_0);
        // 新增时，默认执行结果为-1
        jobTask.setLastExeRslt(Constant.EXEC_RESULT_DEF);
        Date date = new Date();
        jobTask.setCrteTime(date);
        jobTask.setModiTime(date);
        jobTaskCommonBO.saveEntity(jobTask);
        return JobTaskDtoAssembler.toJobTaskDto(jobTask);
    }


    /**
     * 更新
     *
     * @param jobTaskDto JobTaskDTO
     * @return JobTaskDTO
     */
    @Override
    public JobTaskDTO updateById(JobTaskDTO jobTaskDto) {
        JobTaskDO jobTask = JobTaskDtoAssembler.toJobTaskDo(jobTaskDto);
        jobTaskCommonBO.updateEntityById(jobTask);
        return JobTaskDtoAssembler.toJobTaskDto(jobTask);
    }

    /**
     * 更新
     *
     * @param jobTaskDtos List<JobTaskDTO>
     * @return List<JobTaskDTO>
     */
    @Override
    public List<JobTaskDTO> updateBatchById(List<JobTaskDTO> jobTaskDtos) {
        List<JobTaskDO> updateList = JobTaskDtoAssembler.toJobTaskDoList(jobTaskDtos);
        jobTaskCommonBO.updateEntityBatchById(updateList);
        return JobTaskDtoAssembler.toJobTaskDtoList(updateList);
    }

    /**
     * 保存
     *
     * @param jobTaskDtos List<JobTaskDTO>
     * @return List<JobTaskDTO>
     */
    @Override
    public List<JobTaskDTO> saveBatch(List<JobTaskDTO> jobTaskDtos) {
        List<JobTaskDO> saveList = JobTaskDtoAssembler.toJobTaskDoList(jobTaskDtos);
        jobTaskCommonBO.saveEntityBatch(saveList);
        return JobTaskDtoAssembler.toJobTaskDtoList(saveList);
    }

     /**
     * 逻辑删除
     *
     * @param id 主键
     */
     @Override
     public void delete(String id) {
        JobTaskDO jobTask = new JobTaskDO();
        jobTask.setTaskNo(id);
        jobTask.setValiFlag("0");
        jobTaskCommonBO.updateEntityById(jobTask);
     }

    @Override
    public JobTaskDTO queryByTaskNo(String taskNo) {

        JobTaskDO jobTask = jobTaskDAO.queryByTaskNo(taskNo);
        return JobTaskDtoAssembler.toJobTaskDto(jobTask);
    }

    @Override
    public List<JobTaskDTO> preSave(JobTaskDTO jobTaskDto) {
        List<JobTaskDO> jobTaskDOS = jobTaskDAO.preSave(jobTaskDto);
        return JobTaskDtoAssembler.toJobTaskDtoList(jobTaskDOS);
    }

}
