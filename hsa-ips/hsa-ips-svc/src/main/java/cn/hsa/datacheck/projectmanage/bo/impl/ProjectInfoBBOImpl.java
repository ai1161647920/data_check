package cn.hsa.datacheck.projectmanage.bo.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import cn.hsa.datacheck.projectmanage.entity.ProjectInfoBDO;
import cn.hsa.datacheck.projectmanage.dto.ProjectInfoBDtoAssembler;
import cn.hsa.datacheck.projectmanage.dao.ProjectInfoBDAO;
import cn.hsa.datacheck.projectmanage.bo.ProjectInfoBBO;
import cn.hsa.framework.commons.page.IQuery;
import cn.hsa.framework.commons.page.QueryResult;
import cn.hsa.framework.commons.bo.BaseBO;

import cn.hsa.datacheck.projectmanage.dto.ProjectInfoBQueryDTO;
import cn.hsa.datacheck.projectmanage.dto.ProjectInfoBDTO;
import cn.hsa.framework.commons.util.OrgHelper;
import cn.hsa.framework.commons.util.SequenceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;


/**
 * <p>
 * 项目信息表 服务实现类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Service("NeuProjectInfoBBOImpl")
@Slf4j
public class ProjectInfoBBOImpl extends BaseBO implements ProjectInfoBBO {

    @Autowired
    protected SequenceUtil sequenceUtil;

    private final ProjectInfoBDAO projectInfoBDAO;

    private final ProjectInfoBCommonBOImpl projectInfoBCommonBO;

    @Autowired
    public ProjectInfoBBOImpl(ProjectInfoBDAO projectInfoBDAO, ProjectInfoBCommonBOImpl projectInfoBCommonBO) {
        this.projectInfoBDAO = projectInfoBDAO;
        this.projectInfoBCommonBO = projectInfoBCommonBO;
    }


    /**
     * 分页查询信息
     *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名
     * @param projectInfoBDto 参数 DTO 对象
     * @return 分页对象
     */
    @Override
    public Map<String, Object> queryByDTOPage(int pageNumber, int pageSize, String sort, String order, ProjectInfoBQueryDTO projectInfoBQueryDto) {
       
        //调用中台或DB
    	IQuery<ProjectInfoBDO> page = QueryResult.of(pageNumber, pageSize, sort, order);
    	IQuery<ProjectInfoBDO> da = projectInfoBDAO.baseQueryByDTO(page, projectInfoBQueryDto);
    	List<ProjectInfoBDTO> projectInfoBList = ProjectInfoBDtoAssembler.toProjectInfoBDtoList(da.getResult());

    	Map<String,Object> rs = new HashMap<String, Object>();
    	rs.put("pageNumber", da.getPageNumber());
    	rs.put("pageSize", da.getPageSize());
    	rs.put("total", da.getRecordCount());
    	rs.put("result", projectInfoBList);
    	return rs;
    	    	
	}

    /**
     * 分页查询信息
     *
     * @param projectInfoBDto 参数 DTO 对象
     * @return 分页对象
     */
    @Override
    public List<ProjectInfoBDTO> queryByDTO(ProjectInfoBQueryDTO projectInfoBDto) {
        List<ProjectInfoBDO> projectInfoBList = projectInfoBDAO.baseQueryByDTO(projectInfoBDto);
        return ProjectInfoBDtoAssembler.toProjectInfoBDtoList(projectInfoBList);
    }

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return ProjectInfoB
     */
    @Override
    public ProjectInfoBDTO getById(String id) {
        ProjectInfoBDO projectInfoB = projectInfoBDAO.selectById(id);
        return ProjectInfoBDtoAssembler.toProjectInfoBDto(projectInfoB);
    }

    /**
     * 添加
     *
     * @param projectInfoBDto ProjectInfoBDTO
     * @return ProjectInfoBDTO
     */
    @Override
    public ProjectInfoBDTO save(ProjectInfoBDTO projectInfoBDto) {
        ProjectInfoBDO projectInfoB = ProjectInfoBDtoAssembler.toProjectInfoBDo(projectInfoBDto);
        Date date = new Date();
        projectInfoB.setValiFlag("1");
        projectInfoB.setCrteTime(date);
        projectInfoB.setModiTime(date);
        projectInfoB.setModiUser(OrgHelper.getUserAcct());
        projectInfoB.setCrteUser(OrgHelper.getUserAcct());
        projectInfoB.setRid(sequenceUtil.getRID());
        projectInfoBCommonBO.saveEntity(projectInfoB);
        return ProjectInfoBDtoAssembler.toProjectInfoBDto(projectInfoB);
    }


    /**
     * 更新
     *
     * @param projectInfoBDto ProjectInfoBDTO
     * @return ProjectInfoBDTO
     */
    @Override
    public ProjectInfoBDTO updateById(ProjectInfoBDTO projectInfoBDto) {
        ProjectInfoBDO projectInfoB = ProjectInfoBDtoAssembler.toProjectInfoBDo(projectInfoBDto);
        projectInfoB.setModiTime(new Date());
        projectInfoB.setModiUser(OrgHelper.getUserAcct());
        projectInfoBCommonBO.updateEntityById(projectInfoB);
        return ProjectInfoBDtoAssembler.toProjectInfoBDto(projectInfoB);
    }

    /**
     * 更新
     *
     * @param projectInfoBDtos List<ProjectInfoBDTO>
     * @return List<ProjectInfoBDTO>
     */
    @Override
    public List<ProjectInfoBDTO> updateBatchById(List<ProjectInfoBDTO> projectInfoBDtos) {
        List<ProjectInfoBDO> updateList = ProjectInfoBDtoAssembler.toProjectInfoBDoList(projectInfoBDtos);
        projectInfoBCommonBO.updateEntityBatchById(updateList);
        return ProjectInfoBDtoAssembler.toProjectInfoBDtoList(updateList);
    }

    /**
     * 保存
     *
     * @param projectInfoBDtos List<ProjectInfoBDTO>
     * @return List<ProjectInfoBDTO>
     */
    @Override
    public List<ProjectInfoBDTO> saveBatch(List<ProjectInfoBDTO> projectInfoBDtos) {
        List<ProjectInfoBDO> saveList = ProjectInfoBDtoAssembler.toProjectInfoBDoList(projectInfoBDtos);
        projectInfoBCommonBO.saveEntityBatch(saveList);
        return ProjectInfoBDtoAssembler.toProjectInfoBDtoList(saveList);
    }

     /**
     * 逻辑删除
     *
     * @param id 主键
     */
     @Override
     public void delete(String id) {
        ProjectInfoBDO projectInfoB = new ProjectInfoBDO();
        projectInfoB.setItemcode(id);
        projectInfoB.setValiFlag("0");
        projectInfoBCommonBO.updateEntityById(projectInfoB);
     }

}
