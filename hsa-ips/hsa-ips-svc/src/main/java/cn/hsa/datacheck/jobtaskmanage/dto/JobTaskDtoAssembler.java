package cn.hsa.datacheck.jobtaskmanage.dto;


import cn.hsa.datacheck.jobtaskmanage.entity.JobTaskDO;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 转换
 */
public class JobTaskDtoAssembler {

    private JobTaskDtoAssembler() {
        // hide for utils
    }

    /**
     * 转换为DTO对象
     *
     * @param value
     * @return
     */
    public static JobTaskDTO toJobTaskDto(JobTaskDO value) {
        if (value == null) {
            return null;
        }
        JobTaskDTO dto = new JobTaskDTO();
        BeanUtils.copyProperties(value, dto);
        return dto;
    }

    /**
     * 转换为实体对象
     *
     * @param dto
     * @return
     */
    public static JobTaskDO toJobTaskDo(JobTaskDTO dto) {
        if (dto == null) {
            return null;
        }
        JobTaskDO value = new JobTaskDO();
        BeanUtils.copyProperties(dto, value);
        return value;
    }

    /**
     * 转换为DTO对象list
     *
     * @param jobTasks
     * @return
     */
    public static List<JobTaskDTO> toJobTaskDtoList(List<JobTaskDO> jobTasks) {
        if (jobTasks == null) {
            return null;
        }
        return jobTasks.stream().map(JobTaskDtoAssembler::toJobTaskDto).collect(Collectors.toList());
    }

    /**
     * 转换为实体对象list
     *
     * @param dtos
     * @return
     */
    public static List<JobTaskDO> toJobTaskDoList(List<JobTaskDTO> dtos) {
        if (dtos == null) {
            return null;
        }
        return dtos.stream().map(JobTaskDtoAssembler::toJobTaskDo).collect(Collectors.toList());
    }

}
