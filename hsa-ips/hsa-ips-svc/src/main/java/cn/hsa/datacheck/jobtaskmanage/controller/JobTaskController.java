package cn.hsa.datacheck.jobtaskmanage.controller;

import cn.hsa.datacheck.jobtaskmanage.common.constants.Constant;
import cn.hsa.datacheck.jobtaskmanage.dto.JobTaskDTO;
import cn.hsa.datacheck.jobtaskmanage.dto.JobTaskQueryDTO;
import cn.hsa.datacheck.jobtaskmanage.service.JobTaskService;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.hsa.hsaf.core.framework.web.WrapperResponse;
import cn.hsa.framework.commons.page.IQuery;
import cn.hsa.framework.commons.page.QueryResult;

import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.web.bind.annotation.RestController;
import cn.hsa.framework.commons.controller.BaseController;
import lombok.extern.slf4j.Slf4j;
import javax.validation.Valid;

/**
 * <p>
 * 定时任务表   前端控制器
 * </p>
 *
 * @author pengli
 * @since 2021-07-26
 */
@RestController
@Slf4j
@Api(value = "/web/jobtaskmanage/jobTask", tags = {"操作定时任务表  "})
@RequestMapping("/web/jobtaskmanage/jobTask")
public class JobTaskController extends BaseController  {


    private final JobTaskService jobTaskService;

    @Autowired
    public JobTaskController(JobTaskService jobTaskService){
        this.jobTaskService = jobTaskService;
    }

    /**
     *
     * @param taskNo
     * @return
     */
    @ApiOperation(value = "通过taskNo查询", tags = {"通过ID查询"}, notes = "taskNo不能为空")
    @GetMapping("/{taskNo}")
    public WrapperResponse<JobTaskDTO> get(@PathVariable String taskNo) {
        return WrapperResponse.success(jobTaskService.queryByTaskNo(taskNo));
    }

    /**
     * 分页查询信息
     *
     * @param jobTaskQuery 分页对象
     * @return 分页对象
     */
    @ApiOperation(value = "分页查询")
    @GetMapping("/page")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "pageNumber", value = "页码", dataType = "string", paramType = "query", example = "1"),
        @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "long", paramType = "query", example = "10"),
        @ApiImplicitParam(name = "sort", value = "正序/倒序", dataType = "string", paramType = "query", example = "asc/desc"),
        @ApiImplicitParam(name = "order", value = "排序字段-属性名", dataType = "string", paramType = "query", example = "rid"),
        @ApiImplicitParam(name = "jobTask", value = "定时任务表  查询条件", dataType = "JobTaskQueryDTO", paramType = "query")
    })
    public WrapperResponse<Map<String, Object>> page(
            Integer pageNumber,
            Integer pageSize,
            String sort,
            String order,
            JobTaskQueryDTO jobTaskQuery) {
        Map<String, Object> map = jobTaskService.queryPage(pageNumber, pageSize, sort, order, jobTaskQuery);
    	// 如不想动BO，此处可以再加工为前台需要的自定义对象，适配界面用，例如 (**DTO)map.get("result")
    	return WrapperResponse.success(map);
    }

    /**
     * 添加
     * @param jobTaskDto
     * @return
     */
    @PostMapping
    @ApiOperation(value = "添加定时任务表  ")
    @ResponseStatus(HttpStatus.CREATED)
    public WrapperResponse<?> add(@Valid @RequestBody @ApiParam(name="定时任务表  ",value="传入json格式",required=true) JobTaskDTO jobTaskDto) {
        //添加前先检查是否已经添加
        List<JobTaskDTO> jobTaskDTOS = jobTaskService.preSave(jobTaskDto);
        if(jobTaskDTOS != null && jobTaskDTOS.size() > 0){
            return WrapperResponse.error(-1,"该任务编号、任务名称已存在",jobTaskDTOS);
        }
        JobTaskDTO JobTaskDto = jobTaskService.save(jobTaskDto);
        return WrapperResponse.success(JobTaskDto);
    }

    /**
     * 删除
     *
     * @param taskNo taskNo
     * @return success/false
     */
    @DeleteMapping("/{taskNo}")
    @ApiOperation(value = "删除定时任务表  ")
    public WrapperResponse<?>  delete(@PathVariable String taskNo) {
        JobTaskDTO jobTaskDTO = jobTaskService.queryById(taskNo);
        // 定时器状态为启动或者执行状态不能删除
        if(!jobTaskDTO.getTimrStas().equals(Constant.TIMR_STAS_STOP)){
            return WrapperResponse.fail("存在任务正在执行，不能删除");
        }
        jobTaskService.delete(taskNo);
        return WrapperResponse.success("");
    }

    /**
     * 编辑
     * @param jobTaskDto
     * @return
     */
    @PutMapping
    @ApiOperation(value = "编辑定时任务表  ")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public WrapperResponse<?> edit(@Valid @RequestBody @ApiParam(name="定时任务表  ",value="传入json格式",required=true) JobTaskDTO jobTaskDto) {
        JobTaskDTO JobTaskDto = jobTaskService.update(jobTaskDto);
        return WrapperResponse.success(JobTaskDto);
    }

    /**
     * 保存List
     *
     * @param jobTaskList 实体
     * @return success/false
     */
    @PostMapping("/saveList")
    @ApiOperation(value = "编辑定时任务表  ")
    public WrapperResponse<?> saveList(@Valid @RequestBody @ApiParam(name="定时任务表  ",value="传入json格式",required=true) List<JobTaskDTO> jobTaskList) {
        jobTaskService.saveBatch(jobTaskList);
        return WrapperResponse.success("");
    }
}
