package cn.hsa.datacheck.projectrulemanage.bo.impl;

import cn.hsa.datacheck.projectrulemanage.entity.ProjectRuleDO;
import cn.hsa.datacheck.projectrulemanage.dao.ProjectRuleDAO;

import cn.hsa.framework.commons.bo.CommonBO;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 项目规则表 服务实现类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Service
public class ProjectRuleCommonBOImpl extends CommonBO<ProjectRuleDAO, ProjectRuleDO>  {


}
