package cn.hsa.datacheck.jobtaskmanage.service;


import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


import cn.hsa.datacheck.jobtaskmanage.dto.JobTaskDTO;
import cn.hsa.datacheck.jobtaskmanage.dto.JobTaskQueryDTO;
import cn.hsa.datacheck.jobtaskmanage.dto.RuleExecInfoDto;
import cn.hsa.framework.commons.page.IQuery;
import cn.hsa.framework.commons.service.IBaseService;

/**
 * <p>
 * 定时任务表   服务类
 * </p>
 *
 * @author pengli
 * @since 2021-07-26
 */
public interface JobTaskService extends IBaseService {
	/**
	 * 定时任务表  简单分页查询
	 *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名     	 
	 * @param jobTask 定时任务表  
	 * @return
	 */
	Map<String,Object> queryPage(int pageNumber, int pageSize, String sort, String order, JobTaskQueryDTO jobTask);

    /**
     * 查询所有信息
     *
     * @param jobTask 参数 JobTaskQueryDTO 对象
     * @return 分页对象
     */
    List<JobTaskDTO> queryAll(JobTaskQueryDTO jobTask);

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return JobTask
     */
    JobTaskDTO queryById(String id);

    /**
     * 添加
     *
     * @param jobTask DTO
     * @return JobTaskDTO
     */
    JobTaskDTO save(JobTaskDTO jobTask);

    /**
     * 更新
     *
     * @param jobTask DTO
     * @return JobTaskDTO
     */
    JobTaskDTO update(JobTaskDTO jobTask);

    /**
     * 删除
     *
     * @param id 主键
     * @return success/false
     */
    void delete(String id);


    /**
     * 更新
     *
     * @param jobTasks DTO
     * @return List<JobTaskDTO>
     */
    List<JobTaskDTO> updateBatch(List<JobTaskDTO> jobTasks);
    /**
     * 保存
     *
     * @param jobTasks DTO
     * @return List<JobTaskDTO>
     */
    List<JobTaskDTO> saveBatch(List<JobTaskDTO> jobTasks);


    /**
     * 根据taskNo查询
     * @param taskNo
     * @return
     */
    JobTaskDTO queryByTaskNo(String taskNo);

    /**
     * 根据任务编号、任务名称判断是否已经存在
     * @param jobTaskDto
     */
    List<JobTaskDTO>  preSave(JobTaskDTO jobTaskDto);

    ConcurrentHashMap<String, RuleExecInfoDto> constructRuleExecParam(JobTaskDTO dto);


}

