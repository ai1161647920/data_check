package cn.hsa.datacheck.ruleinfomanage.service.impl;

import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.ruleinfomanage.dto.RuleInfoBQueryDTO;
import cn.hsa.datacheck.ruleinfomanage.dto.RuleInfoBDTO;
import cn.hsa.datacheck.ruleinfomanage.bo.RuleInfoBBO;
import cn.hsa.datacheck.ruleinfomanage.service.RuleInfoBService;
import cn.hsa.framework.commons.page.IQuery;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import cn.hsa.framework.commons.service.BaseService;
/**
 * <p>
 * 规则表   服务实现类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Service("NeuRuleInfoBServiceImpl")
public class RuleInfoBServiceImpl extends BaseService implements RuleInfoBService {


    private final RuleInfoBBO ruleInfoBBO;

    @Autowired
    public RuleInfoBServiceImpl(RuleInfoBBO ruleInfoBBO){
        this.ruleInfoBBO = ruleInfoBBO;
    }
    
    /**
     * 规则表  简单分页查询
     *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名  
     * @param ruleInfoB 规则表  
     * @return
     */
    @Override
	public Map<String, Object> queryPage(int pageNumber, int pageSize, String sort, String order, RuleInfoBQueryDTO ruleInfoB) {
    	return ruleInfoBBO.queryByDTOPage(pageNumber, pageSize, sort, order, ruleInfoB);
	}

    /**
     * 查询所有信息
     *
     * @param ruleInfoB 参数 RuleInfoBQueryDTO 对象
     * @return 对象List
     */
    @Override
    public List<RuleInfoBDTO> queryAll(RuleInfoBQueryDTO ruleInfoB) {
        return ruleInfoBBO.queryByDTO(ruleInfoB);
    }

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return RuleInfoB
     */
    @Override
    public RuleInfoBDTO queryById(String id) {
        return ruleInfoBBO.getById(id);
    }

    @Override
    public List<RuleInfoBDTO> queryByRuleGrpNo(String ruleGrpNo) {
        return ruleInfoBBO.queryByRuleGrpNo(ruleGrpNo);
    }


    /**
     * 添加
     *
     * @param ruleInfoB DTO
     * @return RuleInfoBDTO
     */
    @Override
    public RuleInfoBDTO save(RuleInfoBDTO ruleInfoB) {
        return ruleInfoBBO.save(ruleInfoB);
    }

    @Override
    public RuleInfoBDTO testConnect(RuleInfoBDTO ruleInfoB) {
        return ruleInfoBBO.testConnect(ruleInfoB);
    }

    @Override
    public Map<String, Object> getTables(RuleInfoBDTO ruleInfoBDto) {
        return ruleInfoBBO.getTables(ruleInfoBDto);
    }

    /**
     * 更新
     *
     * @param ruleInfoB DTO
     * @return RuleInfoBDTO
     */
    @Override
    public RuleInfoBDTO update(RuleInfoBDTO ruleInfoB) {
        return ruleInfoBBO.updateById(ruleInfoB);
    }

    /**
     * 删除
     *
     * @param id 主键
     */
    @Override
    public void delete(String id) {
        ruleInfoBBO.delete(id);
    }

    /**
     * 批量更新
     *
     * @param ruleInfoBs 实体
     * @return List<RuleInfoBDTO>
     */
    @Override
    public List<RuleInfoBDTO> updateBatch(List<RuleInfoBDTO> ruleInfoBs) {
        return ruleInfoBBO.updateBatchById(ruleInfoBs);
    }

    /**
     * 批量新增
     *
     * @param ruleInfoBs 实体
     * @return List<RuleInfoBDTO>
     */
    @Override
    public List<RuleInfoBDTO> saveBatch(List<RuleInfoBDTO> ruleInfoBs) {
        return ruleInfoBBO.saveBatch(ruleInfoBs);
    }

}
