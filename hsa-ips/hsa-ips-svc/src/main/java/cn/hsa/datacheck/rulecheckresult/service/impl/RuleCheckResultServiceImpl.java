package cn.hsa.datacheck.rulecheckresult.service.impl;

import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.rulecheckresult.dto.RuleCheckResultQueryDTO;
import cn.hsa.datacheck.rulecheckresult.dto.RuleCheckResultDTO;
import cn.hsa.datacheck.rulecheckresult.bo.RuleCheckResultBO;
import cn.hsa.datacheck.rulecheckresult.service.RuleCheckResultService;
import cn.hsa.framework.commons.page.IQuery;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import cn.hsa.framework.commons.service.BaseService;
/**
 * <p>
 * 项目规则校验结果汇总表 服务实现类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Service("NeuRuleCheckResultServiceImpl")
public class RuleCheckResultServiceImpl extends BaseService implements RuleCheckResultService {


    private final RuleCheckResultBO ruleCheckResultBO;

    @Autowired
    public RuleCheckResultServiceImpl(RuleCheckResultBO ruleCheckResultBO){
        this.ruleCheckResultBO = ruleCheckResultBO;
    }
    
    /**
     * 项目规则校验结果汇总表简单分页查询
     *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名  
     * @param ruleCheckResult 项目规则校验结果汇总表
     * @return
     */
    @Override
	public Map<String, Object> queryPage(int pageNumber, int pageSize, String sort, String order, RuleCheckResultQueryDTO ruleCheckResult) {
    	return ruleCheckResultBO.queryByDTOPage(pageNumber, pageSize, sort, order, ruleCheckResult);
	}

    @Override
    public Map<String, Object> queryPage(int pageNumber, int pageSize, String sort, String order,  String ruleNo,String chkBtch,String tableName) {
        return ruleCheckResultBO.queryByDTOPage(pageNumber, pageSize, sort, order, ruleNo,chkBtch,tableName);
    }

    /**
     * 查询所有信息
     *
     * @param ruleCheckResult 参数 RuleCheckResultQueryDTO 对象
     * @return 对象List
     */
    @Override
    public List<RuleCheckResultDTO> queryAll(RuleCheckResultQueryDTO ruleCheckResult) {
        return ruleCheckResultBO.queryByDTO(ruleCheckResult);
    }

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return RuleCheckResult
     */
    @Override
    public RuleCheckResultDTO queryById(String id) {
        return ruleCheckResultBO.getById(id);
    }


    /**
     * 添加
     *
     * @param ruleCheckResult DTO
     * @return RuleCheckResultDTO
     */
    @Override
    public RuleCheckResultDTO save(RuleCheckResultDTO ruleCheckResult) {
        return ruleCheckResultBO.save(ruleCheckResult);
    }

    /**
     * 更新
     *
     * @param ruleCheckResult DTO
     * @return RuleCheckResultDTO
     */
    @Override
    public RuleCheckResultDTO update(RuleCheckResultDTO ruleCheckResult) {
        return ruleCheckResultBO.updateById(ruleCheckResult);
    }

    /**
     * 删除
     *
     * @param id 主键
     */
    @Override
    public void delete(String id) {
        ruleCheckResultBO.delete(id);
    }

    /**
     * 批量更新
     *
     * @param ruleCheckResults 实体
     * @return List<RuleCheckResultDTO>
     */
    @Override
    public List<RuleCheckResultDTO> updateBatch(List<RuleCheckResultDTO> ruleCheckResults) {
        return ruleCheckResultBO.updateBatchById(ruleCheckResults);
    }

    /**
     * 批量新增
     *
     * @param ruleCheckResults 实体
     * @return List<RuleCheckResultDTO>
     */
    @Override
    public List<RuleCheckResultDTO> saveBatch(List<RuleCheckResultDTO> ruleCheckResults) {
        return ruleCheckResultBO.saveBatch(ruleCheckResults);
    }

}
