package cn.hsa.datacheck.rulecheckresult.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import javax.validation.constraints.Size;

/**
 * <p>
 * 项目规则校验结果汇总表
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("rule_check_result")
@ApiModel(value="RuleCheckResult", description="项目规则校验结果汇总表")
public class RuleCheckResultDO extends Model<RuleCheckResultDO> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "校验流水")
    @TableId("CHK_WATER")
    private String chkWater;

    @ApiModelProperty(value = "规则编号")
    @TableField("RULE_NO")
    private String ruleNo;

    @ApiModelProperty(value = "校验SQL条件")
    @TableField("CHK_SQL_COND")
    private String chkSqlCond;

    @ApiModelProperty(value = "项目编号")
    @TableField("ITEMCODE")
    private String itemcode;

    @ApiModelProperty(value = "总数据量")
    @TableField("TOTL_DATA_AMT")
    private BigDecimal totlDataAmt;

    @ApiModelProperty(value = "异常数据量")
    @TableField("ABN_DATA_AMT")
    private BigDecimal abnDataAmt;

    @ApiModelProperty(value = "当前处理人")
    @TableField("CRT_PROCER")
    private String crtProcer;

    @ApiModelProperty(value = "当前处理意见")
    @TableField("CRT_OPNN")
    private String crtOpnn;

    @ApiModelProperty(value = "校验批次")
    @TableField("CHK_BTCH")
    private String chkBtch;

    @ApiModelProperty(value = "创建时间")
    @TableField("CRTE_TIME")
    private Date crteTime;

    @ApiModelProperty(value = "修改时间")
    @TableField("MODI_TIME")
    private Date modiTime;

    @ApiModelProperty(value = "创建用户")
    @TableField("CRTE_USER")
    private String crteUser;

    @ApiModelProperty(value = "修改用户")
    @TableField("MODI_USER")
    private String modiUser;

    @ApiModelProperty(value = "有效标志")
    @TableField("VALI_FLAG")
    private String valiFlag;

    @ApiModelProperty(value = "唯一记录号")
    @TableField("RID")
    private String rid;

    @ApiModelProperty(value = "备注")
    @TableField("DSCR")
    private String dscr;


    @Override
    protected Serializable pkVal() {
        return this.chkWater;
    }

}
