package cn.hsa.datacheck.projectmanage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import javax.validation.constraints.Size;

/**
 * <p>
 * 项目信息表
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("project_info_b")
@ApiModel(value="ProjectInfoB", description="项目信息表")
public class ProjectInfoBDO extends Model<ProjectInfoBDO> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "项目编号")
    @TableId("ITEMCODE")
    private String itemcode;

    @ApiModelProperty(value = "项目名称")
    @TableField("ITEMNAME")
    private String itemname;

    @ApiModelProperty(value = "创建时间")
    @TableField("CRTE_TIME")
    private Date crteTime;

    @ApiModelProperty(value = "修改时间")
    @TableField("MODI_TIME")
    private Date modiTime;

    @ApiModelProperty(value = "创建用户")
    @TableField("CRTE_USER")
    private String crteUser;

    @ApiModelProperty(value = "修改用户")
    @TableField("MODI_USER")
    private String modiUser;

    @ApiModelProperty(value = "有效标志")
    @TableField("VALI_FLAG")
    private String valiFlag;

    @ApiModelProperty(value = "唯一记录号")
    @TableField("RID")
    private String rid;

    @ApiModelProperty(value = "描述")
    @TableField("DSCR")
    private String dscr;


    @Override
    protected Serializable pkVal() {
        return this.itemcode;
    }

}
