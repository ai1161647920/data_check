package cn.hsa.datacheck.jobtaskmanage.bo.impl;


import cn.hsa.datacheck.jobtaskmanage.bo.CommonBO;
import cn.hsa.common.dao.CommonDao;
import com.baomidou.dynamic.datasource.annotation.DS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
@DS("slave")
@Component
public class CommonBOImpl implements CommonBO {

    @Autowired
    private CommonDao commonDao;

    /**
     * 创建表
     * @param tableName
     */
    @DS("slave")
    @Override
    public void addTable(String tableName) {
        commonDao.addTable(tableName);
    }

    /**
     * 删除表
     * @param tableName
     */
    @DS("slave")
    @Override
    public void delTable(String tableName) {
        commonDao.delTable(tableName);
    }

    @DS("slave")
    @Override
    public void insertByDynamicSql(String tableName, String dynamicSql) {
        commonDao.insertByDynamicSql(tableName, dynamicSql);
    }
}
