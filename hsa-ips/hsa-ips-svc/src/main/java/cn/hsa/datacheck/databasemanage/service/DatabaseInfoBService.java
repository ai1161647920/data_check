package cn.hsa.datacheck.databasemanage.service;


import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.databasemanage.dto.DatabaseInfoBDTO;
import cn.hsa.datacheck.databasemanage.dto.DatabaseInfoBQueryDTO;
import cn.hsa.framework.commons.page.IQuery;
import cn.hsa.framework.commons.service.IBaseService;

/**
 * <p>
 * 数据源信息表 服务类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
public interface DatabaseInfoBService extends IBaseService {
	/**
	 * 数据源信息表简单分页查询
	 *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名     	 
	 * @param databaseInfoB 数据源信息表
	 * @return
	 */
	Map<String,Object> queryPage(int pageNumber, int pageSize, String sort, String order, DatabaseInfoBQueryDTO databaseInfoB);

    /**
     * 查询所有信息
     *
     * @param databaseInfoB 参数 DatabaseInfoBQueryDTO 对象
     * @return 分页对象
     */
    List<DatabaseInfoBDTO> queryAll(DatabaseInfoBQueryDTO databaseInfoB);

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return DatabaseInfoB
     */
    DatabaseInfoBDTO queryById(String id);

    /**
     * 添加
     *
     * @param databaseInfoB DTO
     * @return DatabaseInfoBDTO
     */
    DatabaseInfoBDTO save(DatabaseInfoBDTO databaseInfoB);

    /**
     * 测试连接
     *
     * @param databaseInfoB DTO
     * @return DatabaseInfoBDTO
     */
    DatabaseInfoBDTO testConnect(DatabaseInfoBDTO databaseInfoB);

    /**
     * 更新
     *
     * @param databaseInfoB DTO
     * @return DatabaseInfoBDTO
     */
    DatabaseInfoBDTO update(DatabaseInfoBDTO databaseInfoB);

    /**
     * 删除
     *
     * @param id 主键
     * @return success/false
     */
    void delete(String id);


    /**
     * 更新
     *
     * @param databaseInfoBs DTO
     * @return List<DatabaseInfoBDTO>
     */
    List<DatabaseInfoBDTO> updateBatch(List<DatabaseInfoBDTO> databaseInfoBs);
    /**
     * 保存
     *
     * @param databaseInfoBs DTO
     * @return List<DatabaseInfoBDTO>
     */
    List<DatabaseInfoBDTO> saveBatch(List<DatabaseInfoBDTO> databaseInfoBs);



}

