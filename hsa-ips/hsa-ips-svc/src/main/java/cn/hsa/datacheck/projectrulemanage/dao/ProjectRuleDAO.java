package cn.hsa.datacheck.projectrulemanage.dao;

import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.projectrulemanage.entity.ProjectRuleDO;
import cn.hsa.datacheck.projectrulemanage.dto.ProjectRuleQueryDTO;
import cn.hsa.framework.commons.dao.BaseDAO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.hsa.framework.commons.page.IQuery;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 项目规则表 Mapper 接口
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Mapper
@Repository("NeuProjectRuleDAO")
public interface ProjectRuleDAO extends BaseDAO<ProjectRuleDO> {

    /**
     * 分页查询（根据 dto 条件）
     * @param page         分页查询条件（可以为 RowBounds.DEFAULT）
     * @param dto          表字段 ProjectRuleQueryDTO 对象
     */
    IQuery<ProjectRuleDO> baseQueryByDTO(IQuery<ProjectRuleDO> page,@Param("dto") ProjectRuleQueryDTO dto);

    /**
     * 不分页查询（根据 dto 条件）
     * @param dto          表字段 ProjectRuleQueryDTO 对象
     */
    List<ProjectRuleDO> baseQueryByDTO(@Param("dto") ProjectRuleQueryDTO dto);
}
