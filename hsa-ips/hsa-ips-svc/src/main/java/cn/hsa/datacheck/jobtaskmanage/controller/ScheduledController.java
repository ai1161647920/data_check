package cn.hsa.datacheck.jobtaskmanage.controller;

import cn.hsa.datacheck.jobtaskmanage.common.constants.Constant;
import cn.hsa.datacheck.jobtaskmanage.dto.JobTaskDTO;
import cn.hsa.datacheck.jobtaskmanage.dto.RuleExecInfoDto;
import cn.hsa.datacheck.jobtaskmanage.service.JobTaskService;
import cn.hsa.datacheck.jobtaskmanage.task.JobTask;
import cn.hsa.datacheck.jobtaskmanage.util.ScheduledFutureHolder;
import cn.hsa.hsaf.core.framework.web.WrapperResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.redisson.executor.CronExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
@RestController
@Slf4j
@Api(value = "/web/jobtaskmanage/scheduled", tags = {"定时任务的操作  "})
@RequestMapping("/web/jobtaskmanage/scheduled")
public class ScheduledController {
    @Autowired
    @Qualifier("jobTaskThreadPoolTaskScheduler")
    private ThreadPoolTaskScheduler threadPoolTaskScheduler;
    @Autowired
    private JobTaskService jobTaskService;

    //存储任务执行的包装类
    private static Map<String, ScheduledFutureHolder> scheduleMap = new ConcurrentHashMap();

    public static Map<String, ScheduledFutureHolder> getScheduleMap() {
        return scheduleMap;
    }

    /**
     *启动任务
     * 如果不想手动触发任务可以使用 @PostConstruct注解来启动
     */

    @PostMapping("/start")
    @ApiOperation(value = "启动定时任务")
    public WrapperResponse<?> startTask(@Valid @RequestBody @ApiParam(name="定时任务参数  ",value="传入json格式",required=true) JobTaskDTO jobTaskDto)  {
        if(jobTaskDto.getJobCorn() == null ||  !CronExpression.isValidExpression(jobTaskDto.getJobCorn())){
            log.info("=========corn参数非法,taskNo:" + jobTaskDto.getTaskNo());
            return WrapperResponse.fail("参数无效");
        }

        try {
            // 执行周期
            String corn = jobTaskDto.getJobCorn();
            ConcurrentHashMap<String, RuleExecInfoDto> ruleExecParamMap = jobTaskService.constructRuleExecParam(jobTaskDto);
            //将任务交给任务调度器执行
            ScheduledFuture<?> schedule = threadPoolTaskScheduler.schedule(new JobTask(ruleExecParamMap, jobTaskDto, Constant.JOB_TASK_EXEC_START), new CronTrigger(corn));
            //将任务包装成ScheduledFutureHolder
            ScheduledFutureHolder scheduledFutureHolder = new ScheduledFutureHolder();
            scheduledFutureHolder.setScheduledFuture(schedule);
            //scheduledFutureHolder.setRunnableClass(helloTask.getClass());
            scheduledFutureHolder.setCorn(corn);
            scheduledFutureHolder.setTaskNo(jobTaskDto.getTaskNo());
            //将任务信息放入scheduleMap
            scheduleMap.put(scheduledFutureHolder.getTaskNo(),scheduledFutureHolder);
            //将定时器的状态修改为启动状态
            jobTaskDto.setTimrStas(Constant.TIMR_STAS_START);
            jobTaskDto.setModiTime(new Date());
            jobTaskService.update(jobTaskDto);
            return WrapperResponse.success("success");
        } catch (Exception e) {
         log.error("启动定时任务失败，", e);
        }
           return WrapperResponse.fail("fail");
    }


    @PostMapping("/execute")
    @ApiOperation(value = "定时任务立即执行")
    public WrapperResponse<?> executeTask(@Valid @RequestBody @ApiParam(name="定时任务参数  ",value="传入json格式",required=true) JobTaskDTO jobTaskDto)  {
        if(jobTaskDto.getJobCorn() == null ||  !CronExpression.isValidExpression(jobTaskDto.getJobCorn())){
            log.info("=========corn参数非法,taskNo:" + jobTaskDto.getTaskNo());
            return WrapperResponse.fail("参数无效");
        }
        try {
            ConcurrentHashMap<String, RuleExecInfoDto> ruleExecParamMap = jobTaskService.constructRuleExecParam(jobTaskDto);
            //将任务交给任务调度器执行
            threadPoolTaskScheduler.execute(new JobTask(ruleExecParamMap, jobTaskDto, Constant.JOB_TASK_EXEC_OPERATION));
            //将定时器的状态修改为启动状态
            jobTaskDto.setTimrStas(Constant.TIMR_STAS_EXEC);
            jobTaskDto.setModiTime(new Date());
            jobTaskService.update(jobTaskDto);
            return WrapperResponse.success("success");
        } catch (Exception e) {
            log.error("定时任务立即执行失败，", e);
        }
        return WrapperResponse.fail("fail");

    }


    @PostMapping("/stop")
    @ApiOperation(value = "定时任务停止")
    public  WrapperResponse<?> stopTask(@Valid @RequestBody @ApiParam(name="定时任务参数  ",value="传入json格式",required=true) JobTaskDTO jobTaskDto)  {
        String taskNo = jobTaskDto.getTaskNo();
        if(taskNo == null ||!scheduleMap.containsKey(taskNo)){
            log.info("定时任务停止失败，TaskNo非法");
            return WrapperResponse.fail("参数无效");
        }

        try {
            ScheduledFuture<?> scheduledFuture = scheduleMap.get(taskNo).getScheduledFuture();
            if(scheduledFuture!=null){
                scheduledFuture.cancel(true);
            }
            //将定时器的状态修改为停止状态
            jobTaskDto.setTimrStas(Constant.TIMR_STAS_STOP);
            jobTaskDto.setModiTime(new Date());
            jobTaskService.update(jobTaskDto);
            return WrapperResponse.success("success");
        } catch (Exception e) {
           log.error("定时任务停止停止失败", e);
        }
        return WrapperResponse.fail("fail");

    }

    /**
     * 查询所有的任务
     */
    @RequestMapping("/queryTask")
    public void queryTask(){
        scheduleMap.forEach((k,v)->{
            System.out.println(k+"  "+v);
        });
    }

    /**
     *
     * @param taskNo
     */
    @RequestMapping("/stop/{taskNo}")
    public void stopTask(@PathVariable String taskNo){
        if(scheduleMap.containsKey(taskNo)){//如果包含这个任务
            ScheduledFuture<?> scheduledFuture = scheduleMap.get(taskNo).getScheduledFuture();
            if(scheduledFuture!=null){
                scheduledFuture.cancel(true);
            }
        }
    }


    /**
     * 重启任务，修改任务的触发时间
     * @param className
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    @RequestMapping("/restart/{className}")
    public void restartTask(@PathVariable String className) throws InstantiationException, IllegalAccessException {
        if(scheduleMap.containsKey(className)){//如果包含这个任务


            //这里的corn可以通过前端动态传过来
            String corn = "0/50 * * * *  ?";
            ScheduledFutureHolder scheduledFutureHolder = scheduleMap.get(className);
            ScheduledFuture<?> scheduledFuture = scheduledFutureHolder.getScheduledFuture();
            if(scheduledFuture!=null){
                //先停掉任务
                scheduledFuture.cancel(true);

                //修改触发时间重新启动任务
                Runnable runnable = scheduledFutureHolder.getRunnableClass().newInstance();

                ScheduledFuture<?> schedule = threadPoolTaskScheduler.schedule(runnable, new CronTrigger(corn));

                scheduledFutureHolder.setScheduledFuture(schedule);
                scheduledFutureHolder.setCorn(corn);

                scheduleMap.put(scheduledFutureHolder.getRunnableClass().getName(),scheduledFutureHolder);
            }
        }
    }
}

