package cn.hsa.datacheck.ruleinfomanage.bo.impl;

import java.sql.*;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.databasemanage.dao.DatabaseInfoBDAO;
import cn.hsa.datacheck.databasemanage.entity.DatabaseInfoBDO;
import cn.hsa.datacheck.jobtaskmanage.common.constants.Constant;
import cn.hsa.datacheck.jobtaskmanage.common.util.CommonUtil;
import cn.hsa.datacheck.projectrulemanage.dao.ProjectRuleDAO;
import cn.hsa.datacheck.projectrulemanage.entity.ProjectRuleDO;
import cn.hsa.datacheck.rulecheckresult.common.Constants;
import cn.hsa.datacheck.ruleinfomanage.entity.RuleInfoBDO;
import cn.hsa.datacheck.ruleinfomanage.dto.RuleInfoBDtoAssembler;
import cn.hsa.datacheck.ruleinfomanage.dao.RuleInfoBDAO;
import cn.hsa.datacheck.ruleinfomanage.bo.RuleInfoBBO;
import cn.hsa.framework.commons.page.IQuery;
import cn.hsa.framework.commons.page.QueryResult;
import cn.hsa.framework.commons.bo.BaseBO;

import cn.hsa.datacheck.ruleinfomanage.dto.RuleInfoBQueryDTO;
import cn.hsa.datacheck.ruleinfomanage.dto.RuleInfoBDTO;
import cn.hsa.framework.commons.util.OrgHelper;
import cn.hsa.framework.commons.util.SequenceUtil;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;


/**
 * <p>
 * 规则表   服务实现类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Service("NeuRuleInfoBBOImpl")
@Slf4j
public class RuleInfoBBOImpl extends BaseBO implements RuleInfoBBO {
    @Autowired
    protected SequenceUtil sequenceUtil;

    private final RuleInfoBDAO ruleInfoBDAO;
    private final ProjectRuleDAO projectRuleDAO;

    private final DatabaseInfoBDAO databaseInfoBDAO;

    private final RuleInfoBCommonBOImpl ruleInfoBCommonBO;

    @Autowired
    public RuleInfoBBOImpl(RuleInfoBDAO ruleInfoBDAO, RuleInfoBCommonBOImpl ruleInfoBCommonBO,ProjectRuleDAO projectRuleDAO,DatabaseInfoBDAO databaseInfoBDAO) {
        this.ruleInfoBDAO = ruleInfoBDAO;
        this.ruleInfoBCommonBO = ruleInfoBCommonBO;
        this.projectRuleDAO = projectRuleDAO;
        this.databaseInfoBDAO = databaseInfoBDAO;
    }


    /**
     * 分页查询信息
     *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名
     * @param ruleInfoBQueryDto 参数 DTO 对象
     * @return 分页对象
     */
    @Override
    public Map<String, Object> queryByDTOPage(int pageNumber, int pageSize, String sort, String order, RuleInfoBQueryDTO ruleInfoBQueryDto) {
       
        //调用中台或DB
    	IQuery<RuleInfoBDO> page = QueryResult.of(pageNumber, pageSize, sort, order);
    	IQuery<RuleInfoBDO> da = ruleInfoBDAO.baseQueryByDTO(page, ruleInfoBQueryDto);
    	List<RuleInfoBDTO> ruleInfoBList = RuleInfoBDtoAssembler.toRuleInfoBDtoList(da.getResult());

    	Map<String,Object> rs = new HashMap<String, Object>();
    	rs.put("pageNumber", da.getPageNumber());
    	rs.put("pageSize", da.getPageSize());
    	rs.put("total", da.getRecordCount());
    	rs.put("result", ruleInfoBList);
    	return rs;
    	    	
	}

    /**
     * 分页查询信息
     *
     * @param ruleInfoBDto 参数 DTO 对象
     * @return 分页对象
     */
    @Override
    public List<RuleInfoBDTO> queryByDTO(RuleInfoBQueryDTO ruleInfoBDto) {
        List<RuleInfoBDO> ruleInfoBList = ruleInfoBDAO.baseQueryByDTO(ruleInfoBDto);
        return RuleInfoBDtoAssembler.toRuleInfoBDtoList(ruleInfoBList);
    }

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return RuleInfoB
     */
    @Override
    public RuleInfoBDTO getById(String id) {
        RuleInfoBDO ruleInfoB = ruleInfoBDAO.selectById(id);
        return RuleInfoBDtoAssembler.toRuleInfoBDto(ruleInfoB);
    }

    /**
     * 添加
     *
     * @param ruleInfoBDto RuleInfoBDTO
     * @return RuleInfoBDTO
     */
    @Override
    public RuleInfoBDTO save(RuleInfoBDTO ruleInfoBDto) {
        RuleInfoBDO ruleInfoB = RuleInfoBDtoAssembler.toRuleInfoBDo(ruleInfoBDto);
        Date date = new Date();
        ruleInfoB.setValiFlag("1");
        ruleInfoB.setCrteTime(date);
        ruleInfoB.setModiTime(date);
        ruleInfoB.setModiUser(OrgHelper.getUserAcct());
        ruleInfoB.setCrteUser(OrgHelper.getUserAcct());
        ruleInfoB.setRid(sequenceUtil.getRID());
        ruleInfoBCommonBO.saveEntity(ruleInfoB);
        return RuleInfoBDtoAssembler.toRuleInfoBDto(ruleInfoB);
    }


    /**
     * 添加
     *
     * @param ruleInfoBDto RuleInfoBDTO
     * @return map
     */
    @Override
    public Map<String, Object> getTables(RuleInfoBDTO ruleInfoBDto) {
        RuleInfoBDO ruleInfoB = RuleInfoBDtoAssembler.toRuleInfoBDo(ruleInfoBDto);
        ProjectRuleDO projectRule = projectRuleDAO.selectById(ruleInfoB.getRuleGrpNo());
        DatabaseInfoBDO databaseInfoB = databaseInfoBDAO.selectById(projectRule.getDsNo());
        Map<String,Object> rs = new HashMap<String, Object>();
        boolean success = false;
        if(databaseInfoB == null){
            rs.put("success", success);
            rs.put("msg", "数据源不存在！");
            rs.put("result", "");
            return rs;
        }

        Connection connection = null;
        Statement stmt = null;
        JSONArray queryResults;
        try {
            Class.forName(databaseInfoB.getDriver());
            connection = DriverManager.getConnection(databaseInfoB.getConnectstring(), databaseInfoB.getUserName(), databaseInfoB.getPwd());
            stmt = connection.createStatement();
            StringBuffer  sql = new StringBuffer();
            switch (databaseInfoB.getDs()){
                case Constants.ORACLE:
                    sql.append("select TABLE_NAME tableName,COMMENTS comments from user_tab_comments WHERE TABLE_NAME like '%") ;
                    sql.append(ruleInfoBDto.getTabName());
                    sql.append("%' AND ROWNUM < 11");
                    break;
                default ://默认mysql
                    String schema = databaseInfoB.getConnectstring().split("/")[3].split("\\?")[0];
                    sql.append("SELECT TABLE_NAME tableName,TABLE_COMMENT comments FROM information_schema.TABLES WHERE table_schema='") ;
                    sql.append(schema);
                    sql.append("' AND table_name LIKE '%");
                    sql.append(ruleInfoBDto.getTabName());
                    sql.append("%' LIMIT 0,10");
                    break;


            }
            ResultSet resultSet = stmt.executeQuery(sql.toString());
            queryResults = CommonUtil.resultSetToJson(resultSet);

            success = true;
        } catch (Exception e) {
            rs.put("success", success);
            rs.put("msg", "执行异常！");
            rs.put("result", "");
            return rs;
        } finally {
            try {
                if(stmt != null){
                    stmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

        rs.put("success", success);
        rs.put("msg", "执行成功！");
        rs.put("result", queryResults);
        return rs;
    }


    /**
     * 添加
     *
     * @param ruleInfoBDto RuleInfoBDTO
     * @return RuleInfoBDTO
     */
    @Override
    public RuleInfoBDTO testConnect(RuleInfoBDTO ruleInfoBDto) {
        RuleInfoBDO ruleInfoB = RuleInfoBDtoAssembler.toRuleInfoBDo(ruleInfoBDto);
        ProjectRuleDO projectRule = projectRuleDAO.selectById(ruleInfoB.getRuleGrpNo());
        DatabaseInfoBDO databaseInfoB = databaseInfoBDAO.selectById(projectRule.getDsNo());
        if(databaseInfoB == null){
            return null;
        }
        Connection connection = null;
        Statement stmt = null;
        try {
            Class.forName(databaseInfoB.getDriver());
            connection = DriverManager.getConnection(databaseInfoB.getConnectstring(), databaseInfoB.getUserName(), databaseInfoB.getPwd());
            stmt = connection.createStatement();
            StringBuffer  CountSql = new StringBuffer(Constant.SELECT_COUNT).append(ruleInfoB.getChkSqlCond());
            stmt.executeQuery(CountSql.toString());
        } catch (Exception e) {
            return null;
        } finally {
            try {
                if(stmt != null){
                    stmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return RuleInfoBDtoAssembler.toRuleInfoBDto(ruleInfoB);
    }


    /**
     * 更新
     *
     * @param ruleInfoBDto RuleInfoBDTO
     * @return RuleInfoBDTO
     */
    @Override
    public RuleInfoBDTO updateById(RuleInfoBDTO ruleInfoBDto) {
        RuleInfoBDO ruleInfoB = RuleInfoBDtoAssembler.toRuleInfoBDo(ruleInfoBDto);
        ruleInfoB.setModiTime(new Date());
        ruleInfoB.setModiUser(OrgHelper.getUserAcct());
        ruleInfoBCommonBO.updateEntityById(ruleInfoB);
        return RuleInfoBDtoAssembler.toRuleInfoBDto(ruleInfoB);
    }

    /**
     * 更新
     *
     * @param ruleInfoBDtos List<RuleInfoBDTO>
     * @return List<RuleInfoBDTO>
     */
    @Override
    public List<RuleInfoBDTO> updateBatchById(List<RuleInfoBDTO> ruleInfoBDtos) {
        List<RuleInfoBDO> updateList = RuleInfoBDtoAssembler.toRuleInfoBDoList(ruleInfoBDtos);
        ruleInfoBCommonBO.updateEntityBatchById(updateList);
        return RuleInfoBDtoAssembler.toRuleInfoBDtoList(updateList);
    }

    /**
     * 保存
     *
     * @param ruleInfoBDtos List<RuleInfoBDTO>
     * @return List<RuleInfoBDTO>
     */
    @Override
    public List<RuleInfoBDTO> saveBatch(List<RuleInfoBDTO> ruleInfoBDtos) {
        List<RuleInfoBDO> saveList = RuleInfoBDtoAssembler.toRuleInfoBDoList(ruleInfoBDtos);
        ruleInfoBCommonBO.saveEntityBatch(saveList);
        return RuleInfoBDtoAssembler.toRuleInfoBDtoList(saveList);
    }

     /**
     * 逻辑删除
     *
     * @param id 主键
     */
     @Override
     public void delete(String id) {
        RuleInfoBDO ruleInfoB = new RuleInfoBDO();
        ruleInfoB.setRuleNo(id);
        ruleInfoB.setValiFlag("0");
        ruleInfoBCommonBO.updateEntityById(ruleInfoB);
     }

    @Override
    public List<RuleInfoBDTO> queryByRuleGrpNo(String ruleGrpNo) {
        RuleInfoBQueryDTO ruleInfoBQueryDTO = new RuleInfoBQueryDTO();
        ruleInfoBQueryDTO.setRuleGrpNo(ruleGrpNo);
        List<RuleInfoBDO> ruleInfoBDOS = ruleInfoBDAO.baseQueryByDTO(ruleInfoBQueryDTO);
        return RuleInfoBDtoAssembler.toRuleInfoBDtoList(ruleInfoBDOS);
    }

}
