package cn.hsa.datacheck.jobtaskmanage.common.scheduledService;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadPoolExecutor;
@Slf4j
@Component
public class ScheduledService {
      @Autowired
      @Qualifier("jobTaskThreadPoolTaskScheduler")
      private ThreadPoolTaskScheduler myThreadPoolTaskScheduler;

        @Bean(name = "jobTaskThreadPoolTaskScheduler")
        public ThreadPoolTaskScheduler getMyThreadPoolTaskScheduler() {
            ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
            taskScheduler.setPoolSize(10);
            taskScheduler.setThreadNamePrefix("Scheduled-task");
            taskScheduler.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
            //调度器shutdown被调用时等待当前被调度的任务完成
            taskScheduler.setWaitForTasksToCompleteOnShutdown(true);
            //等待时长
            taskScheduler.setAwaitTerminationSeconds(60);
            return taskScheduler;
        }
    //接受任务的返回结果
    private ScheduledFuture<?> future;
    /**
     * 启动定时任务
     * @return
     */
    public boolean startTask(Runnable task,String cron) {
        log.info(Thread.currentThread().getName() + "==========定时任务开始启动==============");
        boolean flag = false;

        future = myThreadPoolTaskScheduler.schedule(task,new CronTrigger(cron));
        if (future!=null){
            flag = true;
            log.info(Thread.currentThread().getName() + "==========定时任务启动成功==============");
        }else {
            log.info(Thread.currentThread().getName() +"==========定时任务启动失败==============");
        }
        return flag;
    }

    /**
     * 停止定时任务
     * @return
     */
    public boolean stopTask() {
        boolean flag = false;
        if (future != null) {
            boolean cancel = future.cancel(true);
            if (cancel){
                flag = true;
                log.info("==========定时任务停止成功==============");
            }else {
                log.info("==========定时任务停止失败==============");
            }
        }else {
            flag = true;
            log.info("==========定时任务已经停止失败==============！！！");
        }
        return flag;
    }


}
