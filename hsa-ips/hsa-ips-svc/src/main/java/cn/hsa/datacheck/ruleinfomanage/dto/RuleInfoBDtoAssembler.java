package cn.hsa.datacheck.ruleinfomanage.dto;

import cn.hsa.datacheck.ruleinfomanage.entity.RuleInfoBDO;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 转换
 */
public class RuleInfoBDtoAssembler {

    private RuleInfoBDtoAssembler() {
        // hide for utils
    }

    /**
     * 转换为DTO对象
     *
     * @param value
     * @return
     */
    public static RuleInfoBDTO toRuleInfoBDto(RuleInfoBDO value) {
        if (value == null) {
            return null;
        }
        RuleInfoBDTO dto = new RuleInfoBDTO();
        BeanUtils.copyProperties(value, dto);
        return dto;
    }

    /**
     * 转换为实体对象
     *
     * @param dto
     * @return
     */
    public static RuleInfoBDO toRuleInfoBDo(RuleInfoBDTO dto) {
        if (dto == null) {
            return null;
        }
        RuleInfoBDO value = new RuleInfoBDO();
        BeanUtils.copyProperties(dto, value);
        return value;
    }

    /**
     * 转换为DTO对象list
     *
     * @param ruleInfoBs
     * @return
     */
    public static List<RuleInfoBDTO> toRuleInfoBDtoList(List<RuleInfoBDO> ruleInfoBs) {
        if (ruleInfoBs == null) {
            return null;
        }
        return ruleInfoBs.stream().map(RuleInfoBDtoAssembler::toRuleInfoBDto).collect(Collectors.toList());
    }

    /**
     * 转换为实体对象list
     *
     * @param dtos
     * @return
     */
    public static List<RuleInfoBDO> toRuleInfoBDoList(List<RuleInfoBDTO> dtos) {
        if (dtos == null) {
            return null;
        }
        return dtos.stream().map(RuleInfoBDtoAssembler::toRuleInfoBDo).collect(Collectors.toList());
    }

}
