package cn.hsa.datacheck.rulecheckresult.controller;

import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.hsa.hsaf.core.framework.web.WrapperResponse;
import cn.hsa.framework.commons.page.IQuery;
import cn.hsa.framework.commons.page.QueryResult;

import cn.hsa.datacheck.rulecheckresult.dto.RuleCheckResultDTO;
import cn.hsa.datacheck.rulecheckresult.dto.RuleCheckResultQueryDTO;
import cn.hsa.datacheck.rulecheckresult.service.RuleCheckResultService;
import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.web.bind.annotation.RestController;
import cn.hsa.framework.commons.controller.BaseController;
import lombok.extern.slf4j.Slf4j;
import javax.validation.Valid;

/**
 * <p>
 * 项目规则校验结果汇总表 前端控制器
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@RestController
@Slf4j
@Api(value = "/web/rulecheckresult/ruleCheckResult", tags = {"操作项目规则校验结果汇总表"})
@RequestMapping("/web/rulecheckresult/ruleCheckResult")
public class RuleCheckResultController extends BaseController  {


    private final RuleCheckResultService ruleCheckResultService;

    @Autowired
    public RuleCheckResultController(RuleCheckResultService ruleCheckResultService){
        this.ruleCheckResultService = ruleCheckResultService;
    }
    /**
     * 通过ID查询
     *
     * @param id ID
     * @return RuleCheckResult
     */
    @ApiOperation(value = "通过ID查询", tags = {"通过ID查询"}, notes = "ID不能为空")
    @GetMapping("/{id}")
    public WrapperResponse<RuleCheckResultDTO> get(@PathVariable String id) {
        return WrapperResponse.success(ruleCheckResultService.queryById(id));
    }

    /**
     * 分页查询信息
     *
     * @return 分页对象
     */
    @ApiOperation(value = "分页查询")
    @GetMapping("/dpage")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "pageNumber", value = "页码", dataType = "string", paramType = "query", example = "1"),
        @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "long", paramType = "query", example = "10"),
        @ApiImplicitParam(name = "sort", value = "正序/倒序", dataType = "string", paramType = "query", example = "asc/desc"),
        @ApiImplicitParam(name = "order", value = "排序字段-属性名", dataType = "string", paramType = "query", example = "rid"),
        @ApiImplicitParam(name = "ruleNo", value = "规则编号", dataType = "string", paramType = "query", example = "RL001"),
        @ApiImplicitParam(name = "chkBtch", value = "校验批次号", dataType = "string", paramType = "query", example = "0246544"),
        @ApiImplicitParam(name = "tableName", value = "校验表名", dataType = "string", paramType = "query", example = "PSN_INFO_B")
    })
    public WrapperResponse<Map<String, Object>> dpage(
            Integer pageNumber,
            Integer pageSize,
            String sort,
            String order,
            String ruleNo,
            String chkBtch,
            String tableName) {
        Map<String, Object> map = ruleCheckResultService.queryPage(pageNumber, pageSize, sort, order, ruleNo,chkBtch,tableName);

    	return WrapperResponse.success(map);
    }

    /**
     * 分页查询明细
     *
     * @return 分页对象
     */
    @ApiOperation(value = "分页查询")
    @GetMapping("/page")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNumber", value = "页码", dataType = "string", paramType = "query", example = "1"),
            @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "long", paramType = "query", example = "10"),
            @ApiImplicitParam(name = "sort", value = "正序/倒序", dataType = "string", paramType = "query", example = "asc/desc"),
            @ApiImplicitParam(name = "order", value = "排序字段-属性名", dataType = "string", paramType = "query", example = "rid"),
            @ApiImplicitParam(name = "ruleCheckResult", value = "项目规则校验结果汇总表查询条件", dataType = "RuleCheckResultQueryDTO", paramType = "query")
    })
    public WrapperResponse<Map<String, Object>> page(
            Integer pageNumber,
            Integer pageSize,
            String sort,
            String order,
            RuleCheckResultQueryDTO ruleCheckResultQuery
            ) {
        Map<String, Object> map = ruleCheckResultService.queryPage(pageNumber, pageSize, sort, order, ruleCheckResultQuery);
        // 如不想动BO，此处可以再加工为前台需要的自定义对象，适配界面用，例如 (**DTO)map.get("result")
        return WrapperResponse.success(map);
    }

    /**
     * 添加
     *
     * @return success/false
     */
    @PostMapping
    @ApiOperation(value = "添加项目规则校验结果汇总表")
    @ResponseStatus(HttpStatus.CREATED)
    public WrapperResponse<?> add(@Valid @RequestBody @ApiParam(name="项目规则校验结果汇总表",value="传入json格式",required=true) RuleCheckResultDTO ruleCheckResultDto) {
        RuleCheckResultDTO RuleCheckResultDto = ruleCheckResultService.save(ruleCheckResultDto);
        return WrapperResponse.success(RuleCheckResultDto);
    }

    /**
     * 删除
     *
     * @param id ID
     * @return success/false
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除项目规则校验结果汇总表")
    public WrapperResponse<?>  delete(@PathVariable String id) {
        ruleCheckResultService.delete(id);
        return WrapperResponse.success("");
    }

    /**
     * 编辑
     *
     * @return success/false
     */
    @PutMapping
    @ApiOperation(value = "编辑项目规则校验结果汇总表")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public WrapperResponse<?> edit(@Valid @RequestBody @ApiParam(name="项目规则校验结果汇总表",value="传入json格式",required=true) RuleCheckResultDTO ruleCheckResultDto) {
        RuleCheckResultDTO RuleCheckResultDto = ruleCheckResultService.update(ruleCheckResultDto);
        return WrapperResponse.success(RuleCheckResultDto);
    }

    /**
     * 保存List
     *
     * @param ruleCheckResultList 实体
     * @return success/false
     */
    @PostMapping("/saveList")
    @ApiOperation(value = "编辑项目规则校验结果汇总表")
    public WrapperResponse<?> saveList(@Valid @RequestBody @ApiParam(name="项目规则校验结果汇总表",value="传入json格式",required=true) List<RuleCheckResultDTO> ruleCheckResultList) {
        ruleCheckResultService.saveBatch(ruleCheckResultList);
        return WrapperResponse.success("");
    }
}
