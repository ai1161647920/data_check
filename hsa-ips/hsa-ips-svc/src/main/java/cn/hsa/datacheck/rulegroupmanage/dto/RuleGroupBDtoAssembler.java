package cn.hsa.datacheck.rulegroupmanage.dto;

import cn.hsa.datacheck.rulegroupmanage.entity.RuleGroupBDO;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 转换
 */
public class RuleGroupBDtoAssembler {

    private RuleGroupBDtoAssembler() {
        // hide for utils
    }

    /**
     * 转换为DTO对象
     *
     * @param value
     * @return
     */
    public static RuleGroupBDTO toRuleGroupBDto(RuleGroupBDO value) {
        if (value == null) {
            return null;
        }
        RuleGroupBDTO dto = new RuleGroupBDTO();
        BeanUtils.copyProperties(value, dto);
        return dto;
    }

    /**
     * 转换为实体对象
     *
     * @param dto
     * @return
     */
    public static RuleGroupBDO toRuleGroupBDo(RuleGroupBDTO dto) {
        if (dto == null) {
            return null;
        }
        RuleGroupBDO value = new RuleGroupBDO();
        BeanUtils.copyProperties(dto, value);
        return value;
    }


    /**
     * 转换为DTO对象list
     *
     * @param ruleGroupBs
     * @return
     */
    public static List<RuleGroupBDTO> toRuleGroupBDtoList(List<RuleGroupBDO> ruleGroupBs) {
        if (ruleGroupBs == null) {
            return null;
        }
        return ruleGroupBs.stream().map(RuleGroupBDtoAssembler::toRuleGroupBDto).collect(Collectors.toList());
    }

    /**
     * 转换为实体对象list
     *
     * @param dtos
     * @return
     */
    public static List<RuleGroupBDO> toRuleGroupBDoList(List<RuleGroupBDTO> dtos) {
        if (dtos == null) {
            return null;
        }
        return dtos.stream().map(RuleGroupBDtoAssembler::toRuleGroupBDo).collect(Collectors.toList());
    }

}
