package cn.hsa.datacheck.projectmanage.bo.impl;

import cn.hsa.datacheck.projectmanage.entity.ProjectInfoBDO;
import cn.hsa.datacheck.projectmanage.dao.ProjectInfoBDAO;

import cn.hsa.framework.commons.bo.CommonBO;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 项目信息表 服务实现类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Service
public class ProjectInfoBCommonBOImpl extends CommonBO<ProjectInfoBDAO, ProjectInfoBDO>  {


}
