package cn.hsa.datacheck.ruleinfomanage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import javax.validation.constraints.Size;

/**
 * <p>
 * 规则表  
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("rule_info_b")
@ApiModel(value="RuleInfoB", description="规则表  ")
public class RuleInfoBDO extends Model<RuleInfoBDO> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "规则编号")
    @TableId("RULE_NO")
    private String ruleNo;

    @ApiModelProperty(value = "所属组")
    @TableField("RULE_GRP_NO")
    private String ruleGrpNo;

    @ApiModelProperty(value = "表用户")
    @TableField("TAB_USER")
    private String tabUser;

    @ApiModelProperty(value = "表名")
    @TableField("TAB_NAME")
    private String tabName;

    @ApiModelProperty(value = "表注释")
    @TableField("TAB_ATN")
    private String tabAtn;

    @ApiModelProperty(value = "校验SQL条件")
    @TableField("CHK_SQL_COND")
    private String chkSqlCond;

    @ApiModelProperty(value = "规则类型")
    @TableField("RULE_TYPE")
    private String ruleType;

    @ApiModelProperty(value = "审核标志")
    @TableField("CHK_FLAG")
    private String chkFlag;

    @ApiModelProperty(value = "创建时间")
    @TableField("CRTE_TIME")
    private Date crteTime;

    @ApiModelProperty(value = "修改时间")
    @TableField("MODI_TIME")
    private Date modiTime;

    @ApiModelProperty(value = "创建用户")
    @TableField("CRTE_USER")
    private String crteUser;

    @ApiModelProperty(value = "修改用户")
    @TableField("MODI_USER")
    private String modiUser;

    @ApiModelProperty(value = "有效标志")
    @TableField("VALI_FLAG")
    private String valiFlag;

    @ApiModelProperty(value = "唯一记录号")
    @TableField("RID")
    private String rid;

    @ApiModelProperty(value = "备注")
    @TableField("DSCR")
    private String dscr;

    @ApiModelProperty(value = "规则注释")
    @TableField("RULE_NOTE")
    private String ruleNote;

    @ApiModelProperty(value = "规则描述")
    @TableField("RULE_DEC")
    private String ruleDec;


    @Override
    protected Serializable pkVal() {
        return this.ruleNo;
    }

}
