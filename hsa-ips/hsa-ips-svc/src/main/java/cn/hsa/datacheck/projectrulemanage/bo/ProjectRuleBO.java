package cn.hsa.datacheck.projectrulemanage.bo;

import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.projectrulemanage.dto.ProjectRuleDTO;
import cn.hsa.framework.commons.page.IQuery;
import cn.hsa.framework.commons.bo.IBO;
import cn.hsa.datacheck.projectrulemanage.dto.ProjectRuleQueryDTO;


/**
 * <p>
 * 项目规则表 服务类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
public interface ProjectRuleBO extends IBO {

    /**
     * 分页查询信息
     *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名
     * @param projectRuleDto 参数 DTO 对象
     * @return 分页对象
     */
    Map<String,Object> queryByDTOPage(int pageNumber, int pageSize, String sort, String order,  ProjectRuleQueryDTO projectRuleQueryDto);

    /**
     * 分页查询信息
     *
     * @param projectRuleDto 参数 DTO 对象
     * @return 分页对象
     */
    List<ProjectRuleDTO> queryByDTO(ProjectRuleQueryDTO projectRuleDto);

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return ProjectRule
     */
    ProjectRuleDTO getById(String id);


    /**
     * 添加
     *
     * @param projectRule 实体
     * @return void
     */
    ProjectRuleDTO save(ProjectRuleDTO projectRule);


    /**
     * 更新
     *
     * @param projectRule 实体
     * @return void
     */
    ProjectRuleDTO updateById(ProjectRuleDTO projectRule);

    /**
     * 更新
     *
     * @param projectRules 实体
     * @return List<ProjectRuleDTO>
     */
    List<ProjectRuleDTO> updateBatchById(List<ProjectRuleDTO> projectRules);
    /**
     * 保存
     *
     * @param projectRules 实体
     * @return List<ProjectRuleDTO>
     */
    List<ProjectRuleDTO> saveBatch(List<ProjectRuleDTO> projectRules);
    /**
     * 删除
     *
     * @param id 主键
     */
    void delete(String id);

    ProjectRuleDTO getByRuleGrpNo(String ruleGrpNo);
}
