package cn.hsa.datacheck.databasemanage.bo.impl;

import cn.hsa.datacheck.databasemanage.entity.DatabaseInfoBDO;
import cn.hsa.datacheck.databasemanage.dao.DatabaseInfoBDAO;

import cn.hsa.framework.commons.bo.CommonBO;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 数据源信息表 服务实现类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Service
public class DatabaseInfoBCommonBOImpl extends CommonBO<DatabaseInfoBDAO, DatabaseInfoBDO>  {


}
