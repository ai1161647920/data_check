package cn.hsa.datacheck.databasemanage.dao;

import java.util.List;

import cn.hsa.datacheck.databasemanage.entity.DatabaseInfoBDO;
import cn.hsa.datacheck.databasemanage.dto.DatabaseInfoBQueryDTO;
import cn.hsa.framework.commons.dao.BaseDAO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import cn.hsa.framework.commons.page.IQuery;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 数据源信息表 Mapper 接口
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Mapper
@Repository("NeuDatabaseInfoBDAO")
public interface DatabaseInfoBDAO extends BaseDAO<DatabaseInfoBDO> {


    /**
     * 分页查询（根据 dto 条件）
     * @param page         分页查询条件（可以为 RowBounds.DEFAULT）
     * @param dto          表字段 DatabaseInfoBQueryDTO 对象
     */
    IQuery<DatabaseInfoBDO> baseQueryByDTO(IQuery<DatabaseInfoBDO> page,@Param("dto") DatabaseInfoBQueryDTO dto);

    /**
     * 不分页查询（根据 dto 条件）
     * @param dto          表字段 DatabaseInfoBQueryDTO 对象
     */
    List<DatabaseInfoBDO> baseQueryByDTO(@Param("dto") DatabaseInfoBQueryDTO dto);
}
