package cn.hsa.datacheck.rulecheckresult.service;


import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.rulecheckresult.dto.RuleCheckResultDTO;
import cn.hsa.datacheck.rulecheckresult.dto.RuleCheckResultQueryDTO;
import cn.hsa.framework.commons.page.IQuery;
import cn.hsa.framework.commons.service.IBaseService;

/**
 * <p>
 * 项目规则校验结果汇总表 服务类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
public interface RuleCheckResultService extends IBaseService {
	/**
	 * 项目规则校验结果汇总表简单分页查询
	 *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名     	 
	 * @param ruleCheckResult 项目规则校验结果汇总表
	 * @return
	 */
	Map<String,Object> queryPage(int pageNumber, int pageSize, String sort, String order, RuleCheckResultQueryDTO ruleCheckResult);

    Map<String,Object> queryPage(int pageNumber, int pageSize, String sort, String order, String ruleNo,String chkBtch,String tableName);

    /**
     * 查询所有信息
     *
     * @param ruleCheckResult 参数 RuleCheckResultQueryDTO 对象
     * @return 分页对象
     */
    List<RuleCheckResultDTO> queryAll(RuleCheckResultQueryDTO ruleCheckResult);

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return RuleCheckResult
     */
    RuleCheckResultDTO queryById(String id);

    /**
     * 添加
     *
     * @param ruleCheckResult DTO
     * @return RuleCheckResultDTO
     */
    RuleCheckResultDTO save(RuleCheckResultDTO ruleCheckResult);

    /**
     * 更新
     *
     * @param ruleCheckResult DTO
     * @return RuleCheckResultDTO
     */
    RuleCheckResultDTO update(RuleCheckResultDTO ruleCheckResult);

    /**
     * 删除
     *
     * @param id 主键
     * @return success/false
     */
    void delete(String id);


    /**
     * 更新
     *
     * @param ruleCheckResults DTO
     * @return List<RuleCheckResultDTO>
     */
    List<RuleCheckResultDTO> updateBatch(List<RuleCheckResultDTO> ruleCheckResults);
    /**
     * 保存
     *
     * @param ruleCheckResults DTO
     * @return List<RuleCheckResultDTO>
     */
    List<RuleCheckResultDTO> saveBatch(List<RuleCheckResultDTO> ruleCheckResults);



}

