package cn.hsa.datacheck.projectmanage.bo;

import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.projectmanage.dto.ProjectInfoBDTO;
import cn.hsa.framework.commons.page.IQuery;
import cn.hsa.framework.commons.bo.IBO;
import cn.hsa.datacheck.projectmanage.dto.ProjectInfoBQueryDTO;


/**
 * <p>
 * 项目信息表 服务类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
public interface ProjectInfoBBO extends IBO {

    /**
     * 分页查询信息
     *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名
     * @param projectInfoBDto 参数 DTO 对象
     * @return 分页对象
     */
    Map<String,Object> queryByDTOPage(int pageNumber, int pageSize, String sort, String order,  ProjectInfoBQueryDTO projectInfoBQueryDto);

    /**
     * 分页查询信息
     *
     * @param projectInfoBDto 参数 DTO 对象
     * @return 分页对象
     */
    List<ProjectInfoBDTO> queryByDTO(ProjectInfoBQueryDTO projectInfoBDto);

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return ProjectInfoB
     */
    ProjectInfoBDTO getById(String id);


    /**
     * 添加
     *
     * @param projectInfoB 实体
     * @return void
     */
    ProjectInfoBDTO save(ProjectInfoBDTO projectInfoB);


    /**
     * 更新
     *
     * @param projectInfoB 实体
     * @return void
     */
    ProjectInfoBDTO updateById(ProjectInfoBDTO projectInfoB);

    /**
     * 更新
     *
     * @param projectInfoBs 实体
     * @return List<ProjectInfoBDTO>
     */
    List<ProjectInfoBDTO> updateBatchById(List<ProjectInfoBDTO> projectInfoBs);
    /**
     * 保存
     *
     * @param projectInfoBs 实体
     * @return List<ProjectInfoBDTO>
     */
    List<ProjectInfoBDTO> saveBatch(List<ProjectInfoBDTO> projectInfoBs);
    /**
     * 删除
     *
     * @param id 主键
     */
    void delete(String id);
}
