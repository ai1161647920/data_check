package cn.hsa.datacheck.jobtaskmanage.dto;

import lombok.Data;

@Data
public class DbInfoDTO {
    private String id;
    private String port;
    private String url;
    private String dbName;
    private String driveClassName;
    private String username;
    private String password;
}
