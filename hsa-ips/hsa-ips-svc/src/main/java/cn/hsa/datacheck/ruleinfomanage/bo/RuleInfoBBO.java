package cn.hsa.datacheck.ruleinfomanage.bo;

import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.ruleinfomanage.dto.RuleInfoBDTO;
import cn.hsa.framework.commons.page.IQuery;
import cn.hsa.framework.commons.bo.IBO;
import cn.hsa.datacheck.ruleinfomanage.dto.RuleInfoBQueryDTO;


/**
 * <p>
 * 规则表   服务类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
public interface RuleInfoBBO extends IBO {

    /**
     * 分页查询信息
     *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名
     * @param ruleInfoBQueryDto 参数 DTO 对象
     * @return 分页对象
     */
    Map<String,Object> queryByDTOPage(int pageNumber, int pageSize, String sort, String order,  RuleInfoBQueryDTO ruleInfoBQueryDto);

    /**
     * 分页查询信息
     *
     * @param ruleInfoBDto 参数 DTO 对象
     * @return 分页对象
     */
    List<RuleInfoBDTO> queryByDTO(RuleInfoBQueryDTO ruleInfoBDto);

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return RuleInfoB
     */
    RuleInfoBDTO getById(String id);


    /**
     * 添加
     *
     * @param ruleInfoB 实体
     * @return void
     */
    RuleInfoBDTO save(RuleInfoBDTO ruleInfoB);
    RuleInfoBDTO testConnect(RuleInfoBDTO ruleInfoB);
    Map<String, Object> getTables(RuleInfoBDTO ruleInfoBDto);

    /**
     * 更新
     *
     * @param ruleInfoB 实体
     * @return void
     */
    RuleInfoBDTO updateById(RuleInfoBDTO ruleInfoB);

    /**
     * 更新
     *
     * @param ruleInfoBs 实体
     * @return List<RuleInfoBDTO>
     */
    List<RuleInfoBDTO> updateBatchById(List<RuleInfoBDTO> ruleInfoBs);
    /**
     * 保存
     *
     * @param ruleInfoBs 实体
     * @return List<RuleInfoBDTO>
     */
    List<RuleInfoBDTO> saveBatch(List<RuleInfoBDTO> ruleInfoBs);
    /**
     * 删除
     *
     * @param id 主键
     */
    void delete(String id);

    /**
     * 根据规则组编号查询
     * @param ruleGrpNo
     * @return
     */
    List<RuleInfoBDTO> queryByRuleGrpNo(String ruleGrpNo);
}
