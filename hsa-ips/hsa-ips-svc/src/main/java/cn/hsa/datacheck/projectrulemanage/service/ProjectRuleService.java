package cn.hsa.datacheck.projectrulemanage.service;


import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.projectrulemanage.dto.ProjectRuleDTO;
import cn.hsa.datacheck.projectrulemanage.dto.ProjectRuleQueryDTO;
import cn.hsa.framework.commons.page.IQuery;
import cn.hsa.framework.commons.service.IBaseService;

/**
 * <p>
 * 项目规则表 服务类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
public interface ProjectRuleService extends IBaseService {
	/**
	 * 项目规则表简单分页查询
	 *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名     	 
	 * @param projectRule 项目规则表
	 * @return
	 */
	Map<String,Object> queryPage(int pageNumber, int pageSize, String sort, String order, ProjectRuleQueryDTO projectRule);

    /**
     * 查询所有信息
     *
     * @param projectRule 参数 ProjectRuleQueryDTO 对象
     * @return 分页对象
     */
    List<ProjectRuleDTO> queryAll(ProjectRuleQueryDTO projectRule);

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return ProjectRule
     */
    ProjectRuleDTO queryById(String id);

    /**
     * 添加
     *
     * @param projectRule DTO
     * @return ProjectRuleDTO
     */
    ProjectRuleDTO save(ProjectRuleDTO projectRule);

    /**
     * 更新
     *
     * @param projectRule DTO
     * @return ProjectRuleDTO
     */
    ProjectRuleDTO update(ProjectRuleDTO projectRule);

    /**
     * 删除
     *
     * @param id 主键
     * @return success/false
     */
    void delete(String id);


    /**
     * 更新
     *
     * @param projectRules DTO
     * @return List<ProjectRuleDTO>
     */
    List<ProjectRuleDTO> updateBatch(List<ProjectRuleDTO> projectRules);
    /**
     * 保存
     *
     * @param projectRules DTO
     * @return List<ProjectRuleDTO>
     */
    List<ProjectRuleDTO> saveBatch(List<ProjectRuleDTO> projectRules);


    ProjectRuleDTO getByRuleGrpNo(String ruleGrpNo);
}

