package cn.hsa.datacheck.rulecheckresult.bo.impl;

import cn.hsa.datacheck.rulecheckresult.entity.RuleCheckResultDO;
import cn.hsa.datacheck.rulecheckresult.dao.RuleCheckResultDAO;

import cn.hsa.framework.commons.bo.CommonBO;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 项目规则校验结果汇总表 服务实现类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Service
public class RuleCheckResultCommonBOImpl extends CommonBO<RuleCheckResultDAO, RuleCheckResultDO>  {


}
