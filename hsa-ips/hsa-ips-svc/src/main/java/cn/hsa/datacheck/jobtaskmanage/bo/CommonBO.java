package cn.hsa.datacheck.jobtaskmanage.bo;

public interface CommonBO {

    void addTable(String tableName);

    /**
     * 删除表
     * @param tableName
     */
    void delTable( String tableName);

    void insertByDynamicSql(String tableName, String dynamicSql);
}
