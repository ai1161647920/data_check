package cn.hsa.datacheck.rulegroupmanage.bo;

import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.rulegroupmanage.dto.RuleGroupBDTO;
import cn.hsa.datacheck.rulegroupmanage.entity.RuleGroupBDO;
import cn.hsa.framework.commons.page.IQuery;
import cn.hsa.framework.commons.bo.IBO;
import cn.hsa.datacheck.rulegroupmanage.dto.RuleGroupBQueryDTO;
import org.apache.ibatis.annotations.Param;


/**
 * <p>
 * 规则组   服务类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
public interface RuleGroupBBO extends IBO {

    /**
     * 分页查询信息
     *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名
     * @param ruleGroupBQueryDto 参数 DTO 对象
     * @return 分页对象
     */
    Map<String,Object> queryByDTOPage(int pageNumber, int pageSize, String sort, String order,  RuleGroupBQueryDTO ruleGroupBQueryDto);

    /**
     * 分页查询信息
     *
     * @param ruleGroupBDto 参数 DTO 对象
     * @return 分页对象
     */
    List<RuleGroupBDTO> queryByDTO(RuleGroupBQueryDTO ruleGroupBDto);

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return RuleGroupB
     */
    RuleGroupBDTO getById(String id);


    /**
     * 添加
     *
     * @param ruleGroupB 实体
     * @return void
     */
    RuleGroupBDTO save(RuleGroupBDTO ruleGroupB);


    /**
     * 更新
     *
     * @param ruleGroupB 实体
     * @return void
     */
    RuleGroupBDTO updateById(RuleGroupBDTO ruleGroupB);

    /**
     * 更新
     *
     * @param ruleGroupBs 实体
     * @return List<RuleGroupBDTO>
     */
    List<RuleGroupBDTO> updateBatchById(List<RuleGroupBDTO> ruleGroupBs);
    /**
     * 保存
     *
     * @param ruleGroupBs 实体
     * @return List<RuleGroupBDTO>
     */
    List<RuleGroupBDTO> saveBatch(List<RuleGroupBDTO> ruleGroupBs);
    /**
     * 删除
     *
     * @param ruleGroupBDto 主键
     */
    void delete(RuleGroupBDTO ruleGroupBDto);

    List<RuleGroupBDTO> baseQueryByDTO(RuleGroupBQueryDTO dto);
}
