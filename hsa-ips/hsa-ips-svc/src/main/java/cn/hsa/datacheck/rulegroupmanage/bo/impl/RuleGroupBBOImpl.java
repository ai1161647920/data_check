package cn.hsa.datacheck.rulegroupmanage.bo.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.hsa.datacheck.projectrulemanage.bo.impl.ProjectRuleCommonBOImpl;
import cn.hsa.datacheck.projectrulemanage.dto.ProjectRuleDtoAssembler;
import cn.hsa.datacheck.projectrulemanage.entity.ProjectRuleDO;
import cn.hsa.datacheck.rulegroupmanage.entity.RuleGroupBDO;
import cn.hsa.datacheck.rulegroupmanage.dto.RuleGroupBDtoAssembler;
import cn.hsa.datacheck.rulegroupmanage.dao.RuleGroupBDAO;
import cn.hsa.datacheck.rulegroupmanage.bo.RuleGroupBBO;
import cn.hsa.framework.commons.exception.ExceptionManager;
import cn.hsa.framework.commons.page.IQuery;
import cn.hsa.framework.commons.page.QueryResult;
import cn.hsa.framework.commons.bo.BaseBO;

import cn.hsa.datacheck.rulegroupmanage.dto.RuleGroupBQueryDTO;
import cn.hsa.datacheck.rulegroupmanage.dto.RuleGroupBDTO;
import cn.hsa.framework.commons.util.OrgHelper;
import cn.hsa.framework.commons.util.SequenceUtil;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;


/**
 * <p>
 * 规则组   服务实现类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Service("NeuRuleGroupBBOImpl")
@Slf4j
public class RuleGroupBBOImpl extends BaseBO implements RuleGroupBBO {
    @Autowired
    protected SequenceUtil sequenceUtil;

    private final RuleGroupBDAO ruleGroupBDAO;

    private final RuleGroupBCommonBOImpl ruleGroupBCommonBO;

    private final ProjectRuleCommonBOImpl projectRuleCommonBO;

    @Autowired
    public RuleGroupBBOImpl(RuleGroupBDAO ruleGroupBDAO, RuleGroupBCommonBOImpl ruleGroupBCommonBO, ProjectRuleCommonBOImpl projectRuleCommonBO) {
        this.ruleGroupBDAO = ruleGroupBDAO;
        this.ruleGroupBCommonBO = ruleGroupBCommonBO;
        this.projectRuleCommonBO = projectRuleCommonBO;
    }


    /**
     * 分页查询信息
     *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名
     * @param ruleGroupBQueryDto 参数 DTO 对象
     * @return 分页对象
     */
    @Override
    public Map<String, Object> queryByDTOPage(int pageNumber, int pageSize, String sort, String order, RuleGroupBQueryDTO ruleGroupBQueryDto) {
       
        //调用中台或DB
    	IQuery<RuleGroupBDO> page = QueryResult.of(pageNumber, pageSize, sort, order);
    	IQuery<RuleGroupBDO> da = ruleGroupBDAO.baseQueryByDTO(page, ruleGroupBQueryDto);
    	List<RuleGroupBDTO> ruleGroupBList = RuleGroupBDtoAssembler.toRuleGroupBDtoList(da.getResult());

    	Map<String,Object> rs = new HashMap<String, Object>();
    	rs.put("pageNumber", da.getPageNumber());
    	rs.put("pageSize", da.getPageSize());
    	rs.put("total", da.getRecordCount());
    	rs.put("result", ruleGroupBList);
    	return rs;
    	    	
	}

    /**
     * 分页查询信息
     *
     * @param ruleGroupBDto 参数 DTO 对象
     * @return 分页对象
     */
    @Override
    public List<RuleGroupBDTO> queryByDTO(RuleGroupBQueryDTO ruleGroupBDto) {
        List<RuleGroupBDO> ruleGroupBList = ruleGroupBDAO.baseQueryByDTO(ruleGroupBDto);
        return RuleGroupBDtoAssembler.toRuleGroupBDtoList(ruleGroupBList);
    }

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return RuleGroupB
     */
    @Override
    public RuleGroupBDTO getById(String id) {
        RuleGroupBDO ruleGroupB = ruleGroupBDAO.selectById(id);
        return RuleGroupBDtoAssembler.toRuleGroupBDto(ruleGroupB);
    }

    /**
     * 添加
     *
     * @param ruleGroupBDto RuleGroupBDTO
     * @return RuleGroupBDTO
     */
    @Override
    public RuleGroupBDTO save(RuleGroupBDTO ruleGroupBDto) {
        RuleGroupBDO ruleGroupB = RuleGroupBDtoAssembler.toRuleGroupBDo(ruleGroupBDto);
        ProjectRuleDO projectRule = new ProjectRuleDO();
        Date date = new Date();
        projectRule.setDsNo(ruleGroupBDto.getDsNo());
        projectRule.setItemCode(ruleGroupBDto.getItemCode());
        projectRule.setRuleGrpNo(ruleGroupBDto.getRuleGrpNo());
        ruleGroupB.setValiFlag("1");
        ruleGroupB.setCrteTime(date);
        ruleGroupB.setModiTime(date);
        ruleGroupB.setModiUser(OrgHelper.getUserAcct());
        ruleGroupB.setCrteUser(OrgHelper.getUserAcct());
        ruleGroupB.setRid(sequenceUtil.getRID());

        projectRule.setValiFlag("1");
        projectRule.setCrteTime(date);
        projectRule.setModiTime(date);
        projectRule.setModiUser(OrgHelper.getUserAcct());
        projectRule.setCrteUser(OrgHelper.getUserAcct());
        projectRule.setRid(sequenceUtil.getRID());

        RuleGroupBDO ruleGroupBSelect = ruleGroupBDAO.selectById(ruleGroupB.getRuleGrpNo());
        if(ruleGroupBSelect == null){
            projectRuleCommonBO.saveEntity(projectRule);
            ruleGroupBCommonBO.saveEntity(ruleGroupB);
        }else{
            throw new ExceptionManager(24006);
        }

        return RuleGroupBDtoAssembler.toRuleGroupBDto(ruleGroupB);
    }


    /**
     * 更新
     *
     * @param ruleGroupBDto RuleGroupBDTO
     * @return RuleGroupBDTO
     */
    @Override
    public RuleGroupBDTO updateById(RuleGroupBDTO ruleGroupBDto) {
        RuleGroupBDO ruleGroupB = RuleGroupBDtoAssembler.toRuleGroupBDo(ruleGroupBDto);
        ProjectRuleDO projectRule = new ProjectRuleDO();
        projectRule.setModiTime(new Date());
        projectRule.setModiUser(OrgHelper.getUserAcct());
        projectRule.setDsNo(ruleGroupBDto.getDsNo());
        projectRule.setItemCode(ruleGroupBDto.getItemCode());
        projectRule.setRuleGrpNo(ruleGroupBDto.getRuleGrpNo());


        ruleGroupB.setModiTime(new Date());
        ruleGroupB.setModiUser(OrgHelper.getUserAcct());
        RuleGroupBDO ruleGroupBSelect = ruleGroupBDAO.selectById(ruleGroupB.getRuleGrpNo());
        if(ruleGroupBSelect != null){
            projectRuleCommonBO.updateEntityById(projectRule);
            ruleGroupBCommonBO.updateEntityById(ruleGroupB);
        }else{
            throw new ExceptionManager(24005);
        }
        return RuleGroupBDtoAssembler.toRuleGroupBDto(ruleGroupB);
    }

    /**
     * 更新
     *
     * @param ruleGroupBDtos List<RuleGroupBDTO>
     * @return List<RuleGroupBDTO>
     */
    @Override
    public List<RuleGroupBDTO> updateBatchById(List<RuleGroupBDTO> ruleGroupBDtos) {
        List<RuleGroupBDO> updateList = RuleGroupBDtoAssembler.toRuleGroupBDoList(ruleGroupBDtos);
        ruleGroupBCommonBO.updateEntityBatchById(updateList);
        return RuleGroupBDtoAssembler.toRuleGroupBDtoList(updateList);
    }

    /**
     * 保存
     *
     * @param ruleGroupBDtos List<RuleGroupBDTO>
     * @return List<RuleGroupBDTO>
     */
    @Override
    public List<RuleGroupBDTO> saveBatch(List<RuleGroupBDTO> ruleGroupBDtos) {
        List<RuleGroupBDO> saveList = RuleGroupBDtoAssembler.toRuleGroupBDoList(ruleGroupBDtos);
        ruleGroupBCommonBO.saveEntityBatch(saveList);
        return RuleGroupBDtoAssembler.toRuleGroupBDtoList(saveList);
    }

     /**
     * 逻辑删除
     *
     * @param ruleGroupBDto 主键
     */
     @Override
     public void delete(RuleGroupBDTO ruleGroupBDto) {
         ProjectRuleDO projectRule = new ProjectRuleDO();
         projectRule.setRuleGrpNo(ruleGroupBDto.getRuleGrpNo());
         projectRule.setValiFlag("0");
         projectRuleCommonBO.updateEntityById(projectRule);

        RuleGroupBDO ruleGroupB = new RuleGroupBDO();
        ruleGroupB.setRuleGrpNo(ruleGroupBDto.getRuleGrpNo());
        ruleGroupB.setValiFlag("0");
        ruleGroupBCommonBO.updateEntityById(ruleGroupB);
     }

    @Override
    public List<RuleGroupBDTO> baseQueryByDTO(RuleGroupBQueryDTO dto) {

        List<RuleGroupBDO> ruleGroupBDOS = ruleGroupBDAO.baseQueryByDTO(dto);

        return RuleGroupBDtoAssembler.toRuleGroupBDtoList(ruleGroupBDOS);
    }


}
