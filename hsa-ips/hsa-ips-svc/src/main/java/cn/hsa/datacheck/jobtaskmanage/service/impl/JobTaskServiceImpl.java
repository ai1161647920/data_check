package cn.hsa.datacheck.jobtaskmanage.service.impl;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import cn.hsa.datacheck.databasemanage.dto.DatabaseInfoBDTO;
import cn.hsa.datacheck.databasemanage.service.DatabaseInfoBService;
import cn.hsa.datacheck.jobtaskmanage.bo.JobTaskBO;
import cn.hsa.datacheck.jobtaskmanage.common.constants.Constant;
import cn.hsa.datacheck.jobtaskmanage.common.util.SpringContextHolder;
import cn.hsa.datacheck.jobtaskmanage.dto.DbInfoDTO;
import cn.hsa.datacheck.jobtaskmanage.dto.JobTaskDTO;
import cn.hsa.datacheck.jobtaskmanage.dto.JobTaskQueryDTO;
import cn.hsa.datacheck.jobtaskmanage.dto.RuleExecInfoDto;
import cn.hsa.datacheck.jobtaskmanage.service.JobTaskService;
import cn.hsa.datacheck.projectmanage.bo.ProjectInfoBBO;
import cn.hsa.datacheck.projectmanage.dto.ProjectInfoBDTO;
import cn.hsa.datacheck.projectmanage.dto.ProjectInfoBQueryDTO;
import cn.hsa.datacheck.projectrulemanage.dto.ProjectRuleDTO;
import cn.hsa.datacheck.projectrulemanage.service.ProjectRuleService;
import cn.hsa.datacheck.ruleinfomanage.dto.RuleInfoBDTO;
import cn.hsa.datacheck.ruleinfomanage.service.RuleInfoBService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import cn.hsa.framework.commons.service.BaseService;
/**
 * <p>
 * 定时任务表   服务实现类
 * </p>
 *
 * @author pengli
 * @since 2021-07-26
 */
@Slf4j
@Service("NeuJobTaskServiceImpl")
public class JobTaskServiceImpl extends BaseService implements JobTaskService {
    @Autowired
    private ProjectInfoBBO ProjectInfoBO;

    private final JobTaskBO jobTaskBO;

    @Autowired
    public JobTaskServiceImpl(JobTaskBO jobTaskBO){
        this.jobTaskBO = jobTaskBO;
    }
    
    /**
     * 定时任务表  简单分页查询
     *
     * @param pageNumber 页码
     * @param pageSize 每页数量
     * @param sort 正序/倒序
     * @param order 排序字段-属性名  
     * @param jobTask 定时任务表  
     * @return
     */
    @Override
	public Map<String, Object> queryPage(int pageNumber, int pageSize, String sort, String order, JobTaskQueryDTO jobTask) {
    	return jobTaskBO.queryByDTOPage(pageNumber, pageSize, sort, order, jobTask);
	}

    /**
     * 查询所有信息
     *
     * @param jobTask 参数 JobTaskQueryDTO 对象
     * @return 对象List
     */
    @Override
    public List<JobTaskDTO> queryAll(JobTaskQueryDTO jobTask) {
        return jobTaskBO.queryByDTO(jobTask);
    }

    /**
     * 通过ID查询
     *
     * @param id ID
     * @return JobTask
     */
    @Override
    public JobTaskDTO queryById(String id) {
        return jobTaskBO.getById(id);
    }


    /**
     * 添加
     *
     * @param jobTask DTO
     * @return JobTaskDTO
     */
    @Override
    public JobTaskDTO save(JobTaskDTO jobTask) {
        return jobTaskBO.save(jobTask);
    }

    /**
     * 更新
     *
     * @param jobTask DTO
     * @return JobTaskDTO
     */
    @Override
    public JobTaskDTO update(JobTaskDTO jobTask) {
        return jobTaskBO.updateById(jobTask);
    }

    /**
     * 删除
     *
     * @param id 主键
     */
    @Override
    public void delete(String id) {
        jobTaskBO.delete(id);
    }

    /**
     * 批量更新
     *
     * @param jobTasks 实体
     * @return List<JobTaskDTO>
     */
    @Override
    public List<JobTaskDTO> updateBatch(List<JobTaskDTO> jobTasks) {
        return jobTaskBO.updateBatchById(jobTasks);
    }

    /**
     * 批量新增
     *
     * @param jobTasks 实体
     * @return List<JobTaskDTO>
     */
    @Override
    public List<JobTaskDTO> saveBatch(List<JobTaskDTO> jobTasks) {
        return jobTaskBO.saveBatch(jobTasks);
    }

    /**
     * 通过taskNo查询
     * @param taskNo
     * @return
     */

    @Override
    public JobTaskDTO queryByTaskNo(String taskNo) {
        return jobTaskBO.queryByTaskNo(taskNo);
    }

    /**
     * 根据任务编号、任务名称判断是否已经存在
     * @param jobTaskDto
     */
    @Override
    public List<JobTaskDTO> preSave(JobTaskDTO jobTaskDto) {
        return jobTaskBO.preSave(jobTaskDto);
    }


    @Override
    public synchronized ConcurrentHashMap<String, RuleExecInfoDto> constructRuleExecParam(JobTaskDTO jobTaskDTO) {
        String exeRuleScp = jobTaskDTO.getExeRuleScp();
        //执行规则范围
        String[] ruleGrpNos = exeRuleScp.split(Constant.SPLIT_RULESCP);
        //查询项目名称
        ProjectInfoBQueryDTO projectInfoBQueryDTO = new ProjectInfoBQueryDTO();
        projectInfoBQueryDTO.setItemname(jobTaskDTO.getItemname());
        projectInfoBQueryDTO.setValiFlag(Constant.VALI_FLAG_1);
        List<ProjectInfoBDTO> projectInfoBDTOS = ProjectInfoBO.queryByDTO(projectInfoBQueryDTO);
        if(projectInfoBDTOS == null ||projectInfoBDTOS.size() == 0){
            log.info("========查询项目信息为空======"+ projectInfoBDTOS);
            return new ConcurrentHashMap();
        }
        // 获取项目编号
        String itemcode = projectInfoBDTOS.get(0).getItemcode();
        if(ruleGrpNos.length == 0){
            log.info("========ruleGrpNos非法======"+ ruleGrpNos);
            return new ConcurrentHashMap();
        }
        //rule执行的参数map集合
        ConcurrentHashMap<String, RuleExecInfoDto> ruleExecInfoMap = new ConcurrentHashMap<>();
        //规则组列表
        for(int i = 0; i < ruleGrpNos.length; i++){
            String ruleGrpNo = ruleGrpNos[i];
            //根据规则编号查询规则列表
            RuleInfoBService ruleInfoBService = SpringContextHolder.getBean(RuleInfoBService.class);
            List<RuleInfoBDTO> ruleInfoBDTOList = ruleInfoBService.queryByRuleGrpNo(ruleGrpNo);
            if(ruleInfoBDTOList == null ||ruleInfoBDTOList.size() == 0){
                log.info("==============根据ruleGrpNo查询规则信息为空,ruleGrpNo is" + ruleGrpNo);
                continue;
            }
//
//      final List<String> sqlConsList = new ArrayList<>();//拼接的sql集合
//      for (RuleInfoBDTO ruleInfo : ruleInfoBDTOS) {
//        String chkSqlCond = ruleInfo.getChkSqlCond();
//        sqlConsList.add(chkSqlCond);
//      }

            //根据规则组编号查询查询数据源标号
            ProjectRuleService projectRuleService = SpringContextHolder.getBean(ProjectRuleService.class);
            ProjectRuleDTO projectRuleDTO = projectRuleService.getByRuleGrpNo(ruleGrpNo);
            if(projectRuleDTO == null){
                log.info("==============根据ruleGrpNo查询的ProjectRuleDTO为null");
                continue;
            }


            DatabaseInfoBService databaseInfoBService = SpringContextHolder.getBean(DatabaseInfoBService.class);
            // 数据源信息
            DatabaseInfoBDTO databaseInfoBDTO = databaseInfoBService.queryById(projectRuleDTO.getDsNo());
            DbInfoDTO dbInfoDTO = new DbInfoDTO();
            dbInfoDTO.setUsername(databaseInfoBDTO.getUserName());
            dbInfoDTO.setPassword(databaseInfoBDTO.getPwd());
            dbInfoDTO.setDriveClassName(databaseInfoBDTO.getDriver());
            dbInfoDTO.setUrl(databaseInfoBDTO.getConnectstring());

            //规则执行参数
            RuleExecInfoDto ruleExecInfoDto = new RuleExecInfoDto(ruleGrpNo, itemcode, ruleInfoBDTOList, dbInfoDTO);
            ruleExecInfoMap.put(ruleGrpNo, ruleExecInfoDto);
        }
        return ruleExecInfoMap;
    }



}
