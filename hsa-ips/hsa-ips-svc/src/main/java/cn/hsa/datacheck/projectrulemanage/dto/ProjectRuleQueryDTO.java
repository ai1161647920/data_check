package cn.hsa.datacheck.projectrulemanage.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * <p>
 * 项目规则表
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@ApiModel(value="ProjectRuleQueryDTO", description="项目规则表")
public class ProjectRuleQueryDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "规则组编号")
    private String ruleGrpNo;

    @ApiModelProperty(value = "项目编号")
    private String itemcode;

    @ApiModelProperty(value = "数据源编号")
    private String dsNo;

    @ApiModelProperty(value = "创建时间")
    private Date crteTime;

    @ApiModelProperty(value = "修改时间")
    private Date modiTime;

    @ApiModelProperty(value = "创建用户")
    private String crteUser;

    @ApiModelProperty(value = "修改用户")
    private String modiUser;

    @ApiModelProperty(value = "有效标志")
    private String valiFlag;

    @ApiModelProperty(value = "唯一记录号")
    private String rid;


    @ApiModelProperty(value = "备注")
    private String dscr;

    public String getRuleGrpNo() {
        return ruleGrpNo;
    }

    public void setRuleGrpNo(String ruleGrpNo) {
        this.ruleGrpNo = ruleGrpNo;
    }

    public void setCrteTime(Date crteTime) {
        this.crteTime = crteTime;
    }

    public void setModiTime(Date modiTime) {
        this.modiTime = modiTime;
    }

    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    public String getDsNo() {
        return dsNo;
    }

    public void setDsNo(String dsNo) {
        this.dsNo = dsNo;
    }

    public Date getCrteTime() {
        return crteTime;
    }

    public void setCrteTime(Long crteTime) {
        this.crteTime = crteTime != null ? new Date(crteTime) : null;
    }

    public Date getModiTime() {
        return modiTime;
    }

    public void setModiTime(Long modiTime) {
        this.modiTime = modiTime != null ? new Date(modiTime) : null;
    }

    public String getCrteUser() {
        return crteUser;
    }

    public void setCrteUser(String crteUser) {
        this.crteUser = crteUser;
    }

    public String getModiUser() {
        return modiUser;
    }

    public void setModiUser(String modiUser) {
        this.modiUser = modiUser;
    }

    public String getValiFlag() {
        return valiFlag;
    }

    public void setValiFlag(String valiFlag) {
        this.valiFlag = valiFlag;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getDscr() {
        return dscr;
    }

    public void setDscr(String dscr) {
        this.dscr = dscr;
    }

    @Override
    public String toString() {
        return "ProjectRuleQueryDTO{" +
                "ruleGrpNo='" + ruleGrpNo + '\'' +
                ", itemcode='" + itemcode + '\'' +
                ", dsNo='" + dsNo + '\'' +
                ", crteTime=" + crteTime +
                ", modiTime=" + modiTime +
                ", crteUser='" + crteUser + '\'' +
                ", modiUser='" + modiUser + '\'' +
                ", valiFlag='" + valiFlag + '\'' +
                ", rid='" + rid + '\'' +
                ", dscr='" + dscr + '\'' +
                '}';
    }

}
