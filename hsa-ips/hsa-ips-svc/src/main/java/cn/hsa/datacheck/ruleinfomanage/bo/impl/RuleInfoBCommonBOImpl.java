package cn.hsa.datacheck.ruleinfomanage.bo.impl;

import cn.hsa.datacheck.ruleinfomanage.entity.RuleInfoBDO;
import cn.hsa.datacheck.ruleinfomanage.dao.RuleInfoBDAO;

import cn.hsa.framework.commons.bo.CommonBO;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 规则表   服务实现类
 * </p>
 *
 * @author zhouxiaoxin
 * @since 2021-07-21
 */
@Service
public class RuleInfoBCommonBOImpl extends CommonBO<RuleInfoBDAO, RuleInfoBDO>  {


}
