package cn.hsa.datacheck.projectrulemanage.dto;

import cn.hsa.datacheck.projectrulemanage.entity.ProjectRuleDO;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 转换
 */
public class ProjectRuleDtoAssembler {

    private ProjectRuleDtoAssembler() {
        // hide for utils
    }

    /**
     * 转换为DTO对象
     *
     * @param value
     * @return
     */
    public static ProjectRuleDTO toProjectRuleDto(ProjectRuleDO value) {
        if (value == null) {
            return null;
        }
        ProjectRuleDTO dto = new ProjectRuleDTO();
        BeanUtils.copyProperties(value, dto);
        return dto;
    }

    /**
     * 转换为实体对象
     *
     * @param dto
     * @return
     */
    public static ProjectRuleDO toProjectRuleDo(ProjectRuleDTO dto) {
        if (dto == null) {
            return null;
        }
        ProjectRuleDO value = new ProjectRuleDO();
        BeanUtils.copyProperties(dto, value);
        return value;
    }

    /**
     * 转换为DTO对象list
     *
     * @param projectRules
     * @return
     */
    public static List<ProjectRuleDTO> toProjectRuleDtoList(List<ProjectRuleDO> projectRules) {
        if (projectRules == null) {
            return null;
        }
        return projectRules.stream().map(ProjectRuleDtoAssembler::toProjectRuleDto).collect(Collectors.toList());
    }

    /**
     * 转换为实体对象list
     *
     * @param dtos
     * @return
     */
    public static List<ProjectRuleDO> toProjectRuleDoList(List<ProjectRuleDTO> dtos) {
        if (dtos == null) {
            return null;
        }
        return dtos.stream().map(ProjectRuleDtoAssembler::toProjectRuleDo).collect(Collectors.toList());
    }

}
