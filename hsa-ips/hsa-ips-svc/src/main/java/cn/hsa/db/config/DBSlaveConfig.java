package cn.hsa.db.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
/*指定配置文件名，默认从classpath下寻找该文件，也就是等同于classpath:dataSource.properties
 * 可以指定多个文件
 */
@PropertySource(value = { "/config/db/db.properties" })
/*
 * 指定前缀，读取的配置信息项必须包含该前缀，且除了前缀外，剩余的字段必须和实体类的属性名相同，
 * 才能完成银映射
 */
@ConfigurationProperties(prefix = "spring.datasource.slave")
@Data
public class DBSlaveConfig {
    @Value("${spring.datasource.platform}")
    private String platform;
    //@Value("spring.datasource.slave.url")
    private String url;
    //@Value("spring.datasource.slave.username")
    private String username;
    //@Value("spring.datasource.slave.password")
    private String password;
    //@Value("spring.datasource.slave.driver-class-name")
    private String driverClassName;

}
