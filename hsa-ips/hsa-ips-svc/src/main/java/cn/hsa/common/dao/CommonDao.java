package cn.hsa.common.dao;



import com.baomidou.dynamic.datasource.annotation.DS;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
@DS("slave")
public interface CommonDao {
    /**
     * 创建表
     *
     * @param tableName 表名
     */
    @DS("slave")
    void addTable(@Param("tableName") String tableName);

    /**
     * 删除表
     * @param tableName
     */
    @DS("slave")
    void delTable(@Param("tableName") String tableName);

    @DS("slave")
    void insertByDynamicSql(@Param("tableName") String tableName, @Param("dynamicSql") String dynamicSql);
}
