package cn.hsa.xxx.api.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class OrgDTO implements Serializable {

    private String orgName;

}
