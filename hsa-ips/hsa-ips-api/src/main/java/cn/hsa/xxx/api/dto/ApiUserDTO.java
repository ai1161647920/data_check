package cn.hsa.xxx.api.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ApiUserDTO implements Serializable {
    private Integer id;
    private String name;

    /**
     * 0=male, 1=female
     */
    private Integer sex;


    private String addr;

}
