package cn.hsa.xxx.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cn.hsa.hsaf.core.framework.web.WrapperResponse;
import cn.hsa.xxx.api.dto.ApiUserDTO;

/**
 * 本服务暴露接口给其他服务使用
 * <p>
 * hsa-xxx,为该微服务名称，参考bootstrap.yml中spring.application.name定义
 */
@FeignClient(value = "hsa-xxx")
public interface ApiUserInfoService {

	/**
	 * path路径说明：/xxx/api/module1/user/get
	 *
	 * 其中，xxx为该微服务的context-path，参考bootstrap.yml中server.servlet.context-path定义
	 * 【重要说明】如果方法为get请求，那入参必须使用@RequestParam注解进行标注，否则会被识别为post请求，而出现异常。
	 *
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/xxx/api/module1/user/get", method = RequestMethod.GET)
	public WrapperResponse<ApiUserDTO> getUser(@RequestParam("userId") int userId);

}