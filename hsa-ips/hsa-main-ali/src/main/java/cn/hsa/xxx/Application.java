package cn.hsa.xxx;

import com.taobao.pandora.boot.PandoraBootstrap;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ImportResource;

@EnableCaching
@MapperScan({"cn.hsa.**.dao"})
@SpringBootApplication(scanBasePackages = {"cn.hsa"},exclude = {DataSourceAutoConfiguration.class})
@ImportResource("classpath:config/spring.xml")
public class Application {

    public static void main(String[] args) {
        // 启动 Pandora Boot 用于加载 Pandora 容器
        PandoraBootstrap.run(args);//使用hsf rpc则打开此注释

        SpringApplication.run(Application.class, args);

        // 标记服务启动完成,并设置线程 wait。防止用户业务代码运行完毕退出后，导致容器退出。
        PandoraBootstrap.markStartupAndWait();
    }

}
